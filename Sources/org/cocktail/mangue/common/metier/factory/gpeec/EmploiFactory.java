package org.cocktail.mangue.common.metier.factory.gpeec;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiCategorieFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiLocalisationFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiNatureBudgetFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiSpecialisationFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.OccupationFinder;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiLocalisation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * Gère la création des emplois
 * 
 * @author Chama LAATIK
 */
public final class EmploiFactory {
	
	private static EmploiFactory sharedInstance;

	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiFactory.sharedInstance().methode
	 *
	 * @return une instance de la classe
	 */
	public static EmploiFactory sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EmploiFactory();
		}
		return sharedInstance;
	}
	
	/**
	 * Crée une instance d'un emploi 
	 * @param edc : editingContext
	 * @param utilisateur : l'agent
	 * @return une instance de {@link EOEmploi}
	 */
	public IEmploi creer(EOEditingContext edc, EOAgentPersonnel utilisateur) {
		IEmploi newObject = new EOEmploi();    
		if ((edc != null) && (utilisateur != null)) {
			newObject.setPersonneCreation(utilisateur.toIndividu().persId());
			newObject.setPersonneModification(utilisateur.toIndividu().persId());
			newObject.setDateCreation(new NSTimestamp());
			newObject.setTemArbitrage(CocktailConstantes.FAUX);
			newObject.setTemConcours(CocktailConstantes.FAUX);
			newObject.setTemEnseignant(CocktailConstantes.FAUX);
			newObject.setTemDurabilite(IEmploi.TEM_DURABILITE_P);
			newObject.setTemValide(CocktailConstantes.VRAI);
			newObject.setTemContractuel(CocktailConstantes.FAUX);
			newObject.setTemNational(CocktailConstantes.VRAI);
			
			edc.insertObject((EOEmploi) newObject);
		}
		
		return newObject;
	}
	
	
	/**
	 * @return l'entête du fichier à exporter
	 */
	public String getEnteteExport() {
		String entete = "";

		entete += "NO_EMPLOI";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		if (EOGrhumParametres.isNoEmploiFormatte()) {
			entete += "NO_EMPLOI_FORMATTE";
			entete += CocktailExports.SEPARATEUR_EXPORT;
		}
		entete += "DEBUT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PUBLICATION";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "FIN";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "QUOTITE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "DURABILITE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "UAI";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PROGRAMME";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "TITRE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "CHAPITRE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "ARTICLE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "ENSEIGNANT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "CONTRACTUEL";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "CONCOURS";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "ARBITRAGE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "OBSERVATION";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		
		//CATEGORIE D'EMPLOI
		entete += "CATEG - DEBUT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "CATEG - FIN";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "CATEG - LIBELLE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		
		//SPECIALISATION
		entete += "SPEC - DEBUT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "SPEC - FIN";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "SPEC - LIBELLE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		
		//BUDGET
		entete += "BUDGET - DEBUT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "BUDGET - FIN";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "BUDGET - TYPE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		
		//OCCUPATION
		entete += "OCC - DEBUT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "OCC - FIN";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "OCC - NOM PRENOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "OCC - QUOTITE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		
		//LOCALISATION
		entete += "LOCAL - DEBUT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "LOCAL -  FIN";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "LOCAL - STRUCTURE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "LOCAL - QUOTITE";
		
		entete += CocktailExports.SAUT_DE_LIGNE;

		return entete;
	}
	
	/**
	 * @param edc : editingContext
	 * @param record : emploi à exporter
	 * @return texte à exporter
	 */
	public String getTexteExportPourRecord(EOEditingContext edc, IEmploi record) {
		String texte = "";
		LogManager.logDetail("numéro d'emploi : " + record.getNoEmploi());
		
		NSArray<EOOccupation> listeOccupations = OccupationFinder.sharedInstance().findListeOccupations(edc, record);
		NSArray<IEmploiLocalisation> listeLocalisations = EmploiLocalisationFinder.sharedInstance().findListeAffectationsCourantes(edc, record);
		
		LogManager.logDetail("nb occupations : " + listeOccupations.size());
		LogManager.logDetail("nb localisations : " + listeLocalisations.size());
		

		//On affiche autant de lignes qu'on a d'occupations
		int indexEmplois = 0;
		if (listeOccupations.size() > 0) {
			for (EOOccupation uneOccupation : listeOccupations) {
				texte += getElementsCommun(edc, record);
				texte += CocktailExports.ajouterChampDate(uneOccupation.dateDebut());
				texte += CocktailExports.ajouterChampDate(uneOccupation.dateFin());
				texte += CocktailExports.ajouterChamp(uneOccupation.individu().identite());
				texte += CocktailExports.ajouterChamp(uneOccupation.quotite().toString());
				
				if (listeLocalisations.size() == 1) {
					texte += ajouterLocalisation(listeLocalisations.get(0));
				}
				
				indexEmplois++;
				if (indexEmplois <= listeOccupations.count()) {
					texte += CocktailExports.SAUT_DE_LIGNE;
				}
			}
		} 
		
		indexEmplois = 0;
		//S'il n'y a qu'une localisation, elle apparaitra dans l'affichage des occupations
		//Sinon on affiche autant de lignes qu'on a de localisations
		if ((listeLocalisations.size() > 1)) { 
			for (IEmploiLocalisation uneLocalisation : listeLocalisations) {
				texte += getElementsCommun(edc, record);
				//Ajouter les champs à null pour l'occupation
				texte += CocktailExports.ajouterChampVide();
				texte += CocktailExports.ajouterChampVide();
				texte += CocktailExports.ajouterChampVide();
				texte += CocktailExports.ajouterChampVide();
				texte += ajouterLocalisation(uneLocalisation);
				
				indexEmplois++;
				if (indexEmplois <= listeLocalisations.size()) {
					texte += CocktailExports.SAUT_DE_LIGNE;
				}
			}
		} 
		
		if ((listeOccupations.size() == 0) && (listeLocalisations.size() == 0))  {
			texte += getElementsCommun(edc, record);
			texte += CocktailExports.SAUT_DE_LIGNE;
		}
		
		return texte;
	}
	
	/**
	 * @param edc : editingContext
	 * @param record : emploi à exporter
	 * @return élements en commun à exporter
	 */
	private String getElementsCommun(EOEditingContext edc, IEmploi record) {
		String texte = "";
		
		// No d'emploi
		texte += CocktailExports.ajouterChamp(record.getNoEmploi());
		// No d'emploi formatté si utilisé
		if (EOGrhumParametres.isNoEmploiFormatte()) {
			texte += CocktailExports.ajouterChamp(record.getNoEmploiFormatte());
		}
		// Date d'effet
		texte += CocktailExports.ajouterChampDate(record.getDateEffetEmploi());
		// Date de publication
		texte += CocktailExports.ajouterChampDate(record.getDatePublicationEmploi());
		// Date de fermeture
		texte += CocktailExports.ajouterChampDate(record.getDateFermetureEmploi());
		// Quotité
		texte += CocktailExports.ajouterChamp(record.getQuotite().toString());
		// Témoin de durabilité
		texte += CocktailExports.ajouterChamp(record.getTemDurabiliteEnLettre());
		// UAI
		if (record.getToRne() != null) {
			texte += CocktailExports.ajouterChamp(record.getToRne().code());
		}  else {
			texte += CocktailExports.ajouterChampVide();
		}
		// Programme
		if (record.getToProgramme() != null) {
			texte += CocktailExports.ajouterChamp(record.getToProgramme().toString());
		} else {
			texte += CocktailExports.ajouterChampVide();
		}
		// Titre
		if (record.getToTitre() != null) {
			texte += CocktailExports.ajouterChamp(record.getToTitre().toString());
		} else {
			texte += CocktailExports.ajouterChampVide();
		}
		// Chapitre
		if (record.getToChapitre() != null) {
			texte += CocktailExports.ajouterChamp(record.getToChapitre().toString());
		} else {
			texte += CocktailExports.ajouterChampVide();
		}
		// Article
		if (record.getToChapitreArticle() != null) {
			texte += CocktailExports.ajouterChamp(record.getToChapitreArticle().toString());
		} else {
			texte += CocktailExports.ajouterChampVide();
		}
		// Témoin enseignant
		texte += CocktailExports.ajouterChamp(record.getTemEnseignant());
		// Témoin contractuel
		texte += CocktailExports.ajouterChamp(record.getTemContractuel());
		// Témoin mis au concours
		texte += CocktailExports.ajouterChamp(record.getTemConcours());
		// Témoin mis en arbitrage
		texte += CocktailExports.ajouterChamp(record.getTemArbitrage());
		// Observations
		texte += CocktailExports.ajouterChamp(record.getCommentaire());
		
		// Catégorie d'emploi courante
		if (EmploiCategorieFinder.sharedInstance().findEmploiCategorieCourant(edc, record) != null) {
			texte += CocktailExports.ajouterChampDate(EmploiCategorieFinder.sharedInstance().findEmploiCategorieCourant(edc, record).getDateDebut());
			texte += CocktailExports.ajouterChampDate(EmploiCategorieFinder.sharedInstance().findEmploiCategorieCourant(edc, record).getDateFin());
			texte += CocktailExports.ajouterChamp(EmploiCategorieFinder.sharedInstance().findEmploiCategorieCourant(edc, record)
					.getToCategorieEmploi().codeEtLibelle());
		} else {
			texte += CocktailExports.ajouterChampVide();
			texte += CocktailExports.ajouterChampVide();
			texte += CocktailExports.ajouterChampVide();
		}
		// Spécialisation courante
		if (EmploiSpecialisationFinder.sharedInstance().findEmploiSpecialisationCourante(edc, record) != null) {
			texte += CocktailExports.ajouterChampDate(EmploiSpecialisationFinder.sharedInstance().findEmploiSpecialisationCourante(edc, record).getDateDebut());
			texte += CocktailExports.ajouterChampDate(EmploiSpecialisationFinder.sharedInstance().findEmploiSpecialisationCourante(edc, record).getDateFin());
			texte += CocktailExports.ajouterChamp(EmploiSpecialisationFinder.sharedInstance().findEmploiSpecialisationCourante(edc, record).
					libelleSpecialisation());
		} else {
			texte += CocktailExports.ajouterChampVide();
			texte += CocktailExports.ajouterChampVide();
			texte += CocktailExports.ajouterChampVide();
		}
		
		// Budget courant
		if (EmploiNatureBudgetFinder.sharedInstance().findEmploiNatureBudgetCourant(edc, record) != null) {
			texte += CocktailExports.ajouterChampDate(EmploiNatureBudgetFinder.sharedInstance().findEmploiNatureBudgetCourant(edc, record).getDateDebut());
			texte += CocktailExports.ajouterChampDate(EmploiNatureBudgetFinder.sharedInstance().findEmploiNatureBudgetCourant(edc, record).getDateFin());
			texte += CocktailExports.ajouterChamp(EmploiNatureBudgetFinder.sharedInstance().findEmploiNatureBudgetCourant(edc, record).getToNatureBudget().libelle());
		} else {
			texte += CocktailExports.ajouterChampVide();
			texte += CocktailExports.ajouterChampVide();
			texte += CocktailExports.ajouterChampVide();
		}
		
		return texte;
	}
	
	private String ajouterLocalisation(IEmploiLocalisation uneLocalisation) {
		String texte = "";
		texte += CocktailExports.ajouterChampDate(uneLocalisation.getDateDebut());
		texte += CocktailExports.ajouterChampDate(uneLocalisation.getDateFin());
		texte += CocktailExports.ajouterChamp(uneLocalisation.getToStructure().llStructure());
		texte += CocktailExports.ajouterChamp(uneLocalisation.getQuotite().toString());
		
		return texte;
	}

}

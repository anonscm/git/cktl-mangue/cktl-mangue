package org.cocktail.mangue.common.metier.factory.gpeec;

import org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiNatureBudget;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EONatureBudget;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Gère la création des natures budgets des emplois
 * 
 * @author Chama LAATIK
 */
public final class EmploiNatureBudgetFactory {
	
	private static EmploiNatureBudgetFactory sharedInstance;
	
	private EmploiNatureBudgetFactory() {
		// Pas d'instanciation possible
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiNatureBudgetFactory.sharedInstance().methode
	 *
	 * @return une instance de la classe
	 */
	public static EmploiNatureBudgetFactory sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EmploiNatureBudgetFactory();
		}
		return sharedInstance;
	}
	
	/**
	 * Crée une instance d'une nature budget d'un emploi
	 * @param edc : editingContext
	 * @param emploi : l'emploi
	 * @param utilisateur : l'agent
	 * @return une instance de {@link IEmploiNatureBudget}
	 */
	public IEmploiNatureBudget creer(EOEditingContext edc, IEmploi emploi, EOAgentPersonnel utilisateur) {
		IEmploiNatureBudget newObject = new EOEmploiNatureBudget();    
		newObject.setToEmploiRelationship(emploi);
		newObject.setPersonneCreation(utilisateur.toIndividu().persId());
		newObject.setPersonneModification(utilisateur.toIndividu().persId());
		newObject.setDateCreation(new NSTimestamp());
		edc.insertObject((EOEmploiNatureBudget) newObject);
		return newObject;
	}
	
	/**
	 * Crée une instance d'une nature budget d'un emploi
	 * @param edc : editingContext
	 * @param emploi : l'emploi
	 * @param dateDebut : date de début
	 * @param dateFin : date de fin
	 * @param budget : nature du budget
	 * @param utilisateur : l'agent
	 * @return une instance de {@link IEmploiNatureBudget}
	 */
	public IEmploiNatureBudget creer(EOEditingContext edc, IEmploi emploi, NSTimestamp dateDebut, NSTimestamp dateFin, 
			EONatureBudget budget, EOAgentPersonnel utilisateur) {
		IEmploiNatureBudget newObject = new EOEmploiNatureBudget();    
		newObject.setToEmploiRelationship(emploi);
		newObject.setToNatureBudgetRelationship(budget);
		newObject.setDateDebut(dateDebut);
		newObject.setDateFin(dateFin);
		newObject.setPersonneCreation(utilisateur.toIndividu().persId());
		newObject.setPersonneModification(utilisateur.toIndividu().persId());
		newObject.setDateCreation(new NSTimestamp());
		edc.insertObject((EOEmploiNatureBudget) newObject);
		return newObject;
	}

}

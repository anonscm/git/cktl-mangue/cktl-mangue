package org.cocktail.mangue.common.metier.factory.gpeec;

import org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Gère la création des spécialisations pour un emploi
 * 
 * @author Chama LAATIK
 */
public final class EmploiSpecialisationFactory {
	
	private static EmploiSpecialisationFactory sharedInstance;
	
	private EmploiSpecialisationFactory() {
		// Pas d'instanciation possible
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiSpecialisationFactory.sharedInstance().methode
	 *
	 * @return une instance de la classe
	 */
	public static EmploiSpecialisationFactory sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EmploiSpecialisationFactory();
		}
		return sharedInstance;
	}
	
	/**
	 * Crée une instance d'une spécialisation d'un emploi
	 * @param edc : editingContext
	 * @param emploi : l'emploi
	 * @param utilisateur : l'agent
	 * @return une instance de {@link EOEmploiSpecialisation}
	 */
	public IEmploiSpecialisation creer(EOEditingContext edc, IEmploi emploi, EOAgentPersonnel utilisateur) {
		IEmploiSpecialisation newObject = new EOEmploiSpecialisation();    
		newObject.setToEmploiRelationship(emploi);
		newObject.setPersonneCreation(utilisateur.toIndividu().persId());
		newObject.setPersonneModification(utilisateur.toIndividu().persId());
		newObject.setDateCreation(new NSTimestamp());
		edc.insertObject((EOEmploiSpecialisation) newObject);
		return newObject;
	}

	/**
	 * Crée une instance d'une spécialisation d'un emploi
	 * @param edc : editingContext
	 * @param emploi : l'emploi
	 * @param dateDebut : date de début
	 * @param dateFin : date de fin
	 * @param atos : spécialité ATOS
	 * @param cnu : spécialité CNU
	 * @param discSecondDegre : spécialité du second degré
	 * @param referensEmploi : spécialité 
	 * @param utilisateur : l'agent
	 * @return une instance de {@link EOEmploiSpecialisation}
	 */
	public IEmploiSpecialisation creer(EOEditingContext edc, IEmploi emploi, NSTimestamp dateDebut, NSTimestamp dateFin,
			EOSpecialiteAtos atos, EOCnu cnu, EODiscSecondDegre discSecondDegre, EOReferensEmplois referensEmploi, EOAgentPersonnel utilisateur) {
		IEmploiSpecialisation newObject = new EOEmploiSpecialisation();    
		newObject.setToEmploiRelationship(emploi);
		newObject.setToCnuRelationship(cnu);
		newObject.setToDisciplineSdDegreRelationship(discSecondDegre);
		newObject.setToEmploiReferensRelationship(referensEmploi);
		newObject.setToSpecialiteAtosRelationship(atos);
		newObject.setDateDebut(dateDebut);
		newObject.setDateFin(dateFin);
		newObject.setPersonneCreation(utilisateur.toIndividu().persId());
		newObject.setPersonneModification(utilisateur.toIndividu().persId());
		newObject.setDateCreation(new NSTimestamp());
		edc.insertObject((EOEmploiSpecialisation) newObject);
		return newObject;
	}
}

package org.cocktail.mangue.common.metier.factory.gpeec;

import org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiLocalisation;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Gère la création des affectation d'emploi
 * 
 * @author Chama LAATIK
 */
public final class EmploiLocalisationFactory {
	
	private static EmploiLocalisationFactory sharedInstance;
	
	private EmploiLocalisationFactory() {
		// Pas d'instanciation possible
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiAffectationFactory.sharedInstance(edc).methode
	 *
	 * @return une instance de la classe
	 */
	public static EmploiLocalisationFactory sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EmploiLocalisationFactory();
		}
		return sharedInstance;
	}
	
	/**
	 * Crée une instance d'une localisation d'emploi
	 * @param edc : editingContext
	 * @param emploi : l'emploi
	 * @param utilisateur : l'agent
	 * @return une instance de {@link EOEmploiLocalisation}
	 */
	public IEmploiLocalisation creer(EOEditingContext edc, IEmploi emploi, EOAgentPersonnel utilisateur) {
		IEmploiLocalisation newObject = new EOEmploiLocalisation();    
		newObject.setToEmploiRelationship(emploi);
		newObject.setPersonneCreation(utilisateur.toIndividu().persId());
		newObject.setPersonneModification(utilisateur.toIndividu().persId());
		newObject.setDateCreation(new NSTimestamp());
		edc.insertObject((EOEmploiLocalisation) newObject);
		return newObject;
	}

	/**
	 * Crée une instance d'une localisation d'emploi
	 * @param edc : editingContext
	 * @param emploi : l'emploi
	 * @param dateDebut : date de début
	 * @param dateFin : date de fin
	 * @param structure : structure de localisation de l'emploi
	 * @param utilisateur : l'agent
	 * @return une instance de {@link EOEmploiLocalisation}
	 */
	public IEmploiLocalisation creer(EOEditingContext edc, IEmploi emploi, NSTimestamp dateDebut, NSTimestamp dateFin, 
			EOStructure structure, EOAgentPersonnel utilisateur) {
		IEmploiLocalisation newObject = new EOEmploiLocalisation();    
		newObject.setToEmploiRelationship(emploi);
		newObject.setToStructureRelationship(structure);
		newObject.setDateDebut(dateDebut);
		newObject.setDateFin(dateFin);
		newObject.setQuotite(100);
		newObject.setPersonneCreation(utilisateur.toIndividu().persId());
		newObject.setPersonneModification(utilisateur.toIndividu().persId());
		newObject.setDateCreation(new NSTimestamp());
		edc.insertObject((EOEmploiLocalisation) newObject);
		return newObject;
	}
}

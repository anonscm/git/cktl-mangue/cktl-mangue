package org.cocktail.mangue.common.metier.factory.gpeec;

import org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Gère la création des emplois catégories
 * 
 * @author Chama LAATIK
 */
public final class EmploiCategorieFactory {
	
	private static EmploiCategorieFactory sharedInstance;
	
	private EmploiCategorieFactory() {
		// Pas d'instanciation possible
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiCategorieFactory.sharedInstance().methode
	 *
	 * @return une instance de la classe
	 */
	public static EmploiCategorieFactory sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EmploiCategorieFactory();
		}
		return sharedInstance;
	}
	
	/**
	 * Crée une instance d'un emploi catégorie
	 * @param edc : editingContext
	 * @param emploi : l'emploi
	 * @param utilisateur : l'agent
	 * @return une instance de {@link EOEmploiCategorie}
	 */
	public IEmploiCategorie creer(EOEditingContext edc, IEmploi emploi, EOAgentPersonnel utilisateur) {
		IEmploiCategorie newObject = new EOEmploiCategorie();    
		newObject.setToEmploiRelationship(emploi);
		newObject.setPersonneCreation(utilisateur.toIndividu().persId());
		newObject.setPersonneModification(utilisateur.toIndividu().persId());
		newObject.setDateCreation(new NSTimestamp());
		edc.insertObject((EOEmploiCategorie) newObject);
		return newObject;
	}

	/**
	 * Crée une instance d'un emploi catégorie
	 * @param edc : editingContext
	 * @param emploi : l'emploi
	 * @param dateDebut : date de début
	 * @param dateFin : date de fin
	 * @param uneCategorieEmploi : catégorie de l'emploi
	 * @param utilisateur : l'agent
	 * @return une instance de {@link EOEmploiCategorie}
	 */
	public IEmploiCategorie creer(EOEditingContext edc, IEmploi emploi, NSTimestamp dateDebut, NSTimestamp dateFin, 
			EOCategorieEmploi uneCategorieEmploi, EOAgentPersonnel utilisateur) {
		IEmploiCategorie newObject = new EOEmploiCategorie();    
		newObject.setToEmploiRelationship(emploi);
		newObject.setToCategorieEmploiRelationship(uneCategorieEmploi);
		newObject.setDateDebut(dateDebut);
		newObject.setDateFin(dateFin);
		newObject.setPersonneCreation(utilisateur.toIndividu().persId());
		newObject.setPersonneModification(utilisateur.toIndividu().persId());
		newObject.setDateCreation(new NSTimestamp());
		edc.insertObject((EOEmploiCategorie) newObject);
		return newObject;
	}
}

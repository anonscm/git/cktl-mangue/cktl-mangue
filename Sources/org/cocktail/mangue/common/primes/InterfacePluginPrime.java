/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes;

import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;

/** Interface de base pour les plugs-in g&eacute;rant des primes */
public interface InterfacePluginPrime {
	/** Calcule les montants de la prime attribu&eacute; &agrave; un individu<BR>
	 * Fournit montant et dates de validit&eacute; au moteur de calcul (PrimeCalcul) par la m&eacute;thode de ce dernier
	 * ajouterMontantPourPeriode(BigDecimal montant, NSTimestamp debutValidite, NSTimestamp finValidite) */
	public void calculerMontantsPrime(InformationPourPluginPrime information);
	/** Retourne true si l'utilisateur peut modifier le montant d'une prime */
	public abstract boolean peutModifierMontantPrime();
	/** Retourne une string d&eacute;crivant le mode de calcul */
	public abstract String modeCalculDescription();
	/** Retourne true le plug-in a des crit&egrave;res d'&eacute;ligibilit&eacute; sp&eacute;cifiques */
	public abstract boolean aCriteresEligibiliteSpecifiques();
	/** Retourne le diagnostic de non &eacute;gibilit&eacute; pour un individu */
	public abstract String diagnosticRefusEgilibilite(IndividuPourPrime individu);
	/** Retourne true si la prime peut &ecirc;tre renouvell&eacute;e */
	public abstract boolean peutEtreRenouvellee();
}

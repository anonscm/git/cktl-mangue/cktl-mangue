//Created on Wed Jun 11 12:24:00 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps;
import org.cocktail.mangue.modele.IIndividuAvecDuree;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOPrimeCode;
import org.cocktail.mangue.modele.goyave.EOPrimeExclusion;
import org.cocktail.mangue.modele.goyave.EOPrimeExclusionConge;
import org.cocktail.mangue.modele.goyave.EOPrimeExclusionPosition;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.EOPrimePopulation;
import org.cocktail.mangue.modele.goyave.EOPrimeStructure;
import org.cocktail.mangue.modele.goyave.ObjetAvecDureeValiditePourPrime;
import org.cocktail.mangue.modele.goyave.ObjetValidePourPrime;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.PrimeResultat;
import org.cocktail.mangue.modele.goyave.primes.PrimeResultatEligibilite;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartAssociation;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// 21/12/2010 - correction d'un bug lors de la vérification de l'éligilité sur le qualifier de contrat
// 22/03/2011 - dans la proratisation à temps partiel, il faut utiliser la quotité financière
public class PrimeCalcul {
	private static PrimeCalcul sharedInstance = null;
	private NSMutableArray resultatsEnCours; 	// Utiliser pour calculer les attributions de prime

	public static PrimeCalcul sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new PrimeCalcul();
		}
		return sharedInstance;
	}
	/** Retourne le mode de calcul d'une prime en instanciant le plugin et en lui demandant sa formule si elle
	 * est calcul&eacute;e sinon retourne non calcul&eacute;e */
	public String modeCalculPourPrime(EOPrime prime) throws Exception {
		if (prime.estCalculee() == false) {
			return "Prime non calculée";
		} else {
			try {
				Object moteurCalcul = LanceurCalcul.sharedInstance().instancierObjet(prime.primNomClasse());
				if (moteurCalcul instanceof InterfacePluginPrime) {
					return ((InterfacePluginPrime)moteurCalcul).modeCalculDescription();
				} else {
					throw new Exception("Un plug-in de calcul d'une prime doit impl&eacute;menter l'interface InformationPourPluginPrime");
				}	
			} catch (Exception e) { throw e; }
		}
	}
	/** pr&eacute;pare les attributions de prime pour une p&eacute;riode et selon les crit&egrave;res choisis.<BR>
	 * Si on trouve plusieurs valeurs d'un montant sur la p&eacute;riode, on cr&eacute;era autant d'attributions que de valeurs<BR>
	 * Exemple : la prime X a pour montant du 01/01/08 au 01/09/08 la valeur N puis &agrave; partir du 01/09/08 la valeur M => on retournera deux attributions
	 * @param prime prime &agrave; attribuer
	 * @param debutPeriode d&eacute;but de p&eacute;riode d'attribution de la prime
	 * @param finPeriode fin de p&eacute;riode d'attribution de la prime
	 * @param criteres crit&egrave;res de s&eacute;lection des individus (peut &ecirc;tre nul, sera alors cr&eacute;&eacute; en fonction de la prime)
	 * @param insererDansEditingContext true si les attributions doivent &ecirc;tre cr&eacute;&eacute;es dans l'editing context
	 * @return tableau de PrimeResultat, null si pas de candidat &grave; la prime ou un tableau de primes attributions. Dans le cas o&ugrave; la prime est calcul&eacut;e,
	 * les attributions ont des montants &eacute;gaux &agrave; z&eacute;ro
	 */
	public NSArray resultatsAttributionsPrimePourPeriodeEtCriteres(EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode,CriteresEvaluationPrime criteres) throws Exception {
		LogManager.logDetail("PrimeCalcul - resultatsAttributionsPrimePourPeriodeEtCriteres");
		return evaluerPrime(prime, debutPeriode, finPeriode, criteres, false, false,true);
	}
	/** pr&eacute;pare les attributions de prime pour une p&eacute;riode et selon les crit&egrave;res choisis.<BR>
	 * Si on trouve plusieurs valeurs d'un montant sur la p&eacute;riode, on cr&eacute;era autant d'attributions que de valeurs<BR>
	 * Exemple : la prime X a pour montant du 01/01/08 au 01/09/08 la valeur N puis &agrave; partir du 01/09/08 la valeur M => on retournera deux attributions
	 * @param prime prime &agrave; attribuer
	 * @param debutPeriode d&eacute;but de p&eacute;riode d'attribution de la prime
	 * @param finPeriode fin de p&eacute;riode d'attribution de la prime
	 * @param criteres crit&egrave;res de s&eacute;lection des individus (peut &ecirc;tre nul, sera alors cr&eacute;&eacute; en fonction de la prime)
	 * @param insererDansEditingContext true si les attributions doivent &ecirc;tre cr&eacute;&eacute;es dans l'editing context
	 * @param toutRecalculer true si toutes les attributions doivent être recalcul&eacute;es
	 * @return tableau de PrimeAttributions, null si pas de candidat &grave; la prime ou un tableau de primes attributions. Dans le cas o&ugrave; la prime est calcul&eacut;e,
	 * les attributions ont des montants &eacute;gaux &agrave; z&eacute;ro
	 */
	public NSArray attributionsPrimePourPeriodeEtCriteres(EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode,CriteresEvaluationPrime criteres, boolean insererDansEditingContext,boolean toutRecalculer) throws Exception {
		LogManager.logDetail("PrimeCalcul - attributionsPrimePourPeriodeEtCriteres");
		return evaluerPrime(prime, debutPeriode, finPeriode, criteres, insererDansEditingContext, true,toutRecalculer);
	}
	/**	Retourne un PrimeResultatEligibilite qui contient &eacute;ventuellement la liste des individus candidats pour la prime
	 * ou un diagnostic de refus. V&eacute;rifie les crit&egrave;res de population puis d'affectations, puis d'exclusions, puis
	 * de crit&egrave;res propres aux plugs-in */
	public PrimeResultatEligibilite determinerIndividusEligiblesPourPrimePeriodeEtCriteres(EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode,CriteresEvaluationPrime criteres) throws Exception {
		PrimeResultatEligibilite resultatEligibilite = verifierIndividuEligiblePourPrimePeriodeEtCriteres(prime, null, debutPeriode, finPeriode, criteres);
		if (resultatEligibilite.diagnosticRefus() != null) {
			return resultatEligibilite;
		}
		// Vérifier si les plugs-in imposent des contraintes supplémentaires
		if (prime.estCalculee()) {
			return verifierAvecPlugins(prime,resultatEligibilite.individusEligibles()); 
		} else {
			return resultatEligibilite;
		}
	}
	/** Evalue le montant de la prime courante, retourne les r&eacute;sultats de l'&eacute;valuation (tableau de PrimeResultat).<BR>
	 * Une prime personnalisable non calcul&eacute;e attend une personnalisation avec un montant<BR>
	 * Une prime non personnalisable non calcul&eacute;e attend un param&egrave;tre standard avec un montant<BR>
	 * Une prime calcul&eacute;e attend un ou plusieurs param&grave;tres. Si elle est personnalisable, une partie de ces paramt&egrave;tres
	 * sont des paramPerso<BR>
	 * Pour toutes les primes qui ne sont pas des versements uniques, les montants sont proratis&eacutes; sur la dur&eacute;e de l'attribution
	 * par rapport &agrave; la dur&eacute;e de la p&eacute;riode.<BR>
	 * On retourne plusieurs r&eacute;sultats si on trouve plusieurs param&egrave;tres valides pour la p&eacute;riode de l'individu.
	 * Gen&egrave;re une exception en cas de probl&egrave;me, le message de l'exception d&eacute;crit le probl&egrave;me
	 * @param prime	prime pour laquelle on &eacute;value les r&eacute;sultats
	 * @param individuPrime individu concern&eacute;
	 * @param debutPeriode d&eacute;but de la p&eacute;riode &agrave; laquelle on veut &eacute;valuer la prime
	 * @param finPeriode fin de la p&eacute;riode &agrave; laquelle on veut &eacute;valuer la prime  */
	public NSArray evaluerResultatsPourIndividu(EOPrime prime,IndividuPourPrime individuPrime,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		String periodeString = "du " + DateCtrl.dateToString(debutPeriode);
		if (finPeriode != null) {
			periodeString += " au " + DateCtrl.dateToString(finPeriode);
		}
		LogManager.logDetail("PrimeCalcul - Evaluation des resultats pour la periode " + periodeString);
		if (prime == null) {
			throw new Exception("PrimeCalcul - Fournir une prime");
		}
		// Vérifier si la prime est bien cohérente avec les paramètres
		// Une prime calculée a au moins un paramètre utilisé pour faire les calculs
		if (prime.estCalculee()) {
			if (prime.codes().count() < 1) {
				throw new Exception("PrimeCalcul - La prime " + prime.primLibLong() + " est calculée mais aucun paramètre ne lui est associé");
			}
		} else {
			if (prime.estPersonnalisable() == false && prime.codes().count() < 1) {
				throw new Exception("PrimeCalcul - La prime " + prime.primLibLong() + " ne comporte aucun paramètre");
			}
		}
		NSArray resultats = null;
		if (prime.estCalculee() == false) {
			if (prime.estPersonnalisable()) {
				resultats = evaluerResultatsPourPersonnalisation(prime,individuPrime);
			} else {
				resultats = evaluerResultatsAvecParametres(prime,individuPrime);	
			}
		} else {
			resultats = calculerResultats(prime,individuPrime,debutPeriode,finPeriode);
		}
		if (resultats != null && resultats.count() > 0) {
			LogManager.logDetail("PrimeCalcul - Nb resultats apres evaluation " + resultats.count());
			// Vérifier si les résultat trouvés vont au-delà de la période de l'individu si oui, restreindre la période du résultat
			java.util.Enumeration e = resultats.objectEnumerator();
			while (e.hasMoreElements()) {
				PrimeResultat resultatPrime = (PrimeResultat)e.nextElement();
				if (DateCtrl.isAfter(individuPrime.periodeDebut(),resultatPrime.debutValidite())) {
					resultatPrime.setDebutValidite(individuPrime.periodeDebut());
				}
				if (individuPrime.periodeFin() != null) {
					if (resultatPrime.finValidite() == null || DateCtrl.isAfter(resultatPrime.finValidite(), individuPrime.periodeFin())) {
						resultatPrime.setFinValidite(individuPrime.periodeFin());
					}
				}
			}
			// Vérifier si les résultats doivent être proratisés (pour les primes qui ne sont attribuées qu'une seule fois dans l'année)
			LogManager.logDetail("Periodicite de la prime " + prime.periodicite());
			if (prime.estAttributionUniqueDansAnnee()) {
				LogManager.logDetail("PrimeCalcul - Proratisation éventuelle des résultats");
				// Les montants des résultats sont des montants annuels. 
				// on calcule en durée comptable
				int nbJoursTotal = DateCtrl.NB_JOURS_COMPTABLES_ANNUELS;
				e = resultats.objectEnumerator();
				while (e.hasMoreElements()) {
					PrimeResultat resultat = (PrimeResultat)e.nextElement();
					int nbJours = DateCtrl.nbJoursEntre(resultat.debutValidite(), resultat.finValidite(), true,true);
					if (nbJours != nbJoursTotal) {
						LogManager.logDetail("nbJours " + nbJours + ", nbJoursTotal " + nbJoursTotal);
						LogManager.logDetail("Proratisation du montant, ratio : " + ((double)nbJours/nbJoursTotal));
						double montant = resultat.montant().doubleValue();
						montant = (montant * nbJours) / nbJoursTotal;
						resultat.setMontant(new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP));
						resultat.setFormuleCalcul(resultat.formuleCalcul() + "\nproratisé sur la période du " + DateCtrl.dateToString(resultat.debutValidite()) + " au " + DateCtrl.dateToString(resultat.finValidite()) + " avec un ratio de : " + nbJours + "/" + nbJoursTotal);
					}
				}
			}
			if (prime.estProratisable()) {	// la prime exige qu'on applique la quotité de temps partiel
				LogManager.logDetail("Prime proratisable, application éventuelle des quotites de temps partiel");
				resultats = appliquerQuotiteTempsPartielPourIndividuEtResultats(individuPrime.individu(),resultats,debutPeriode,finPeriode);
			}

		} else {
			LogManager.logDetail("PrimeCalcul - Pas de resultat apres evaluation");
		}
		return resultats;
	}
	public CriteresEvaluationPrime criteresPourPrimeEtPeriode(EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		CriteresEvaluationPrime criteres = new CriteresEvaluationPrime();
		try {
			if (prime.verifierContrat() || prime.verifierCorps() || prime.verifierGrade() || prime.verifierPopulation()) {
				NSArray primesPopulation = ObjetValidePourPrime.rechercherObjetsValidesPourPrime(prime.editingContext(), "PrimePopulation",prime);
				if (prime.verifierContrat()) {
					criteres.setTypesContrat(objetsNonNuls(primesPopulation,"typeContratTravail"));
				}
				if (prime.verifierCorps()) {
					criteres.setCorps(objetsNonNuls(primesPopulation,"corps"));
				}
				if (prime.verifierGrade()) {
					criteres.setGrades(objetsNonNuls(primesPopulation,"grade"));
				}
				if (prime.verifierPopulation()) {
					criteres.setTypesPopulation(objetsNonNuls(primesPopulation,"typePopulation"));
				}
			}
			if (prime.verifierStructure()) {
				NSArray primesStructures = ObjetAvecDureeValiditePourPrime.rechercherObjetsValidesPourPrimeEtPeriode(prime.editingContext(), "PrimeStructure",prime, debutPeriode, finPeriode);
				criteres.setStructures((NSArray)primesStructures.valueForKey("structure"));
			}
			if (prime.verifierFonction()) {
				NSArray primesFonctions = ObjetAvecDureeValiditePourPrime.rechercherObjetsValidesPourPrimeEtPeriode(prime.editingContext(), "PrimeFonction",prime, debutPeriode, finPeriode);
				criteres.setFonctions((NSArray)primesFonctions.valueForKey("fonction"));
			}
		} catch (Exception e) {
			String message = e.getMessage();
			message += "\n" + prime.primLibLong();
			throw new Exception(message);
		}
		return criteres;
	}
	/** Retourne un le r&eacute;sultat d'&eacute;ligbilit&eacute; d'un individu */
	public PrimeResultatEligibilite individuEligiblePourPrimeEtPeriode(EOIndividu individu,EOPrime prime, NSTimestamp debutPeriode, NSTimestamp finPeriode) throws Exception {
		PrimeResultatEligibilite resultatEligibilite = verifierIndividuEligiblePourPrimePeriodeEtCriteres(prime, individu, debutPeriode, finPeriode, null);
		if (resultatEligibilite.diagnosticRefus() != null) {
			return resultatEligibilite;
		}
		// Vérifier si les plugs-in imposent des contraintes supplémentaires
		if (prime.estCalculee()) {
			return verifierAvecPlugins(prime,resultatEligibilite.individusEligibles());
		} else {
			return resultatEligibilite;
		}
	}
	/** Retourne le diagnostic de validation d'un montant pour un individu, une prime et une p&acute;riode. Null si pas d'erreur
	 */
	public String verifierMontant(BigDecimal montant,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		// Vérifier si le composant de calcul st une sous-classe de pluginAvecParametresPersonnelsEtValidation
		try {
			InterfacePluginPrime plugin = (InterfacePluginPrime)LanceurCalcul.sharedInstance().instancierObjet(prime.primNomClasse());
			if (plugin instanceof InterfacePluginValidation) {
				PrimeResultatEligibilite resultatEligibilite = individuEligiblePourPrimeEtPeriode(individu, prime, debutPeriode, finPeriode);
				if (resultatEligibilite.diagnosticRefus() != null) {
					return resultatEligibilite.diagnosticRefus();
				} else {
					return ((InterfacePluginValidation)plugin).verifierValiditeMontant(montant, resultatEligibilite.individuPourPrimeAtIndex(0),prime,debutPeriode,finPeriode);
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			return "Exception lors de l'instanciation " + e.getMessage();
		}
	}
	/** Retourne le diagnostic de validation des param&egrave;tres personnels pour un individu, une prime et une p&eacute;riode. Null si
	 * pas d'erreur
	 */
	public String verifierParametresPersonnels(NSArray parametresPersonnels,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		// Vérifier si le composant de calcul st une sous-classe de pluginAvecParametresPersonnelsEtValidation
		try {
			InterfacePluginPrime plugin = (InterfacePluginPrime)LanceurCalcul.sharedInstance().instancierObjet(prime.primNomClasse());
			if (plugin instanceof InterfacePluginValidation) {
				PrimeResultatEligibilite resultatEligibilite = individuEligiblePourPrimeEtPeriode(individu, prime, debutPeriode, finPeriode);
				if (resultatEligibilite.diagnosticRefus() != null) {
					return resultatEligibilite.diagnosticRefus();
				} else {
					return ((InterfacePluginValidation)plugin).verifierParametresPersonnels(parametresPersonnels, resultatEligibilite.individuPourPrimeAtIndex(0));
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			return "Exception lors de l'instanciation " + e.getMessage();
		}
	}
	/** calcul les montants d'une prime pour un individu <BR>
    Instancie la classe de calcul associ&eacute;e &agrave; la prime et lui demande d'effectuer le calcul. <BR>
    La valeur retourn&eacute;e est un resultat (PrimeResultat) 
    @param prime prime &agrave; calculer 
    @param individuPrime (IndividuPourPrime) contient les informations sur l'individu pr&eacute;paration associ&eacute;e &agrave; ce bulletin de salaire
    @param personnalisation personnalisation associ&eacute;e &agrave cette prime (peut &ecirc;tre nulle)
    @return resultats NSArray de PrimesResultats contenant les r&eacute;sultats du calcul ou un message d'erreur si il s'est produit une exception
	 */
	public NSArray calculerResultats(EOPrime prime,IndividuPourPrime individuPrime,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		LogManager.logDetail("PrimeCalcul - calculerResultats");
		// instancier l'objet de calcul en utilisant le lanceur
		resultatsEnCours = new NSMutableArray();
		try {
			Object moteurCalcul = LanceurCalcul.sharedInstance().instancierObjet(prime.primNomClasse());
			if (moteurCalcul instanceof InterfacePluginPrime) {
				InformationPourPluginPrime information = new InformationPourPluginPrime(prime,individuPrime,debutPeriode,finPeriode);
				((InterfacePluginPrime)moteurCalcul).calculerMontantsPrime(information);
				return resultatsEnCours;
			} else {
				throw new Exception("Un plug-in de calcul d'une prime doit impl&eacute;menter l'interface InformationPourPluginPrime");
			}	
		} catch (Exception e) { throw e; }
	}
	/** pour les plugs-in de calcul, pour ajouter un r&eacute;sultat */
	public void ajouterMontantPourPeriode(EOIndividu individu,BigDecimal montant, NSTimestamp debutValidite, NSTimestamp finValidite,String formuleCalcul) {
		PrimeResultat resultat = new PrimeResultat(PrimeResultat.TYPE_CALCUL,individu.editingContext().globalIDForObject(individu),debutValidite,finValidite,montant,formuleCalcul,null);
		resultatsEnCours.addObject(resultat);
	}
	/** pour les plugs-in de calcul, pour ajouter une erreur */
	public void ajouterErreurPourPeriode(EOIndividu individu,String erreur, NSTimestamp debutValidite, NSTimestamp finValidite) {
		PrimeResultat resultat = new PrimeResultat(PrimeResultat.TYPE_CALCUL,individu.editingContext().globalIDForObject(individu),debutValidite,finValidite,new BigDecimal(0),null,erreur);
		resultatsEnCours.addObject(resultat);
		LogManager.logDetail(erreur);
	}
	/** Pr&eacute;pare une attribution pour un individu &grave; partir du r&eacute;sultat de l'&eacute;valuation
	 * 
	 * @param prime
	 * @param individu
	 * @param resultat
	 * @return attribution pr&eacute;par&eacute;e
	 */
	public EOPrimeAttribution preparerAttributionPourIndividu(EOPrime prime,EOIndividu individu,PrimeResultat resultat) {
		EOPrimeAttribution attribution = new EOPrimeAttribution();
		attribution.initAvecPrimeEtIndividu(prime, individu);
		if (resultat.typeCalcul() == PrimeResultat.TYPE_CALCUL) {
			if (resultat.montant() == null || resultat.montant().doubleValue() == 0) {
				// indiquer que le montant n'a pas encore été calculé
				attribution.setEstCalculee();
			} else {
				// on peut dorénavant valider l'attribution
				attribution.setEstProvisoire();
			}
		} else {
			attribution.setEstProvisoire();
		}
		attribution.setDebutValidite(resultat.debutValidite());
		attribution.setFinValidite(resultat.finValidite());
		attribution.setPattExercice(new Integer(DateCtrl.getYear(resultat.debutValidite())));
		attribution.setPattMontant(resultat.montant());
		attribution.setPattCalcul(resultat.formuleCalcul());
		return attribution;
	}
	//	Méthodes privées
	private NSArray evaluerPrime(EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode,CriteresEvaluationPrime criteres,boolean insererDansEditingContext,boolean retournerAttributions,boolean toutRecalculer) throws Exception {
		if (insererDansEditingContext && !retournerAttributions) {
			throw new Exception("PrimeCalcul - Incohérence : seules les attributions peuvent être insérées dans l'editing context. retournerAttributions vaut false");
		}
		try {
			LogManager.logDetail("PrimeCalcul - recalculer toutes les attributions " + toutRecalculer);
			PrimeResultatEligibilite resultatEligibilite = determinerIndividusEligiblesPourPrimePeriodeEtCriteres(prime,debutPeriode,finPeriode,criteres);
			NSArray attributionsEnCours = EOPrimeAttribution.rechercherAttributionsEnAttentePourPrimeIndividuEtPeriode(prime.editingContext(), null, prime, debutPeriode, finPeriode);
			if (toutRecalculer) {
				// Si on demande de tout recalculer, on invalide toutes les attributions en attente au cas où cela ne serait pas encore fait
				java.util.Enumeration e = attributionsEnCours.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
					attribution.setEstValide(false);
				}
			}
			NSArray attributionsValides = EOPrimeAttribution.rechercherAttributionsValidesPourPrimeIndividuEtPeriode(prime.editingContext(), null, prime, debutPeriode, finPeriode);
			if (resultatEligibilite.nbIndividusEligibles() > 0) {
				LogManager.logDetail("PrimeCalcul - Nb individus Eligibles " + resultatEligibilite.nbIndividusEligibles());
				NSMutableArray attributions = new NSMutableArray();
				for (int i = 0; i < resultatEligibilite.nbIndividusEligibles();i++) {
					IndividuPourPrime individuPrime = resultatEligibilite.individuPourPrimeAtIndex(i);
					// pour qu'un individu éligible ait son attribution recalculée, il faut qu'il n'ait pas une attribution validée
					boolean aRecalculer = attributionsValides.count() == 0 || ((NSArray)attributionsValides.valueForKey("individu")).containsObject(individuPrime.individu()) == false;
					// et que tout soit à recalculer ou que l'individu n'ait pas d'attribution en court
					aRecalculer = aRecalculer && (toutRecalculer || attributionsEnCours.count() == 0 || ((NSArray)attributionsEnCours.valueForKey("individu")).containsObject(individuPrime.individu()) == false);
					if (aRecalculer) {
						LogManager.logDetail("PrimeCalcul - calcul du montant de la prime pour " + individuPrime.individu().identite());
						NSArray resultatsPrime = evaluerResultatsPourIndividu(prime,individuPrime,debutPeriode,finPeriode);
						LogManager.logDetail("PrimeCalcul - Nb resultats " + resultatsPrime.count());
						java.util.Enumeration e1 = resultatsPrime.objectEnumerator();
						// Vérifier si il y a des erreurs pour pouvoir lancer une exception
						while (e1.hasMoreElements()) {
							PrimeResultat primeResultat = (PrimeResultat)e1.nextElement();
							if (primeResultat.messageErreur() != null && primeResultat.messageErreur().length() > 0) {
								LogManager.logDetail("PrimeCalcul - Resultat : " + primeResultat);
								prime.editingContext().revert();
								throw new Exception(primeResultat.messageErreur());
							}
						}
						// A ce stade, on sait qu'il ne s'est pas produit d'erreur
						e1 = resultatsPrime.objectEnumerator();
						if (retournerAttributions) {
							while (e1.hasMoreElements()) {
								PrimeResultat primeResultat = (PrimeResultat)e1.nextElement();
								LogManager.logDetail("PrimeCalcul - Resultat : " + primeResultat);

								EOPrimeAttribution attribution = preparerAttributionPourIndividu(prime,individuPrime.individu(),primeResultat);
								if (insererDansEditingContext) {
									prime.editingContext().insertObject(attribution);
								}
								attributions.addObject(attribution);	
							}
						} else {
							attributions.addObjectsFromArray(resultatsPrime);
						}
					}
				}
				return attributions;
			} else {
				LogManager.logDetail("PrimeCalcul - pas d'individus éligibles, diagnostic " + resultatEligibilite.diagnosticRefus());
				return null;
			}
		} catch (Exception e) {
			prime.editingContext().revert();
			throw e;
		}
	}
	//	Retourne tous les résultats trouvés
	private NSArray evaluerResultatsPourPersonnalisation(EOPrime prime,IndividuPourPrime individuPourPrime) throws Exception {
		LogManager.logDetail("PrimeCalcul - evaluerResultatsPourPersonnalisation");
		// Rechercher une personnalisation pour cet individu et cette prime
		NSArray personnalisations = EOPrimePersonnalisation.rechercherPersonnalisationsValidesPourIndividuPrimeEtPeriode(prime.editingContext(), individuPourPrime.individu(), prime, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
		// Si on en trouve aucune, on retourne un résultat avec le montant initialisé à 0
		EOGlobalID individuID = individuPourPrime.individu().editingContext().globalIDForObject(individuPourPrime.individu());
		if (personnalisations.count() == 0) {
			return new NSArray(new PrimeResultat(PrimeResultat.TYPE_MONTANT_PERSONNEL,individuID,individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),new BigDecimal(0),"Valeur personnelle non initialisée",null));	// Il s'agit d'une valeur personnelle
		} else {
			personnalisations = EOSortOrdering.sortedArrayUsingKeyOrderArray(personnalisations, new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareAscending)));
			NSMutableArray resultats = new NSMutableArray();
			// On a forcément une intersection entre les dates des personnalisations et la période d'attribution de la prime
			java.util.Enumeration e = personnalisations.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimePersonnalisation personnalisation = (EOPrimePersonnalisation)e.nextElement();
				if (personnalisation.pmpeMontant() == null) {
					throw new Exception("PrimeCalcul - La personnalisation de la prime est incohérente avec la prime");

				}
				resultats.addObject(new PrimeResultat(PrimeResultat.TYPE_MONTANT_PERSONNEL,individuID,personnalisation.debutValidite(),personnalisation.finValidite(),personnalisation.pmpeMontant(),"Valeur personnalisée : " + personnalisation.pmpeMontant(),null));
			}
			return resultats;
		}
	}
	private NSArray evaluerResultatsAvecParametres(EOPrime prime, IndividuPourPrime individuPourPrime) throws Exception {
		LogManager.logDetail("PrimeCalcul - evaluerResultatsAvecParametres");
		NSArray parametres = EOPrimeParam.rechercherParametresValidesPourCodeEtPeriode(prime.editingContext(), (EOPrimeCode)prime.codes().objectAtIndex(0), individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());	
		if (parametres.count() == 0) {
			throw new Exception("PrimeCalcul - Pas de paramètre valide pour la prime à cette période");
		} 
		// Rechercher le type des paramètres, ils doivent tous être de même type sinon ils sont incohérents
		// on ne peut pas déterminer de valeur du paramètre
		EOPrimeParam parametre = (EOPrimeParam)parametres.objectAtIndex(0);
		java.util.Enumeration e = parametres.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametreCourant = (EOPrimeParam)e.nextElement();
			if (parametreCourant.aMemeType(parametre) == false) {
				throw new Exception("PrimeCalcul - Les paramètres associés au code " + parametre.code().pcodCode() + " sont de type incohérents");
			} else if (parametreCourant.pparMontant() == null) {
				throw new Exception("PrimeCalcul - Les paramètres associés au code " + parametre.code().pcodCode() + " doivent fournir un montant");
			}
		}
		// Vérifier si les données fournis dans les paramètres sont cohérents avec les données fournies pour l'individu
		if (parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_CORPS) && individuPourPrime.corps() == null ||
				parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_GRADE) && individuPourPrime.grade() == null ||
				parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_ECHELON) && individuPourPrime.echelon() == null ||
				parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_INDICE) && individuPourPrime.indice() == null ||
				parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_FONCTION) && individuPourPrime.fonction() == null) {
			throw new Exception("PrimeCalcul - Les données fournies pour l'individu sont incomplètes pour un paramètre de type " + parametre.pparType());
		}
		NSArray parametresValides = parametres;
		String typeParametrage = "";
		if (parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_CORPS)) {
			parametresValides = parametresPourValeur(parametres,individuPourPrime.corps(),"corps");
			typeParametrage = "pour le corps : " + individuPourPrime.corps().cCorps();
		} else if (parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_GRADE) || parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_ECHELON)) {
			parametresValides = parametresPourValeur(parametres,individuPourPrime.grade(),"grade");
			typeParametrage = "pour le grade : " + individuPourPrime.grade().cGrade();
		} else if (parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_INDICE)) {
			parametresValides = parametresPourValeur(parametres,individuPourPrime.indice(),"pparIndice");
			typeParametrage = "pour l'indice : " + individuPourPrime.indice();
		} else if (parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_FONCTION)) {
			parametresValides = parametresPourValeur(parametres,individuPourPrime.fonction(),"fonction");
			typeParametrage = "pour la fonction : " + individuPourPrime.fonction().libelleLong();
		}
		if (parametre.pparType().equals(EOPrimeParam.TYPE_PARAM_ECHELON)) {
			// Vérifier si ne faut pas encore restreindre
			parametresValides = parametresPourValeur(parametresValides,individuPourPrime.echelon(),"echelon");

		}

		if (parametresValides == null || parametresValides.count() == 0) {
			throw new Exception("Pas de parametre ou de montant valide pour la prime et le parametre " + parametre.pparLibelle());
		}
		NSMutableArray resultats = new NSMutableArray();

		// On peut trouver plusieurs valeurs du paramètre sur la période, on retournera donc plusieurs résultats le cas échéant
		EOGlobalID individuID = individuPourPrime.individu().editingContext().globalIDForObject(individuPourPrime.individu());
		parametresValides = EOSortOrdering.sortedArrayUsingKeyOrderArray(parametresValides, new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareAscending)));
		e = parametresValides.objectEnumerator();
		while (e.hasMoreElements()) {
			parametre = (EOPrimeParam)e.nextElement();
			IntervalleTemps intervalle = Utilitaires.IntervalleTemps.intersectionPeriodes(parametre.debutValidite(), parametre.finValidite(), individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
			// si l'intervalle de temps est nul, signaler une erreur
			if (intervalle == null) {
				resultats.addObject(new PrimeResultat(PrimeResultat.TYPE_SANS_CALCUL,individuID,individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),null,null,"Pas de valeur du paramètre standard pour la période"));

			} else {
				String formuleCalcul = "Montant annuel fixe : " + parametre.pparMontant() + " " + typeParametrage;
				resultats.addObject(new PrimeResultat(PrimeResultat.TYPE_SANS_CALCUL,individuID,intervalle.debutPeriode(),intervalle.finPeriode(),parametre.pparMontant(),formuleCalcul,null));
			}
		}
		return resultats;
	}
	private NSArray parametresPourValeur(NSArray parametres,Object valeur,String cle) throws Exception {
		if (parametres == null) {
			return null;
		}
		NSMutableArray paramValides = new NSMutableArray();
		java.util.Enumeration e = parametres.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
			if (cle.equals("echelon")) {
				int echelonMini = new Integer(parametre.pparEchelonMini()).intValue();
				int echelonMaxi = new Integer(parametre.pparEchelonMaxi()).intValue();
				int echelon = new Integer((String)valeur).intValue();
				if (echelon >= echelonMini && echelon <= echelonMaxi && parametre.pparMontant() != null) {
					paramValides.addObject(parametre);
				}
			} 
			Object valeurParametre = parametre.valueForKey(cle);
			if (valeurParametre != null && valeurParametre.getClass().getName().equals(valeur.getClass().getName())) {
				if (valeur instanceof EOGenericRecord && valeur == valeurParametre && parametre.pparMontant() != null) {
					paramValides.addObject(parametre);
				} else if (valeur instanceof String && valeur.equals(valeurParametre)  && parametre.pparMontant() != null) {
					paramValides.addObject(parametre);
				}
			} else {
				throw new Exception("PrimeCalcul - les valeurs ne correspondent pas, parametre :" + valeurParametre.getClass().getName() + ", objet : " + valeur.getClass().getName());
			}	
		}
		return paramValides;
	}

	//	Retourne un tableau de PrimePourIndividu construit à partir des individus candidats pour la prime
	// Retourne nul si pas d'individu éligibles
	private PrimeResultatEligibilite verifierIndividuEligiblePourPrimePeriodeEtCriteres(EOPrime prime,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode,CriteresEvaluationPrime criteres) throws Exception {
		if (criteres == null) {
			criteres = criteresPourPrimeEtPeriode(prime, debutPeriode, finPeriode);
		}
		NSMutableArray individusEligibles = new NSMutableArray(); // Contiendra des éléments de carrière, des avenants ou des individus
		// Déterminer les individus éligibles en fonction des corps/grades/populations/typesContrat/fonctions
		// Si individu est non nul, déterminer si l'individu est éligible
		// les objets recherchés pour déterminer l'éligibilité doivent implémenter l'interface IndividuAvecDuree
		if (prime.verifierGrade() || prime.verifierCorps() || prime.verifierPopulation()) {
			NSMutableArray qualifiers = new NSMutableArray();
			if (prime.verifierGrade()) {
				qualifiers.addObject(SuperFinder.construireORQualifier("toGrade.cGrade", (NSArray)criteres.grades().valueForKey("cGrade")));
			} else if (prime.verifierCorps()) {
				qualifiers.addObject(SuperFinder.construireORQualifier("toGrade.toCorps.cCorps", (NSArray)criteres.corps().valueForKey("cCorps")));
			} else if (prime.verifierPopulation()) {
				qualifiers.addObject(SuperFinder.construireORQualifier("toCarriere.toTypePopulation.code", (NSArray)criteres.typesPopulation().valueForKey("cTypePopulation")));
			}
			if (debutPeriode != null) {
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOElementCarriere.DATE_DEBUT_KEY, debutPeriode, EOElementCarriere.DATE_FIN_KEY, finPeriode));
			}
			if (individu != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_INDIVIDU_KEY + " = %@", new NSArray(individu)));
			}
			individusEligibles.addObjectsFromArray(EOElementCarriere.rechercherElementsAvecCriteres(prime.editingContext(), new EOAndQualifier(qualifiers), true, true));
		}
		if (prime.verifierContrat()) {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(SuperFinder.construireORQualifier("contrat.toTypeContratTravail.code", (NSArray)criteres.typesContrat().valueForKey("code")));
			if (finPeriode != null) {
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOContratAvenant.DATE_DEBUT_KEY, debutPeriode, EOContratAvenant.DATE_FIN_KEY, finPeriode));
			}
			if (individu != null) {
				// 21/12/2010 - correction d'un bug lors de la vérification de l'éligilité sur le qualifier de contrat
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("contrat.toIndividu = %@", new NSArray(individu)));
			}
			individusEligibles.addObjectsFromArray(EOContratAvenant.rechercherAvenantsAvecCriteres(prime.editingContext(), new EOAndQualifier(qualifiers), true, true));
		}
		if (prime.verifierFonction()) {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(SuperFinder.construireORQualifier("association.libelleLong", (NSArray)criteres.fonctions().valueForKey("assLibelle")));
			if (finPeriode != null) {
				NSMutableArray orQualifiers = new NSMutableArray();
				// On vérifie que la date début est nulle et que la date fin est nulle ou dans l'intervalle
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateDebut = NIL AND (dateFin = NIL or dateFin >= %@)", new NSArray(debutPeriode)));
				// On ajouter le qulaifier sur la période
				orQualifiers.addObject(SuperFinder.qualifierPourPeriode("dateDebut", debutPeriode, "dateFin", finPeriode));
				qualifiers.addObject(new EOOrQualifier(orQualifiers));
			}
			NSArray repartFonctions = EORepartAssociation.rechercherRepartAssociationsPourCriteres(prime.editingContext(), new EOAndQualifier(qualifiers),true);
			java.util.Enumeration e = repartFonctions.objectEnumerator();
			while (e.hasMoreElements()) {
				EORepartAssociation repart = (EORepartAssociation)e.nextElement();
				EOIndividu individuPourFonction = repart.individu();
				if (individuPourFonction != null) {
					if (individu != null) {
						if (individuPourFonction == individu) {
							individusEligibles.addObject(repart);
						}
					} else {
						individusEligibles.addObject(repart);
					}
				}
			}
		}
		LogManager.logDetail("PrimeCalcul - verifierIndividuEligiblePourPrimePeriodeEtCriteres : nbIndividus potentiels " + individusEligibles.count());
		if (individusEligibles.count() == 0) {
			return new PrimeResultatEligibilite(null,"Critères de population non remplis");
		} else {
			return individusValidesPourPrimeEtPeriode(individusEligibles,prime,debutPeriode,finPeriode,criteres);
		}
	}
	//	Retourne parmi les individus éligibles, ceux qui supportent les critères d'inclusions (structures) ou d'exclusions 
	// si il y en a et auxquels la prime n'a pas encore été attribuée
	//	retourne un tableau de IndividuPourPrime
	private PrimeResultatEligibilite individusValidesPourPrimeEtPeriode(NSArray individusEligibles,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode,CriteresEvaluationPrime criteres) {
		PrimeResultatEligibilite resultat = new PrimeResultatEligibilite();
		String diagnosticErreur = null;		// Pour stocker le dernier diagnostic d'erreur
		// Rechercher les structures si nécessaires
		NSArray structures = null;
		if (prime.verifierStructure()) {
			structures = (NSArray)EOPrimeStructure.rechercherStructuresValidesPourPrimeEtPeriode(prime.editingContext(),prime,debutPeriode, finPeriode);
		}
		// Rechercher si il y a des exclusions de primes, de changements de position ou de congés
		NSArray primesExclues = (NSArray)EOPrimeExclusion.rechercherPrimesExcluesPourPrime(prime.editingContext(), prime);
		NSArray typesCongeExclus = EOPrimeExclusionConge.rechercherTypesCongesValidesPourPrime(prime.editingContext(), prime);
		NSArray positionsExclues = EOPrimeExclusionPosition.rechercherPositionsValidesPourPrime(prime.editingContext(), prime);
		// Rechercher les individus bénéficiant d'une attribution valide de cette prime pour la période
		NSArray individusAvecAttribution = EOPrimeAttribution.rechercherIndividusAvecAttributionValidePourPrimeEtPeriode(prime.editingContext(), prime, debutPeriode, finPeriode);
		java.util.Enumeration e = individusEligibles.objectEnumerator();
		NSMutableArray individusAjoutes = new NSMutableArray();
		while (e.hasMoreElements()) {
			IIndividuAvecDuree individuEligible = (IIndividuAvecDuree)e.nextElement();
			LogManager.logDetail("PrimeCalcul - individusValidesPourPrimeEtPeriode, individu en cours " + individuEligible.individu().identite());
			// Un individu n'est pas éligible si il a déjà la prime et que celle-ci est un versement unique
			if (individusAvecAttribution.containsObject(individuEligible.individu()) == false || prime.periodicite().equals(EOPrime.VERSEMENT_UNIQUE) == false) {
				// Prendre comme période d'éligibilité l'intersection entre la période de la prime et la période retenue pour l'individu
				IntervalleTemps periodeEligibilite = Utilitaires.IntervalleTemps.intersectionPeriodes(debutPeriode,finPeriode,individuEligible.debutPeriode(),individuEligible.finPeriode());
				String message = "Periode periodeEligibilite : du " + DateCtrl.dateToString(periodeEligibilite.debutPeriode());
				if (periodeEligibilite.finPeriode() != null) {
					message += " au " + DateCtrl.dateToString(periodeEligibilite.finPeriode());
				}
				LogManager.logDetail(message);
				// Déterminer les périodes sur lesquels l'individu peut bénéficier de la prime
				PrimeResultatAvecPeriodes resultatPeriodes = periodesValidesPourIndividu(individuEligible.individu(), periodeEligibilite, structures, primesExclues, typesCongeExclus, positionsExclues);
				if (resultatPeriodes.diagnosticRefus() != null) {
					LogManager.logDetail("PrimeCalcul - individusValidesPourPrimeEtPeriode, individu non eligible, diagnostic " + resultat.diagnosticRefus());
					diagnosticErreur = resultatPeriodes.diagnosticRefus();
				} else if (resultatPeriodes.intervallesValides == null || resultatPeriodes.intervallesValides().count() == 0) {
					LogManager.logDetail("PrimeCalcul - individusValidesPourPrimeEtPeriode, pas d'intervalle valide pour prime");
					diagnosticErreur = "Pas d'intervalle valide sur la periode";
				} else {
					NSArray intervallesValides = resultatPeriodes.intervallesValides();
					LogManager.logDetail("PrimeCalcul - individusValidesPourPrimeEtPeriode, nb intervalles valides pour prime " + intervallesValides.count());
					// Ils sont déjà triés par date croissante
					java.util.Enumeration e1 = intervallesValides.objectEnumerator();
					// Ajouter cet individu comme candidat à la prime
					while (e1.hasMoreElements()) {
						IntervalleTemps periode = (IntervalleTemps)e1.nextElement();
						IndividuPourPrime individuPourPrime = new IndividuPourPrime();
						individuPourPrime.initAvecIndividuEtDates(individuEligible.individu(),periode.debutPeriode(), periode.finPeriode());
						if (individuEligible instanceof EOElementCarriere) {
							individuPourPrime.setElementCarriere((EOElementCarriere)individuEligible);
						} else if (individuEligible instanceof EOContratAvenant) {
							individuPourPrime.setContratAvenant((EOContratAvenant)individuEligible);
						} else if (individuEligible instanceof EORepartAssociation) {
							individuPourPrime.setFonction(((EORepartAssociation)individuEligible).association());
						}
						individusAjoutes.addObject(individuPourPrime);
					}
				}
			} else {
				// On ajoute un diagnostic qui sera annulé dans le cas où il y a plusieurs individus qui sont valides
				diagnosticErreur = "Attribution déjà effectuée pour la période";
			}
		}
		if (individusAjoutes.count() > 0) {
			NSMutableArray sortOrderings = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("nomIndividu", EOSortOrdering.CompareAscending));
			sortOrderings.addObject(EOSortOrdering.sortOrderingWithKey("periodeDebut", EOSortOrdering.CompareAscending));
			EOSortOrdering.sortArrayUsingKeyOrderArray(individusAjoutes, sortOrderings);
			resultat.setIndividusEligibles(regrouperPourPrime(individusAjoutes,prime));	// on regroupe toutes les périodes contigües d'un même individu qui ont les mêmes caractéristiques
		} else {
			resultat.setDiagnosticRefus(diagnosticErreur);
		}
		return resultat;
	}
	// Regroupe toutes les périodes contigues des individus qui ont les mêmes caractéristiques
	private NSArray regrouperPourPrime(NSArray individusPourPrime,EOPrime prime) {
		// les individus pour primes sont rangés par ordre alphabétique et dates croissantes
		NSMutableArray individusRegroupes = new NSMutableArray();
		IndividuPourPrime ipPrecedent = (IndividuPourPrime)individusPourPrime.objectAtIndex(0);
		boolean dernierIpAjoute = false;
		for (int i = 1; i < individusPourPrime.count();i++) {
			IndividuPourPrime ipCourant = (IndividuPourPrime)individusPourPrime.objectAtIndex(i);
			// si les deux ip sont identiques au regard de la prime et qu'ils sont contigus, on les regroupe
			if (ipCourant.estEgalPourPrime(ipPrecedent, prime) && DateCtrl.dateToString(ipCourant.periodeDebut()).equals(DateCtrl.dateToString(DateCtrl.jourSuivant(ipPrecedent.periodeFin())))) {
				ipPrecedent.setPeriodeFin(ipCourant.periodeFin());
				dernierIpAjoute = false;
			} else {
				individusRegroupes.addObject(ipPrecedent);
				ipPrecedent = ipCourant;
				dernierIpAjoute = true;
			}
		}
		// Ajouter le dernier individu précédent
		if (dernierIpAjoute == false) {
			individusRegroupes.addObject(ipPrecedent);
		}
		return individusRegroupes;
	}
	/* Retourne un résultat de périodes avec  tableau de p&eacute;riodes pendant lequel un individu peut b&eacute;n&eacute;ficier
	 * d'une prime ou (exclusif) un diagnostic de refus
	 * @param structures structures d'affectation requises pour b&eacute;&eacute;ficier de la prime (une parmi la liste)
	 * @param primesExclues primes que ne doit pas avoir l'individu 
	 * @param typesCongesExclus types de cong&eacute;s que ne doit pas prendre l'individu 
	 * @param positionsExclues positions dans lequel ne doit pas &ecirc;tre l'individu 
	 */
	private PrimeResultatAvecPeriodes periodesValidesPourIndividu(EOIndividu individu,IntervalleTemps periodeEligibilite,NSArray structures,NSArray primesExclues, NSArray typesCongesExclus,NSArray positionsExclues) {
		NSArray intervallesValides = new NSArray(periodeEligibilite);
		// Vérifie si l'individu vérifie les critères de structure
		if (structures != null && structures.count() > 0) {
			LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, verification des structures d'affectation");
			NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("individu = %@ AND temValide = 'O'",new NSArray(individu)));
			qualifiers.addObject(SuperFinder.construireORQualifier("toStructureUlr.cStructure", (NSArray)structures.valueForKey("cStructure")));
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOAffectation.DATE_DEBUT_KEY, periodeEligibilite.debutPeriode(), EOAffectation.DATE_FIN_KEY, periodeEligibilite.finPeriode()));
			NSArray affectations = EOAffectation.rechercherAffectationsAvecCriteres(individu.editingContext(), new EOAndQualifier(qualifiers));
			intervallesValides = intervallesCommunsAvecAffectations(periodeEligibilite,affectations); // classés par date de début croissante
			if (intervallesValides != null && intervallesValides.count() > 0) {
				LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, Intervalles pour structures " + intervallesValides);
			} else {
				LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, pas de periodes d'affectation valides");
				return new PrimeResultatAvecPeriodes("Critère d'affectation aux structures non rempli");
			}
		}
		// Vérifier si l'individu satisfait tous les critères d'exclusion
		NSArray sortOrderings = new NSArray(EOSortOrdering.sortOrderingWithKey("debutPeriode", EOSortOrdering.CompareAscending));
		intervallesValides = EOSortOrdering.sortedArrayUsingKeyOrderArray(intervallesValides, sortOrderings);
		// Exclusion de primes
		if (primesExclues.count() > 0) {
			LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, verification des exclusions");
			java.util.Enumeration e = intervallesValides.objectEnumerator();
			NSMutableArray intervalles = new NSMutableArray();
			while (e.hasMoreElements()) {
				IntervalleTemps periode = (IntervalleTemps)e.nextElement();
				// Rechercher toutes les attributions de primes pour cet individu pendant la période
				NSArray attributions = EOPrimeAttribution.rechercherAttributionsEnCoursPourPrimesIndividuEtPeriode(individu.editingContext(), individu, primesExclues, periode.debutPeriode(), periode.finPeriode());
				if (attributions.count() > 0) {
					attributions = EOSortOrdering.sortedArrayUsingKeyOrderArray(attributions,new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareAscending)));
					intervalles.addObjectsFromArray(intervallesNonCommuns(periode,attributions,"debutValidite","finValidite"));
				} else {
					// On peut prendre l'intégralité de la période
					intervalles.addObject(periode);
				}
			}
			intervallesValides = EOSortOrdering.sortedArrayUsingKeyOrderArray(intervalles, sortOrderings);
			if (intervallesValides != null && intervallesValides.count() > 0) {
				LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, nb periodes sans exclusion " + intervallesValides.count());
			} else {
				LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, pas de periode pour prime car exclusion avec autre prime");
				return new PrimeResultatAvecPeriodes("Exclusion avec une autre prime");
			}
		}
		// Exclusions de congés
		if (typesCongesExclus.count() > 0) {
			LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, verification des exclusions de conge");
			java.util.Enumeration e = intervallesValides.objectEnumerator();
			NSMutableArray intervalles = new NSMutableArray();
			while (e.hasMoreElements()) {
				IntervalleTemps periode = (IntervalleTemps)e.nextElement();
				// Rechercher toutes les absences de ces types d'absence de cet individu pendant la période
				NSArray conges = EOAbsences.rechercherAbsencesPourIndividuDatesEtTypesAbsence(individu.editingContext(), individu, periode.debutPeriode(), periode.finPeriode(), typesCongesExclus);
				if (conges.count() > 0) {
					conges = EOSortOrdering.sortedArrayUsingKeyOrderArray(conges,PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
					intervalles.addObjectsFromArray(intervallesNonCommuns(periode,conges, EOAbsences.DATE_DEBUT_KEY, EOAbsences.DATE_FIN_KEY));
				} else {
					// On peut prendre l'intégralité de la période
					intervalles.addObject(periode);
				}
			}
			intervallesValides = EOSortOrdering.sortedArrayUsingKeyOrderArray(intervalles, sortOrderings);
			if (intervallesValides != null && intervallesValides.count() > 0) {
				LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, nb periodes sans exclusion de conge " + intervallesValides.count());
			} else {
				LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, pas de periode pour prime car exclusion de conge");
				return new PrimeResultatAvecPeriodes("Exclusion pour des congés");
			}
		}
		if (positionsExclues.count() > 0) {
			LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, verification des exclusions de position");
			java.util.Enumeration e = intervallesValides.objectEnumerator();
			NSMutableArray intervalles = new NSMutableArray();
			while (e.hasMoreElements()) {
				IntervalleTemps periode = (IntervalleTemps)e.nextElement();
				// Rechercher tous les changements de position qui sont exclusifs de cet individu pendant la période
				NSArray changementsPosition = EOChangementPosition.rechercherChangementsPourIndividuPositionsEtPeriode(individu.editingContext(), individu, positionsExclues,periode.debutPeriode(), periode.finPeriode());					
				if (changementsPosition.count() > 0) {
					changementsPosition = EOSortOrdering.sortedArrayUsingKeyOrderArray(changementsPosition, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
					intervalles.addObjectsFromArray(intervallesNonCommuns(periode,changementsPosition,"dateDebut","dateFin"));

				} else {
					// On peut prendre l'intégralité de la période
					intervalles.addObject(periode);
				}
			}
			intervallesValides = EOSortOrdering.sortedArrayUsingKeyOrderArray(intervalles, sortOrderings);
			if (intervallesValides != null && intervallesValides.count() > 0) {
				LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, nb periodes sans exclusion de position " + intervallesValides.count());
			} else {
				LogManager.logDetail("PrimeCalcul - periodesValidesPourIndividu, pas de periode pour prime car exclusion de position");
				return new PrimeResultatAvecPeriodes("Exclusion pour des positions");
			}
		}
		// Tous les critères sont passés
		return new PrimeResultatAvecPeriodes(intervallesValides);
	}
	private NSArray objetsNonNuls(NSArray primesPopulation,String cle) throws Exception {
		NSMutableArray objets = new NSMutableArray();
		java.util.Enumeration e = primesPopulation.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimePopulation primePopulation = (EOPrimePopulation)e.nextElement();
			EOGenericRecord record = (EOGenericRecord)primePopulation.valueForKey(cle);
			if (record != null) {
				objets.addObject(record);
			}
		}
		if (objets.count() == 0) {
			throw new Exception("PrimeCalcul - Pas de " + cle + " associé à la prime. Vérifier avec l'administrateur les paramétrages de cette prime");
		}
		return objets;
	}
	
	/**
	 * 
	 * @param periode
	 * @param affectations
	 * @return
	 */
	private NSArray intervallesCommunsAvecAffectations(IntervalleTemps periode,NSArray affectations) {
		affectations = EOSortOrdering.sortedArrayUsingKeyOrderArray(affectations, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		NSMutableArray intervallesCommuns = new NSMutableArray();
		// Déterminer pour la période courante les plages communes avec les objets passés en paramètre
		java.util.Enumeration e = affectations.objectEnumerator();
		while (e.hasMoreElements()) {
			EOAffectation affectation = (EOAffectation)e.nextElement();
			IntervalleTemps intervalle = IntervalleTemps.intersectionPeriodes(periode.debutPeriode(), periode.finPeriode(), affectation.dateDebut(), affectation.dateFin());
			if (intervalle != null) {
				LogManager.logDetail("Intervalle commun trouve " + intervalle);
				intervallesCommuns.addObject(intervalle);
			}
		}
		return intervallesCommuns;
	}
	// Recherche les intervalles non communs entre les intervalles passés en paramètres et des objets
	// il y en a forcément des périodes en commun car on a trouvé des objets
	private NSArray intervallesNonCommuns(IntervalleTemps periode,NSArray objetsAvecDuree,String cleDebut,String cleFin) {
		NSMutableArray intervallesNonCommuns = new NSMutableArray();
		// Déterminer pour pour la période courante les plages non communes avec les objets passés en paramètre
		java.util.Enumeration e = objetsAvecDuree.objectEnumerator();
		while (e.hasMoreElements()) {
			EOGenericRecord record = (EOGenericRecord)e.nextElement();
			NSTimestamp debutPeriode = (NSTimestamp)record.valueForKey(cleDebut);
			NSTimestamp finPeriode = (NSTimestamp)record.valueForKey(cleFin);
			if (DateCtrl.isAfter(debutPeriode,periode.debutPeriode())) {
				intervallesNonCommuns.addObject(new Utilitaires.IntervalleTemps(periode.debutPeriode(),DateCtrl.jourPrecedent(debutPeriode)));
			}
			if (finPeriode != null && (periode.finPeriode() == null || DateCtrl.isBefore(finPeriode,periode.finPeriode()))) {
				intervallesNonCommuns.addObject(new Utilitaires.IntervalleTemps(DateCtrl.jourSuivant(finPeriode),periode.finPeriode()));
			}
		}
		return new NSArray(intervallesNonCommuns);
	}
	private NSArray appliquerQuotiteTempsPartielPourIndividuEtResultats(EOIndividu individu,NSArray resultats,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSArray tempsPartiels = EOTempsPartiel.rechercherTempsPartielPourIndividuEtPeriode(individu.editingContext(), individu, debutPeriode, finPeriode);
		if (tempsPartiels.count() == 0) {
			return resultats;
		} else {
			// Trier les temps partiels par ordre de date croissante
			tempsPartiels = EOSortOrdering.sortedArrayUsingKeyOrderArray(tempsPartiels, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
			NSMutableArray nouveauxResultats = new NSMutableArray();
			java.util.Enumeration e = resultats.objectEnumerator();
			while (e.hasMoreElements()) {
				PrimeResultat resultat = (PrimeResultat)e.nextElement();
				LogManager.logDetail("resultat " + resultat);
				NSArray tempsPartielsPourResultat = rechercherTempsPartielsAvecIntersectionCommune(resultat.debutValidite(),resultat.finValidite(), tempsPartiels);
				if (tempsPartielsPourResultat.count() == 0) {
					// Pas de période de temps partiel sur la période du résultat => on garde le résultat complet
					LogManager.logDetail("Pas d'intersection on ajoute le résultat");
					nouveauxResultats.addObject(resultat);
				} else {
					double montantPourPeriode = resultat.montant().doubleValue();
					String formuleCalcul = resultat.formuleCalcul();
					int nbJoursTotal = DateCtrl.nbJoursEntre(resultat.debutValidite(), resultat.finValidite(), true,true);
					NSTimestamp dateDebut = resultat.debutValidite(), dateFin = resultat.finValidite();
					// Les temps partiels sont dans l'ordre croissant.
					for (int i = 0; i < tempsPartielsPourResultat.count(); i++) {
						boolean creerResultat = false;
						EOTempsPartiel tempsPartiel = (EOTempsPartiel)tempsPartielsPourResultat.objectAtIndex(i);
						LogManager.logDetail("tempsPartiel courant " + tempsPartiel);
						// Rechercher l'intersection de ce temps partiel avec ce résultat, elle existe forcément puisqu'on a sélectionné ce temps partiel
						IntervalleTemps intervalle = IntervalleTemps.intersectionPeriodes(dateDebut, dateFin, tempsPartiel.dateDebut(), tempsPartiel.dateFin());
						LogManager.logDetail("intervalle temps " + intervalle);
						// pour ne pas modifier le résultat courant et le traiter sur l'ensemble des temps partiels trouvés,
						// on duplique le résultat
						PrimeResultat resultatCourant = new PrimeResultat(resultat.individuID(),dateDebut, dateFin,null,null,null);
						if (DateCtrl.isBefore(dateDebut, intervalle.debutPeriode())) {
							// Il y a une période de temps plein avant le début du temps partiel
							// il faut changer la date de fin de validité du résultat et proratiser le montant
							resultatCourant.setFinValidite(DateCtrl.jourPrecedent(intervalle.debutPeriode()));
							int nbJours = DateCtrl.nbJoursEntre(resultatCourant.debutValidite(), resultatCourant.finValidite(), true,true);
							LogManager.logDetail("Période de temps plein initial, proratisation du montant suite à temps partiel, ratio : " + ((double)nbJours/nbJoursTotal));
							double montant = (montantPourPeriode * nbJours) / nbJoursTotal;
							resultatCourant.setMontant(new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP));
							resultatCourant.setFormuleCalcul(resultat.formuleCalcul() + "\nproratisé suite à temps partiel sur la période du " + DateCtrl.dateToString(resultat.debutValidite()) + " au " + DateCtrl.dateToString(resultat.finValidite()) + " avec un ratio de : " + nbJours + "/" + nbJoursTotal);
							creerResultat = true;
							nouveauxResultats.addObject(resultatCourant);
						}
						PrimeResultat nouveauResultat = resultatCourant;
						// Ajouter la période à temps partiel
						if (creerResultat) {
							nouveauResultat = new PrimeResultat(resultatCourant.individuID(),intervalle.debutPeriode(),intervalle.finPeriode(),null,null,null);
						} else {
							nouveauResultat.setDebutValidite(intervalle.debutPeriode());
							nouveauResultat.setFinValidite(intervalle.finPeriode());
						}
						// Il faut proratisé le montant sur la période et en fonction de la quotité
						int nbJours = DateCtrl.nbJoursEntre(intervalle.debutPeriode(), intervalle.finPeriode(), true,true);
						double montant = (montantPourPeriode * nbJours) / nbJoursTotal;
						LogManager.logDetail("Proratisation du montant avec un temps partiel à : " + tempsPartiel.quotite() + " (quotité financière : " + tempsPartiel.quotiteFinanciere() + "%), ratio : " + ((double)nbJours/nbJoursTotal));
						montant = montant * (tempsPartiel.quotiteFinanciere() / 100.00);	// 22/03/2011
						nouveauResultat.setMontant(new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP));
						nouveauResultat.setFormuleCalcul(formuleCalcul + "\nproratisé suite à un temps partiel à "+ tempsPartiel.quotite() +  " (quotité financière : " + tempsPartiel.quotiteFinanciere() + "%), sur la période du " + DateCtrl.dateToString(nouveauResultat.debutValidite()) + " au " + DateCtrl.dateToString(nouveauResultat.finValidite()));
						nouveauxResultats.addObject(nouveauResultat);
						dateDebut = DateCtrl.jourSuivant(intervalle.finPeriode());
						if (DateCtrl.isAfter(dateFin, intervalle.finPeriode()) && i == tempsPartielsPourResultat.count() - 1) {
							// Il s'agit du dernier temps partiel, il reste encore un reliquat à temps plein
							nouveauResultat = new PrimeResultat(resultatCourant.individuID(),dateDebut,dateFin,null,null,null);
							nbJours = DateCtrl.nbJoursEntre(nouveauResultat.debutValidite(), dateFin, true,true);
							montant = (montantPourPeriode * nbJours) / nbJoursTotal;
							LogManager.logDetail("Période de temps plein terminal, proratisation du montant suite à temps partiel, ratio : " + ((double)nbJours/nbJoursTotal));
							nouveauResultat.setMontant(new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP));
							nouveauResultat.setFormuleCalcul(resultat.formuleCalcul() + "\nproratisé suite à temps partiel sur la période du " + DateCtrl.dateToString(nouveauResultat.debutValidite()) + " au " + DateCtrl.dateToString(nouveauResultat.finValidite()) + " avec un ratio de : " + nbJours + "/" + nbJoursTotal);
							nouveauxResultats.addObject(nouveauResultat);
							LogManager.logDetail("resultat pour reliquat " + nouveauResultat);
						}
					}	
				}
			}
			return nouveauxResultats;
		}
	}
	private NSArray rechercherTempsPartielsAvecIntersectionCommune(NSTimestamp debutPeriode,NSTimestamp finPeriode,NSArray tempsPartiels) {
		NSMutableArray results = new NSMutableArray();
		java.util.Enumeration e = tempsPartiels.objectEnumerator();
		while (e.hasMoreElements()) {
			EOTempsPartiel tempsPartiel = (EOTempsPartiel)e.nextElement();
			if (IntervalleTemps.intersectionPeriodes(debutPeriode, finPeriode, tempsPartiel.dateDebut(), tempsPartiel.dateFin()) != null) {
				results.addObject(tempsPartiel);
			} else if (tempsPartiel.dateFin() == null || DateCtrl.isAfter(tempsPartiel.dateFin(), finPeriode)) {
				break;	// On a passé la date de fin
			}
		}
		return results;
	}
	// vérifie si les critères du plug-in sont remplis
	private PrimeResultatEligibilite verifierAvecPlugins(EOPrime prime, NSArray individusEligibles) throws Exception {
		try {
			Object moteurCalcul = LanceurCalcul.sharedInstance().instancierObjet(prime.primNomClasse());
			if (moteurCalcul instanceof InterfacePluginPrime) {
				InterfacePluginPrime plugin = (InterfacePluginPrime)moteurCalcul;
				if (!plugin.aCriteresEligibiliteSpecifiques()) {
					return new PrimeResultatEligibilite(individusEligibles,null);
				}
				// Vérifier les critères pour chaque individu. A ce stade, on sait déjà qu'il y a des individus retenus.
				// on ne va sélectionner que ceux qui remplissent les critères
				NSMutableArray individusRetenus = new NSMutableArray();
				java.util.Enumeration e = individusEligibles.objectEnumerator();
				String diagnostic = null;
				while (e.hasMoreElements()) {
					IndividuPourPrime individuPourPrime = (IndividuPourPrime)e.nextElement();
					String diagnosticPourIndividu = plugin.diagnosticRefusEgilibilite(individuPourPrime);
					if (diagnosticPourIndividu == null) {
						individusRetenus.addObject(individuPourPrime);
					} else {
						diagnostic = diagnosticPourIndividu;
					}
				}
				if (individusRetenus.count() > 0) {
					return new PrimeResultatEligibilite(individusRetenus,null);
				} else {
					return new PrimeResultatEligibilite(null,diagnostic);	// On aura le dernier diagnostic de refus
				}
			} else {
				throw new Exception("Un plug-in de calcul d'une prime doit implémenter l'interface InformationPourPluginPrime");
			}	
		} catch (Exception e) { throw e; }

	}
	// Constructeur : Un diagnostic de refus non null supprime tous les intervalles. 
	// Fournir un tableau non null et non vide supprime le diagnostic de refus

	private class PrimeResultatAvecPeriodes {
		private String diagnosticRefus;
		private NSArray intervallesValides;

		public PrimeResultatAvecPeriodes(String diagnosticRefus) {
			this.diagnosticRefus = diagnosticRefus;
		}
		public PrimeResultatAvecPeriodes(NSArray intervallesValides) {
			this.intervallesValides = intervallesValides;
		}
		public String diagnosticRefus() {
			return diagnosticRefus;
		}
		public NSArray intervallesValides() {
			return intervallesValides;
		}
	}
}

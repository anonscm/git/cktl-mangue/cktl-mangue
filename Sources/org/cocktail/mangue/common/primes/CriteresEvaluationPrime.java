/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes;

import org.cocktail.mangue.common.utilities.Utilitaires;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;

/** g&egrave; les crit&egrave;res d'&eacute;valuation des primes : corps, grades, typesContrat, structures dans des tableaux
 * Pour transf&eacute;rer des crit&egrave;res d'&eacute;valuation vers le serveur, il faut d'abord appeler la m&eacute;thode
 * transformerEnGlobalIDs.<BR>
 * Une fois des crit&egrave;res d'&eacute;valuation r&eacute;cup&eacute;r&eacute;s sur le serveur, il faut appeler la la m&eacute;thode
 * transformerEnObjetsMetier.
 * @author christine
 *
 */
public class CriteresEvaluationPrime implements NSKeyValueCoding,NSCoding {
	private NSArray corps, grades, typesContrat, typesPopulation,fonctions, structures;
	
	public CriteresEvaluationPrime() {
		super();
	}
	/** Accesseur utilis&eacute; pour le NSCoding */
	public CriteresEvaluationPrime(NSDictionary dict) {
		typesPopulation = (NSArray)dict.objectForKey("typesPopulation");
		corps = (NSArray)dict.objectForKey("corps");
		grades = (NSArray)dict.objectForKey("grades");
		fonctions = (NSArray)dict.objectForKey("fonctions");
		structures = (NSArray)dict.objectForKey("structures");
		typesContrat = (NSArray)dict.objectForKey("typesContrat");
	}
	public NSArray corps() {
		return corps;
	}
	public void setCorps(NSArray corps) {
		this.corps = corps;
	}
	public NSArray grades() {
		return grades;
	}
	public void setGrades(NSArray grades) {
		this.grades = grades;
	}
	public NSArray typesContrat() {
		return typesContrat;
	}
	public void setTypesContrat(NSArray typesContrat) {
		this.typesContrat = typesContrat;
	}
	public NSArray typesPopulation() {
		return typesPopulation;
	}
	public void setTypesPopulation(NSArray typesPopulation) {
		this.typesPopulation = typesPopulation;
	}
	public NSArray fonctions() {
		return fonctions;
	}
	public void setFonctions(NSArray fonctions) {
		this.fonctions = fonctions;
	}
	public NSArray structures() {
		return structures;
	}
	public void setStructures(NSArray structures) {
		this.structures = structures;
	}
	public void transformerEnGlobalIDs(EOEditingContext editingContext) {
		if (corps() != null) {
			setCorps(Utilitaires.tableauDeGlobalIDs(corps(), editingContext));
		}
		if (grades() != null) {
			setGrades(Utilitaires.tableauDeGlobalIDs(grades(), editingContext));
		}
		if (typesContrat() != null) {
			setTypesContrat(Utilitaires.tableauDeGlobalIDs(typesContrat(), editingContext));
		}
		if (typesPopulation() != null) {
			setTypesPopulation(Utilitaires.tableauDeGlobalIDs(typesPopulation(), editingContext));
		}
		if (fonctions() != null) {
			setFonctions(Utilitaires.tableauDeGlobalIDs(fonctions(), editingContext));
		}
		if (structures() != null) {
			setStructures(Utilitaires.tableauDeGlobalIDs(structures(), editingContext));
		}
	}
	public void transformerEnObjetsMetier(EOEditingContext editingContext) {
		if (corps() != null) {
			setCorps(Utilitaires.tableauObjetsMetiers(corps(), editingContext));
		}
		if (grades() != null) {
			setGrades(Utilitaires.tableauObjetsMetiers(grades(), editingContext));
		}
		if (typesContrat() != null) {
			setTypesContrat(Utilitaires.tableauObjetsMetiers(typesContrat(), editingContext));
		}
		if (typesPopulation() != null) {
			setTypesPopulation(Utilitaires.tableauObjetsMetiers(typesPopulation(), editingContext));
		}
		if (fonctions() != null) {
			setFonctions(Utilitaires.tableauObjetsMetiers(fonctions(), editingContext));
		}
		if (structures() != null) {
			setStructures(Utilitaires.tableauObjetsMetiers(structures(), editingContext));
		}
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Interface NSCoding
	public Class classForCoder() {
		return CriteresEvaluationPrime.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		NSMutableDictionary dict = new NSMutableDictionary();
		if (typesPopulation() != null)
			dict.setObjectForKey(typesPopulation(), "typesPopulation");
		if (corps() != null)
			dict.setObjectForKey(corps(), "corps");
		if (grades() != null)
			dict.setObjectForKey(grades(), "grades");
		if (fonctions() != null)
			dict.setObjectForKey(fonctions(), "fonctions");
		if (structures() != null)
			dict.setObjectForKey(structures(), "structures");
		if (typesContrat() != null)
			dict.setObjectForKey(typesContrat(), "typesContrat");
		
		coder.encodeObject(dict);
	}
	public static Class decodeClass() {
		return CriteresEvaluationPrime.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new CriteresEvaluationPrime((NSDictionary)coder.decodeObject());
	}

}

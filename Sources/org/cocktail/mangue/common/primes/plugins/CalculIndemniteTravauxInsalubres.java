/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
 package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeParamPerso;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** le montant de la prime de travaux insalubres est &eacute;gal &agrave; &agrave : tx_Categorie * nb demi_journees
 * 
 */
public class CalculIndemniteTravauxInsalubres extends PluginAvecParametresPersonnelsEtValidation {
	private final static String CATEGORIE_TRAVAUX = "CATTVINS";
	private final static String NB_DEMI_JOURNEES = "NBDJOTVI";
	private final static String MONTANT_POUR_CATEGORIE = "MOJTVIN";
	private final static String MONTANT_POUR_CATEGORIE_2A = "MOJTVI2A";
	private int maxCategorie = 3;

	/** pas de refus */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** Pas de montant utilis&eacute; */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime, EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		return null;
	}
	/** Le montant n'est pas modifiable */
	public boolean peutModifierMontantPrime() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** montant cat&eacute;gorie x nb demi-journ&eacute;es effectu&eacute;es par l'individu */
	public String modeCalculDescription() {
		return "montant catégorie x nb demi-journées effectuées par l'individu";
	}
	// Méthodes protégées
	/** V&eacute;rifie la cat&eacute;gorie choisie et si le nombre d'heures est un entier */
	protected String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime) {
		LogManager.logDetail(getClass().getName() + " - Vérification paramètres personnels");
		if (parametrePerso.code().equals(CATEGORIE_TRAVAUX)) {
			// Vérifier si c'est un entier compris entre 1 et 3 ou si c'est 1A
			try {
				int categorieNum = new Integer(parametrePerso.valeur()).intValue();
				if (categorieNum < 0 ||categorieNum > maxCategorie) {
					return "La catégorie de travaux est 1, 2, 2A ou 3";
				}
			} catch (Exception exc) {
				if (parametrePerso.valeur().equals("2A") == false)	{
					return "La catégorie de travaux est 1, 2, 2A ou 3";
				}
			}
		} else if (parametrePerso.code().equals(NB_DEMI_JOURNEES)) {
			try {
				int nbDemiJournees = new Integer(parametrePerso.valeur()).intValue();
				if (nbDemiJournees < 0) {
					return "Le nb de demi-journées doit être un entier positif";
				}
			} catch (Exception exc) {
				return "Le nb de demi-journées doit être un entier";
			}
		}	
		return null;
	}
	/** Utilise toujours les param&egrave;tres personnels */
	protected  boolean peutCalculerSansParametrePersonnel() {
		return false;
	}
	/** Pas de calcul &grave; partir de la personnalisation */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {		
	}
	/** Calcule le montant en fonction des param&egrave;tres personnels */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite,NSTimestamp finValidite) {
		LogManager.logDetail("CalculIndemniteTravauxInsalubres - Calcul du montant de l'attribution");
		// Rechercher la catégorie de travaux et le nb demi journées
		int nbDemiJournees = -1;
		String codeCategorie = null, categorie = null;
		java.util.Enumeration e = parametresPersos.objectEnumerator();
		// A ce stade, les paramètres personnels ont été définis
		while (e.hasMoreElements()) {
			EOPrimeParamPerso paramPerso = (EOPrimeParamPerso)e.nextElement();
			if (paramPerso.code().pcodCode().equals(NB_DEMI_JOURNEES)) {
				nbDemiJournees = new Integer(paramPerso.pppeValeur()).intValue();
			} else if (paramPerso.code().pcodCode().equals(CATEGORIE_TRAVAUX)) {
				categorie = paramPerso.pppeValeur();
				// Vérifier si c'est un entier compris entre 1 et 3 ou si c'est 1A
				try {
					int categorieNum = new Integer(categorie).intValue();
					codeCategorie = MONTANT_POUR_CATEGORIE + categorieNum;
				} catch (Exception exc) {
					if (categorie.equals("2A") == false)	{
						PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"La catégorie doit être entre 1, 1A, 2 ou 3", debutValidite, finValidite);
						return;
					} else {
						codeCategorie = MONTANT_POUR_CATEGORIE_2A;
					}
				}
			}
		}
		try {
			// Récupérer les paramètres définis sur cette période
			NSArray parametres = information.parametresPourCodeEtDates(codeCategorie,debutValidite, finValidite);
			e = parametres.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
				if (DateCtrl.isBefore(dateDebut,debutValidite)) {
					dateDebut = debutValidite;
				}
				if (dateFin != null && finValidite != null && DateCtrl.isAfter(dateFin, finValidite)) {
					dateFin = finValidite;
				}
				if (parametre.pparMontant() == null) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Contactez l'administrateur, le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
				} else {
					double montant = parametre.pparMontant().doubleValue() * nbDemiJournees;
					String formuleCalcul = "Montant demi-journée catégorie " + categorie + ", nb demi-journées " + nbDemiJournees + "\nCalcul :" + parametre.pparMontant().doubleValue() + " x " + nbDemiJournees;
					BigDecimal montantRetour = new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP);
					PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montantRetour, dateDebut, dateFin,formuleCalcul);
					LogManager.logDetail(formuleCalcul);
				}
			}
		} catch (Exception exc) {
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);

		}
	}

}

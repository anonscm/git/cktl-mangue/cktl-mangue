/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
 package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;
import org.cocktail.mangue.modele.grhum.EOGrade;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Le montant annuel de l'IFTS est un montant personnalis&eacute; qui ne peut exc&eacute;der un certain nombre de fois le montant moyen
 * 
 */
public class CalculIFTS extends PluginAvecParametresPersonnelsEtValidation {
	private final static String COEFFICIENT_MULTIPLICATEUR = "COEFIFTS";
	private final static String MONTANT_MOYEN = "MOMYIFTS";
	private final static String CATEGORIE_PRIME = "CATINDEM";

	/** pas de refus */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** Pas de v&eacute;rification de validit&eacute; car seulement un montant */
	public String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime) {
		return null;
	}
	/** l'IFTS est un montant personnalis&eacute; */
	public String modeCalculDescription() {
		return "l'IFTS est un montant personnalisé";
	}
	/** V&eacute;rifie si le montant d&eacute;passe un certain nombre de fois (param&egrave;tre avec code COEFIFTS), le montant moyen */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		if (montant == null) {
			return "CalculIFTS - Le montant à vérifier est nul";
		}
		double montantReel = montant.doubleValue();
		try {
			NSArray montantsMoyens = montantsMoyensPourGradeEtPeriode(individuPourPrime.grade(),individuPourPrime.periodeDebut(),individuPourPrime.periodeFin());
			LogManager.logDetail("CalculIFTS - Grade individu " + individuPourPrime.grade().cGrade() + ", montant vérifié : " + montant);
			NSArray coefficients = parametresValidesPourCodePeriodeEtQualifier(individuPourPrime.individu().editingContext(), COEFFICIENT_MULTIPLICATEUR, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin(),null);
			if (coefficients.count() == 0) {
				return "CalculIFTS - Contactez l'administrateur, pas de coefficient multiplicateur pour l'IFTS pour la période démarrant le " + DateCtrl.dateToString(individuPourPrime.periodeDebut());
			}
			// Dterminer le coefficient minimum parmi tous les coefficients
			int coefficient = 0;
			java.util.Enumeration e = coefficients.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				if (parametre.pparEntier() == null) {
					return "CalculIFTS - Contactez l'administrateur, pas de coefficient multiplicateur pour l'IFTS pour la période démarrant le " + parametre.debutValiditeFormatee();
				}
				int valeur = parametre.pparEntier().intValue();
				if (coefficient == 0 || valeur < coefficient) {
					coefficient = valeur;
				}
			}
			// On peut maintenant vrifier les montants
			e = montantsMoyens.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				if (parametre.pparMontant() == null) {
					return "CalculIFTS - Contactez l'administrateur, pas de montant dfini pour l'IFTS pour la période démarrant le " + parametre.debutValidite();
				}
				double montantMoyen = parametre.pparMontant().doubleValue();
				if (montantReel > montantMoyen * coefficient) {
					return "CalculIFTS - Le montant de l'individu (" + montant + ") est supérieur à " + coefficient + " fois le montant moyen (" + montantMoyen + ")\nVeuillez changer le montant";
				}
			}
			return null;
		} catch (Exception exc) {
			return exc.getMessage();
		}
	}
	// Méthodes protégées
	/** Peut calculer sans param&egrave;tre personnel */
	protected boolean peutCalculerSansParametrePersonnel() {
		return true;
	}
	/** pas de gestion des param&egrave;tres personnels */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
	}
	/** En l'absence de param&grave;tre personnel, retourne les montants moyens pour la p&eacute;riode */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information, EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {
		LogManager.logDetail("CalculIFTS - Calcul du montant de l'attribution");
		try {
			// Commencer par vérifier si un montant personnel a été fourni
			if (personnalisation != null) {
				if (personnalisation.pmpeMontant() == null) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Pas de montant dans la personnalisation ", debutValidite, finValidite);	
				} else {
					LogManager.logDetail("CalculIFTS - Montant " + personnalisation.pmpeMontant());
					String formuleCalcul = "Montant annuel personnalisé : " + personnalisation.pmpeMontant();
					LogManager.logDetail(formuleCalcul);
					PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),personnalisation.pmpeMontant(), information.debutPeriode(), information.finPeriode(),formuleCalcul);
				}
			} else {
				NSArray montantsMoyens = montantsMoyensPourGradeEtPeriode(information.gradeIndividu(), debutValidite, finValidite);
				java.util.Enumeration e = montantsMoyens.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
					NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
					if (DateCtrl.isBefore(dateDebut,debutValidite)) {
						dateDebut = debutValidite;
					}
					if (dateFin == null || (dateFin != null && finValidite != null && DateCtrl.isAfter(dateFin, finValidite))) {
						dateFin = finValidite;
					}
					if (parametre.pparMontant() == null) {
						PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
					} else {
						LogManager.logDetail("montant moyen " + parametre.pparMontant());
						double montant = parametre.pparMontant().doubleValue();
						if (montant == 0) {
							LogManager.logDetail("CalculIFTS - Montant moyen " + montant);
							PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculIFTS - Contactez l'administrateur, Le montant moyen pour la période  est nul", dateDebut, dateFin);
						} else {
							BigDecimal montantRetour = new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP);
							String formuleCalcul = "Montant Moyen annuel : " + montantRetour;
							LogManager.logDetail(formuleCalcul);
							PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montantRetour, dateDebut, dateFin,formuleCalcul);
						}
					}
				}
			}
		} catch (Exception exc) {
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}
	}
	// Mthodes privées
	private NSArray montantsMoyensPourGradeEtPeriode(EOGrade grade,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		// Vérifier qu'on et bien entre les taux moyen et exceptionnel en recherchant ces paramètres pour le grade de l'individu
		if (grade == null) {
			throw new Exception("CalculIFTS - Grade inconnu");
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("grade = %@", new NSArray(grade));		
		NSArray parametresPourCategories = parametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), CATEGORIE_PRIME, debutPeriode, finPeriode,qualifier);
		if (parametresPourCategories.count() == 0) {
			throw new Exception("CalculIFTS - Contactez l'administrateur, pas de paramètre défini pour le grade " + grade.cGrade());
		}
		// Normalement, on ne doit trouver qu'un paramètre
		EOPrimeParam paramCategorie = (EOPrimeParam)parametresPourCategories.objectAtIndex(0);
		if (paramCategorie.pparEntier() == null) {
			throw new Exception("CalculIFTS - Contactez l'administrateur, pas de catégorie définie pour le grade " + grade.cGrade());
		}
		int categorie = paramCategorie.pparEntier().intValue();
		if (categorie < 1 || categorie > 3) {
			String message = "CalculIFTS - " + categorie + " : catgorie invalide pour le grade " + grade.cGrade();
			LogManager.logDetail(message);
			throw new Exception(message);
		}
		qualifier = EOQualifier.qualifierWithQualifierFormat("pparEntier = %@", new NSArray(new Integer(categorie)));
		NSArray parametresMontant = parametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), MONTANT_MOYEN, debutPeriode, finPeriode,qualifier);
		String periode = "du " + DateCtrl.dateToString(debutPeriode);
		if (finPeriode != null) {
			periode += " au " + DateCtrl.dateToString(finPeriode);
		}
		if (parametresMontant.count() == 0) {
			throw new Exception("CalculIFTS - Pas de montant moyen IFTS pour la periode " + periode);
		} else if (parametresValidesSurPeriode(parametresMontant,debutPeriode,finPeriode) == false) {
			throw new Exception("CalculIFTS - Les montants moyens IFTS ne couvrent pas toute la periode " + periode);
		}
		return parametresMontant;
	}

}

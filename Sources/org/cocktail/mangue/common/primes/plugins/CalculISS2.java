/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeCode;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeParamPerso;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


/** L'indemnit&eacute; de suj&eacute;tions sp&eacute;ciales et de travaux suppl&eacute;mentaires est fonction du nombre
 * d'heures effectu&eacute;es (dans une limite de 250h), et du moment o&ugrave; les heures sont effectu&eacute;es (entre 20h et 7h ou entre 7h et 20h, week-end ou jours f&eacute;ri&eacute;s).
 * @author christine
 *
 */
public class CalculISS2 extends PluginAvecParametresPersonnelsEtValidation {
	private final static String	CATEGORIE_TVX = "CATTVISS";
	private final static String	NB_HEURES_ISS = "NBHEUISS";
	private final static String	MONTANT_HORAIRE_ISS = "MOHISSP";
	private final static String	NB_HEURES_ISS_MAX = "NBHISSMX";
	private final static String CODE_INDEMNITE = "5092";
	private final static String CODE_INDEMNITE_FORFAITAIRE = "1092";

	private int nbHeuresTotalesPourISS2 = 0;

	protected void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {
		// A besoin des paramètres personnels
	}

	protected void effectuerCalculPourDates(InformationPourPluginPrime information, NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
		LogManager.logDetail("CalculISS2 - Calcul du montant de l'indemnité");
		// Récupérer les paramètres de montants pour les fonctions
		EOEditingContext editingContext = information.individu().editingContext();
		// Extraire les paramètres de l'individu
		int nbHeures = 0;
		nbHeuresTotalesPourISS2 = 0;
		double montantHoraire = 0.00, montantIndemnite = 0.00;
		String formuleCalcul ="";
		try {
			if (parametresPersos != null && parametresPersos.count() > 0) {
				java.util.Enumeration e = parametresPersos.objectEnumerator();
				// A ce stade, les paramètres personnels ont été définis et ont été vérifiés
				while (e.hasMoreElements()) {
					EOPrimeParamPerso paramPerso = (EOPrimeParamPerso)e.nextElement();
					if (paramPerso.code().pcodCode().equals(NB_HEURES_ISS)) {
						nbHeures = new Integer(paramPerso.pppeValeur()).intValue();
						// A ce stade, le nombre d'heures ne dépasse pas la limite max
						LogManager.logDetail("CalculISS2 - Nombre d'heures effectuée " + nbHeures);
					} else if (paramPerso.code().pcodCode().equals(CATEGORIE_TVX)) {
						// Rechercher le taux de majoration pour cette catégorie si il ne s'agit pas de la catégorie 1
						String code = MONTANT_HORAIRE_ISS + paramPerso.pppeValeur();
						NSArray parametres = parametresValidesPourCodePeriodeEtQualifier(editingContext, code, debutValidite, finValidite,null);
						if (parametres.count() == 0) {
							throw new Exception("CalculISS2 - Contactez l'administrateur, le paramètre de montant horaire pour le code " + code + " n'est pas défini");
						}
						parametres = EOSortOrdering.sortedArrayUsingKeyOrderArray(parametres, new NSArray(EOSortOrdering.sortOrderingWithKey("pparMontant",EOSortOrdering.CompareAscending)));
						// On prend le paramètre avec le montant le plus bas
						EOPrimeParam parametre = (EOPrimeParam)parametres.objectAtIndex(0);
						montantHoraire = parametre.pparMontant().doubleValue();
						LogManager.logDetail("CalculISS2 - Montant horaire " + montantHoraire);
					}
				}
				// Effectuer le calcul
				formuleCalcul = "Montant horaire de l'heure supplémentaire : " + montantHoraire + "\nNb Heures effectuées : " + nbHeures + "\n";
				formuleCalcul += "" + montantHoraire + " x " + nbHeures;
				montantIndemnite = montantHoraire * (double)nbHeures;
				LogManager.logDetail("CalculISS2 - " + formuleCalcul);
			} else {
				formuleCalcul = "Montant non évalué en l'absence de paramètres personnels";
			}
			PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),new BigDecimal(montantIndemnite).setScale(2,BigDecimal.ROUND_UP), debutValidite, finValidite,formuleCalcul);
			return;
		} catch (Exception exc) {
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}

	}
	/** ne peut pas calculer sans param&egrave;tre personnel */
	protected boolean peutCalculerSansParametrePersonnel() {
		return false;
	}
	/** Le nombre d'heures effectu&eacute;es sur toute l'ann&eacute;e doit &ecirc;tre inf&eacute;rieur &agrave; une limite max */
	public boolean aCriteresEligibiliteSpecifiques() {
		return true;
	}
	public String diagnosticRefusEgilibilite(IndividuPourPrime individuPourPrime) {
		EOEditingContext editingContext = individuPourPrime.individu().editingContext();
		NSArray attributions = EOPrimeAttribution.rechercherAttributionsPourIndividuEtExercice(editingContext, individuPourPrime.individu(),new Integer(DateCtrl.getYear(individuPourPrime.periodeDebut())));
		boolean found = false;
		java.util.Enumeration e = attributions.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeAttribution attrib = (EOPrimeAttribution)e.nextElement();
			if (attrib.prime().primCodeIndemnite().equals(CODE_INDEMNITE_FORFAITAIRE)) {
				found = true;
				break;
			}
		}
		if (!found) {
			return "L'indemnité ne peut être accordée que si l'indemnité forfaitaire de sujétions pour conducteurs (1092) est accordée";
		}
		return verifierNbHeuresEffectueesPourPeriodeEtIndividu(editingContext, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin(), individuPourPrime.individu(),0);
	}
	public String modeCalculDescription() {

		return "nb Heures x (taux pour catégorie de travaux/100)";
	}
	/** Non renouvelable */
	public boolean peutEtreRenouvellee() {
		return false;
	}
	/** Non modifiable, seuls les param&egrave;tres personnels sont modifiables */
	public boolean peutModifierMontantPrime() {
		return false;
	}
	protected String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime) {
		LogManager.logDetail("CalculISS2 - Vérification du paramètre personnel " + parametrePerso.code());
		EOEditingContext editingContext = individuPourPrime.individu().editingContext();
		if (parametrePerso.code().equals(NB_HEURES_ISS)) {
			try {
				int nbHeures = new Integer(parametrePerso.valeur()).intValue();
				if (nbHeures <= 0) {
					return "CalculISS2 - Le nombre d'heures est un nombre entier positif";
				}
				// Vérifier si il a dépassé le nombre d'heures maximum
				return verifierNbHeuresEffectueesPourPeriodeEtIndividu(editingContext,individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),individuPourPrime.individu(),nbHeures);
			} catch (Exception exc) {
				return "CalculISS2 - Le nombre d'heures est un nombre entier";
			} 
			
		} else if (parametrePerso.code().equals(CATEGORIE_TVX)) {
			// Vérifier que le paramètre vaut 1 ou 2
			if (parametrePerso.valeur().equals("1") == false && parametrePerso.valeur().equals("2") == false) {
				return "CalculISS2 - La catégorie de travaux ne peut être que la valeur 1 ou 2 " + parametrePerso.valeur();
			}
			NSArray codes = EOPrimeCode.rechercherCodesAvecCode(editingContext, MONTANT_HORAIRE_ISS + parametrePerso.valeur());
			if (codes.count() == 0) {
				return "CalculISS2 - Contactez l'administrateur, pas de code correspondant au code " + CATEGORIE_TVX + parametrePerso.valeur();
			}
			EOPrimeCode code = (EOPrimeCode)codes.objectAtIndex(0);
			NSArray parametres = EOPrimeParam.rechercherParametresValidesPourCodeEtPeriode(editingContext,code, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
			if (parametres.count() == 0) {
				return "CalculISS2 - Contactez l'administrateur, pas de paramètre défini pour le code " + CATEGORIE_TVX + parametrePerso.valeur();
			}
			java.util.Enumeration e = parametres.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				if (parametre.pparMontant() == null || parametre.pparMontant().doubleValue() == 0) {
					return "CalculISS2 - Contactez l'administrateur, le montant du paramètre " + parametre.pparLibelle() + " est nul";
				}
			}
		}
		return null;	
	}
	/** Pas de v&eacute;rification des montants */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime, EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		return null;
	}
	// Méthodes privées
	private void calculerNbHeuresTotales(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		nbHeuresTotalesPourISS2 = 0;
		NSArray paramsPerso = EOPrimeParamPerso.rechercherParametresPersonnelsValidesPourIndividuCodeEtAnnee(editingContext,individu,NB_HEURES_ISS,DateCtrl.getYear(debutPeriode));
		if (paramsPerso.count() > 0) {
			EOPrime prime = EOPrime.rechercherPrimePourCodeIndemnite(editingContext, CODE_INDEMNITE, debutPeriode, finPeriode);
			if (prime == null) {
				return;
			}
			// Rechercher aussi la personnalisation pour la période afin de ne pas prendre en compte la valeur du paramètre
			// personnel lié à cette personnalisation, il se peut que cette valeur ait été modifiée
			EOPrimePersonnalisation personnalisationCourante = EOPrimePersonnalisation.rechercherPersonnalisationValidePourIndividuPrimeEtPeriode(editingContext, individu, prime, debutPeriode, finPeriode);
			java.util.Enumeration e = paramsPerso.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParamPerso param = (EOPrimeParamPerso)e.nextElement();
				if (param.personnalisation() != personnalisationCourante) {
					nbHeuresTotalesPourISS2 += new Integer(param.pppeValeur()).intValue();
				}
			}
		}
	}
	private String verifierNbHeuresEffectueesPourPeriodeEtIndividu(EOEditingContext editingContext,NSTimestamp dateDebut, NSTimestamp dateFin,EOIndividu individu, int nbHeuresPourPeriode) {
		// Rechercher les paramètres personnels pour l'année en cours
		NSArray parametres = parametresValidesPourCodePeriodeEtQualifier(editingContext, NB_HEURES_ISS_MAX, dateDebut, dateFin,null);
		if (parametres.count() == 0) {
			return "CalculISS2 - Contactez l'administrateur, le paramètre de nombre d'heures maximal n'est pas défini";
		}
		EOPrimeParam parametre = (EOPrimeParam)parametres.objectAtIndex(0);
		if (parametre.pparEntier() == null) {
			return "CalculISS2 - Contactez l'administrateur, le nombre d'heures maximum à effectuer pour le code " + NB_HEURES_ISS_MAX + " n'est pas défini";
		}
		
		calculerNbHeuresTotales(editingContext, individu, dateDebut,dateFin);
		if (nbHeuresPourPeriode == 0) {
			if (nbHeuresTotalesPourISS2 ==  parametre.pparEntier().intValue()) {
				return "CalculISS2 - L'individu a effectué le nombre d'heures maximum, il ne peut plus bénéficier de cette indemnité";
			}
		} else {
			int nbHeuresEnTrop = nbHeuresTotalesPourISS2 + nbHeuresPourPeriode - parametre.pparEntier().intValue();
			if (nbHeuresEnTrop >  0) {
				return "CalculISS2 - L'individu dépasse le nombre d'heures maximum de " + nbHeuresEnTrop + " heures, Veuillez limiter le nombre d'heures effectuées à cette valeur";
			}
		}
		return null;
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeParamPerso;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** Le montant annuel de la PFI est un montant personnalis&eacute; qui ne peut exc&eacute;der 25% du montant moyen. Ce dernier est calcul&eacute;
 * en fonction d'un multiple de 1/10000&grave;me du traitement brut annuel de l'indice 585
 * 
 */
public class CalculPFI extends PluginAvecParametresPersonnelsEtValidation {
	private final static String	FONCTION_POUR_NIVEAU_HIERARCHIQUE = "FONPFINI";		// Code pour fonction et corps ou grade
	private final static String	INDICE_REFERENCE = "INDBPFI";
	private final static String	QUOTITENT_TRAITEMENT = "TXTRTPFI";
	private final static String	TAUX_FONCTION_DUREE = "TXPFIFID";
	private final static String	TAUX_MAJORATION_MAXIMUM = "TXMAJPFI";
	private final static String	FONCTION_INDIVIDU = "FONINPFI";
	private final static String	PREMIERE_AFFECTATION_PFI = "DEBAFPFI";
	private final static String	VALEUR_POINT_INDICE = "VALPTIND";
	private final static String MONTANT_INDIVIDUEL = "MOINDPFI";

	/** pas de refus */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** ((indice r&eacute;f&eacute;rence x valeur du point d'indice) x taux pour dur&eacute;e d'attribution de la PFI / quotient traitement) * 12 */
	public String modeCalculDescription() {
		return "((indice référence x valeur du point d'indice) x taux pour durée d'attribution de la PFI / quotient traitement) * 12";
	}
	/** V&eacute;rifie que le montant ne d&eacute;passe pas un montant maximum calcul&eacute; &agrave; partir du montant moyen */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		LogManager.logDetail(getClass().getName() + " - Vérification montant");
		try {
			NSArray personnalisations = EOPrimePersonnalisation.rechercherPersonnalisationsValidesPourIndividuPrimeEtPeriode(prime.editingContext(), individuPourPrime.individu(), prime, debutPeriode, finPeriode);
			java.util.Enumeration e = personnalisations.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimePersonnalisation personnalisation = (EOPrimePersonnalisation)e.nextElement();
				NSArray parametresPersonnels = EOPrimeParamPerso.rechercherParametresPersonnelsValidesPourPersonnalisation(prime.editingContext(), personnalisation);
				String message = verifierMontantPourIndividu(individuPourPrime.individu(),parametresPersonnels,montant,debutPeriode,finPeriode);
				if (message != null) {
					return message;
				}
			}
			return null;
		} catch (Exception e) {
			LogManager.logException(e);
			return e.getMessage();
		}
	}
	/** Surcharge du parent pour valider le montant : v&eacute;rifie que le format de la date de premi&egrave;re affectation,
	 * le montant individuel et la fonction choisie sont valides. Le montant individuel doit &ecicr;tre inf&eacute;rieur &agrave;
	 * un montant maximum calcul&eacute; &agrave; partir du montant moyen */
	public String verifierParametresPersonnels(NSArray parametresPersonnels,IndividuPourPrime individuPourPrime) {
		LogManager.logDetail(getClass().getName() + " - Vérification paramètres personnels");
		if (parametresPersonnels == null || parametresPersonnels.count() == 0) {
			return null;
		}
		java.util.Enumeration e = parametresPersonnels.objectEnumerator();
		BigDecimal montant = null;
		// Commencer par vérifier la validité des types de données
		while (e.hasMoreElements()) {
			ParametrePersonnel parametrePerso = (ParametrePersonnel)e.nextElement();
			if (parametrePerso.valeur() == null || parametrePerso.valeur().length() == 0) {
				return "CalculPFI - Vous devez fournir une valeur pour le paramètre personnel " + parametrePerso.code();
			}
			if (parametrePerso.code().equals(MONTANT_INDIVIDUEL)) {
				try {
					montant = new BigDecimal(parametrePerso.valeur());
				} catch (Exception exc) {
					return "CalculPFI - Le montant n'est pas une valeur numérique";
				}
			} else {
				String erreur = verifierParametrePourIndividu(parametrePerso,individuPourPrime);
				if (erreur != null) {
					LogManager.logDetail(erreur);
					return erreur;
				}
			}
		}
		if (montant == null || montant.doubleValue() == 0) {
			return null;	// Pas de vérifications
		}
		// Vérifier si le montant ne dépasse pas la valeur maximum autorisée
		try {
			return verifierMontantPourIndividu(individuPourPrime.individu(),parametresPersonnels, montant,individuPourPrime.periodeDebut(),individuPourPrime.periodeFin());
		} catch (Exception exc) {
			LogManager.logException(exc);
			return exc.getMessage();
		}
	}
	// Méthodes protégées
	/** V&eacute;rifie que la date de premi&egrave;re affectation et le montant sont valides */
	protected String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime) {
		LogManager.logDetail("CalculPFI - Vérification du paramètre personnel " + parametrePerso.code());
		if (parametrePerso.code().equals(PREMIERE_AFFECTATION_PFI)) {
			if (DateCtrl.stringToDate(parametrePerso.valeur()) == null) {
				return "Le format de la date de première affectation n'est pas valide. Veuillez modifier la date";
			}
		} else if (parametrePerso.code().equals(FONCTION_INDIVIDU)) {
			// Rechercher les fonctions pour le corps ou le grade
			NSMutableArray qualifiers = new NSMutableArray();
			String message = "";
			if (individuPourPrime.corps() != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("corps = %@", new NSArray(individuPourPrime.corps())));
				message = " le corps " + individuPourPrime.corps().cCorps();
			}
			if (individuPourPrime.grade() != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("grade = %@", new NSArray(individuPourPrime.grade())));
				if (message.length() > 0) {
					message += " ou ";
				}
				message = " le grade " + individuPourPrime.grade().cGrade();
			}
			EOQualifier qualifier = new EOOrQualifier(qualifiers);
			NSArray params = (NSArray)EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(individuPourPrime.individu().editingContext(), FONCTION_POUR_NIVEAU_HIERARCHIQUE, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin(), qualifier);
			if (params.count() == 0) {
				return "CalculPFI - Contactez l'administrateur, pas de fonction définie pour " + message;
			}
			java.util.Enumeration e = params.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				if (parametre.fonction().libelleLong().equals(parametrePerso.valeur())) {
					return null;
				}
			}
			// On n'a pas trouvé la fonction
			return "CalculPFI - Le corps ou le grade de cet individu ne lui permettent pas d'avoir la fonction " + parametrePerso.valeur();
		}
		return null;
	}
	/** Sans effet, plusieurs paramètres requis */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {
		// On doit saisir plusieurs paramètres personnels
	}
	/** Montant personnel ou montant moyen calcul&eacute; comme un nombre de fois du traitement brut annuel de l'indice 585 / 10.000 */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information, NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
		LogManager.logDetail("CalculPFI - Calcul du montant de l'attribution");
		try {
			// Commencer par vérifier si un montant personnel a été fourni, déterminer la fonction et la date de première attribution
			double montantPerso = 0;
			NSTimestamp datePremiereAttribution = null;
			String fonction = null;
			boolean peutCalculerMontantMoyen = false;
			double montantReel = 0;
			String formuleCalcul ="";
			if (parametresPersos != null && parametresPersos.count() > 0) {
				java.util.Enumeration e = parametresPersos.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPrimeParamPerso parametrePerso = (EOPrimeParamPerso)e.nextElement();
					if (parametrePerso.code().pcodCode().equals(MONTANT_INDIVIDUEL)) {
						montantPerso = new Double(parametrePerso.pppeValeur()).doubleValue();
						LogManager.logDetail("CalculPFI - Montant individuel " + montantPerso);
					} else if (parametrePerso.code().pcodCode().equals(FONCTION_INDIVIDU)) {
						fonction = parametrePerso.pppeValeur();
						LogManager.logDetail("CalculPFI - Fonction " + fonction);
					} else if (parametrePerso.code().pcodCode().equals(PREMIERE_AFFECTATION_PFI)) {
						datePremiereAttribution = DateCtrl.stringToDate(parametrePerso.pppeValeur());
						LogManager.logDetail("CalculPFI - Date premiere affectation " + datePremiereAttribution);
					}
				}
				if (montantPerso == 0) {
					if (fonction == null || datePremiereAttribution == null) {
						formuleCalcul = "Montant non évalué en l'absence de paramètres personnels";
						montantReel = 0;
					} else {
						peutCalculerMontantMoyen = true;
					}
				} else {
					formuleCalcul = "Montant annuel personnalisé : ";
					montantReel = montantPerso;
				}
			} else {
				formuleCalcul = "Montant non évalué en l'absence de paramètres personnels";
			}
			NSArray tauxPourFonction = information.parametresPourCodeFonctionEtDates(TAUX_FONCTION_DUREE, fonction,debutValidite, finValidite);
			NSArray valeursPointIndice = information.parametresPourCodeEtDates(VALEUR_POINT_INDICE, debutValidite, finValidite);
			NSArray indices = information.parametresPourCodeEtDates(INDICE_REFERENCE, debutValidite, finValidite);
			NSArray quotients = information.parametresPourCodeEtDates(QUOTITENT_TRAITEMENT, debutValidite, finValidite);
			if ((montantReel == 0 && !peutCalculerMontantMoyen) || montantReel > 0) {
				BigDecimal montantRetour = new BigDecimal(montantReel).setScale(2, BigDecimal.ROUND_HALF_UP);
				formuleCalcul += montantRetour;
				// Calculer le montant moyen total pour le retourner dans la formule de calcul
				NSArray resultats = calculerMontantsMoyensPourIndividu(information.individu(), tauxPourFonction, valeursPointIndice, indices, quotients, datePremiereAttribution, fonction, debutValidite, finValidite,false);
				if (resultats != null) {
					BigDecimal montantMoyenTotal = new BigDecimal(0);
					java.util.Enumeration e = resultats.objectEnumerator();
					while (e.hasMoreElements()) {
						BigDecimal montantMoyen = (BigDecimal)e.nextElement();
						montantMoyenTotal = montantMoyenTotal.add(montantMoyen);
					}
					montantMoyenTotal = montantMoyenTotal.setScale(2, BigDecimal.ROUND_HALF_UP);
					formuleCalcul += "\nMontant moyen annuel : " + montantMoyenTotal;
				}
				LogManager.logDetail("CalculPFI - " + formuleCalcul);
				PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montantRetour, debutValidite, finValidite,formuleCalcul);
				return;
			}
			calculerMontantsMoyensPourIndividu(information.individu(),tauxPourFonction,valeursPointIndice,indices,quotients,datePremiereAttribution,fonction,debutValidite,finValidite,true);
		} catch (Exception exc) {
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}		
	}
	protected boolean peutCalculerSansParametrePersonnel() {
		return false;
	}
	// Méthodes privées
	private NSArray calculerMontantsMoyensPourIndividu(EOIndividu individu,NSArray tauxPourFonction,NSArray valeursPointIndice,NSArray indices,NSArray quotients,NSTimestamp datePremiereAttribution,String fonction,NSTimestamp debutValidite,NSTimestamp finValidite,boolean ajouterAPrimeCalcul) throws Exception {
		// Vérifier que les taux sont correctement définis
		String erreur = verifierTaux(tauxPourFonction,valeursPointIndice,indices,quotients);
		if (erreur != null) {
			if (ajouterAPrimeCalcul) {
				PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(individu,"CalculPFI - Contactez l'administrateur, " + erreur, debutValidite, finValidite);
			} else {
				throw new Exception(erreur);
			}
			return null;
		}
		// Tous les paramètres sont bons, on peut maintenant déterminer tous les intervalles pour lesquels il faut des attributions
		// on trouvera alors juste un paramètre pour chaque intervalle
		NSArray intervalles = new NSArray(new Utilitaires.IntervalleTemps(debutValidite,finValidite));
		intervalles = determinerIntervallesPourIntervallesEtParametres(intervalles, tauxPourFonction);
		intervalles = determinerIntervallesPourIntervallesEtParametres(intervalles, valeursPointIndice);
		intervalles = determinerIntervallesPourIntervallesEtParametres(intervalles, indices);
		intervalles = determinerIntervallesPourIntervallesEtParametres(intervalles, quotients);
		// On  trie les fonctions ordre de validité et de taux croissants car les taux de PFI sont croissants en fonction
		// de la durée d'attribution
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("pparTaux", EOSortOrdering.CompareAscending));
		tauxPourFonction = EOSortOrdering.sortedArrayUsingKeyOrderArray(tauxPourFonction, sorts);
		NSMutableArray resultats = new NSMutableArray();
		java.util.Enumeration e = intervalles.objectEnumerator();
		while (e.hasMoreElements()) {
			IntervalleTemps intervalle = (IntervalleTemps)e.nextElement();
			EOPrimeParam valeurPointIndice = parametrePourPeriode(valeursPointIndice,intervalle.debutPeriode(),intervalle.finPeriode());
			double valeurPoint = valeurPointIndice.pparMontant().doubleValue();
			// Calculer la durée d'attribution en mois depuis la date de début d'attribution au jour précédent le démarrage de l'attribution
			int nbMoisAttribution = DateCtrl.calculerDureeEnMois(datePremiereAttribution, DateCtrl.jourPrecedent(intervalle.debutPeriode()),true).intValue();
			EOPrimeParam paramTauxPourFonction = tauxPourFonctionEtDuree(tauxPourFonction, nbMoisAttribution, intervalle.debutPeriode(), intervalle.finPeriode());
			int taux = paramTauxPourFonction.pparTaux().intValue();
			EOPrimeParam paramIndice = parametrePourPeriode(indices, intervalle.debutPeriode(),intervalle.finPeriode());
			String indice = paramIndice.pparIndice();
			EOPrimeParam paramQuotient = parametrePourPeriode(quotients,intervalle.debutPeriode(),intervalle.finPeriode());
			double quotient = paramQuotient.pparTaux().intValue();
			EOIndice indiceMajoreDebut = EOIndice.indiceMajorePourIndiceBrutEtDate(individu.editingContext(), indice, intervalle.debutPeriode());
			if (indiceMajoreDebut == null) {
				throw new Exception("CalculPFI - Contactez l'administrateur, l'indice majoré de l'indice brut " + indice + " n'est pas défini à la date " + intervalle.debutPeriode());
			}
			EOIndice indiceMajoreFin = EOIndice.indiceMajorePourIndiceBrutEtDate(individu.editingContext(), indice, intervalle.finPeriode());
			if (indiceMajoreFin == null) {
				String message ="";
				if (intervalle.finPeriode() != null) {
					message = "à la date " + DateCtrl.dateToString(intervalle.finPeriode());
				}
				throw new Exception("CalculPFI - Contactez l'administrateur, l'indice majoré de l'indice brut " + indice + " n'est pas défini " + message);
			}
			// On suppose qu'on ne trouve au maximum que deux valeurs de l'indice majoré
			int valeurIndiceMajoreDebut = indiceMajoreDebut.cIndiceMajore().intValue();
			int valeurIndiceMajoreFin = indiceMajoreFin.cIndiceMajore().intValue();
			NSTimestamp dateFin = intervalle.finPeriode();
			if (valeurIndiceMajoreDebut != valeurIndiceMajoreFin) {
				dateFin = indiceMajoreDebut.dFermeture();
			}
			String montantUtilise = "montant moyen en 1/" + quotient + " pour la fonction " + fonction + " : " + taux;
			double montant = ((valeurPoint * valeurIndiceMajoreDebut * taux) / quotient) * 12;	// Pour retourner un montant annuel
			String formuleCalcul = "Valeur Point Indice : " + valeurPoint + " - " + montantUtilise + " - indice majoré : " + valeurIndiceMajoreDebut  + "\nCalcul : ";
			formuleCalcul += "Montant annuel moyen - ((" + valeurPoint + " x " + valeurIndiceMajoreDebut + " x " + taux + ") / " + quotient + ") * 12)";
			BigDecimal montantRetour = new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP);
			LogManager.logDetail(formuleCalcul);
			if (ajouterAPrimeCalcul) {
				PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(individu,montantRetour, intervalle.debutPeriode(), dateFin,formuleCalcul); 	
			} else {
				resultats.addObject(montantRetour);
			}
			if (valeurIndiceMajoreDebut != valeurIndiceMajoreFin) {
				montant = ((valeurPoint * valeurIndiceMajoreFin * taux) / quotient) * 12;	// Pour retourner un montant annuel
				formuleCalcul = "Valeur Point Indice : " + valeurPoint + " - " + montantUtilise + " - indice majoré : " + valeurIndiceMajoreFin  + "\nCalcul : ";
				formuleCalcul += "Montant annuel moyen - ((" + valeurPoint + " x " + valeurIndiceMajoreFin + " x " + taux + ") / " + quotient + ") * 12)";
				montantRetour = new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP);
				LogManager.logDetail(formuleCalcul);
				if (ajouterAPrimeCalcul) {
					PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(individu,montantRetour, indiceMajoreFin.dMajoration(), intervalle.finPeriode(),formuleCalcul);
				} else {
					resultats.addObject(montantRetour);
				}
			}
		}
		return resultats;		
	}
	// Pour une période donnée, il y a plusieurs taux de valeur croissante. Un de ces taux n'a pas de durée pour indiquer
	// que c'est le taux à sélectionner pour une durée supérieure à la somme des durées des autres taux
	private EOPrimeParam tauxPourFonctionEtDuree(NSArray tauxPourFonction,int nbMoisAffectation, NSTimestamp debutValidite,NSTimestamp finValidite) throws Exception {
		int nbMoisTotal = 0;
		// Ils sont triés par ordre de date de validité croissante
		java.util.Enumeration e = tauxPourFonction.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
			if (IntervalleTemps.intersectionPeriodes(parametre.debutValidite(), parametre.finValidite(), debutValidite, finValidite) != null) {
				if (parametre.pparEntier() != null) {
					// Il s'agit de la durée en mois. Si la durée d'affectation est inférieure à cette durée totale, on a trouvé le bon taux
					nbMoisTotal += parametre.pparEntier().intValue();
					if (nbMoisAffectation < nbMoisTotal) {
						// On a trouvé le bon taux
						return parametre;
					}
				} else {
					return parametre;	// on est sur le dernier taux
				}
			}
		}
		return null;	// Ne devrait pas se produire car les intervalles communs ont été déterminés auparavant
	}
	// vérifie le montant d'un individu, retourne les montants moyens trouvés si le booléen verifierMontant est à false
	private String verifierMontantPourIndividu(EOIndividu individu,NSArray parametresPersonnels,BigDecimal montant,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		// Rechercher la fonction et la date de première attribution, à ce stade on sait que les valeurs si elles existent sont valides
		String fonction = null;
		NSTimestamp datePremiereAttribution = null;
		java.util.Enumeration e = parametresPersonnels.objectEnumerator();
		while (e.hasMoreElements()) {
			ParametrePersonnel parametre = (ParametrePersonnel)e.nextElement();
			if (parametre.code().equals(PREMIERE_AFFECTATION_PFI)) {
				datePremiereAttribution = DateCtrl.stringToDate(parametre.valeur());
			} else if (parametre.code().equals(FONCTION_INDIVIDU)) {
				fonction = parametre.valeur();
			}
		}
		if (fonction == null || datePremiereAttribution == null) {
			return "CalculPFI - Les paramètres personnels sont insuffisamment définis, il manque la fonction ou la date de première attribution";
		}
		EOEditingContext editingContext = individu.editingContext();
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("fonction.libelleLong = %@", new NSArray(fonction));
		NSArray tauxPourFonction = parametresValidesPourCodePeriodeEtQualifier(editingContext,TAUX_FONCTION_DUREE, debutPeriode, finPeriode,qualifier);
		NSArray valeursPointIndice = parametresValidesPourCodePeriodeEtQualifier(editingContext,VALEUR_POINT_INDICE, debutPeriode, finPeriode,null);
		NSArray indices = parametresValidesPourCodePeriodeEtQualifier(editingContext,INDICE_REFERENCE, debutPeriode, finPeriode,null);
		NSArray quotients = parametresValidesPourCodePeriodeEtQualifier(editingContext,QUOTITENT_TRAITEMENT, debutPeriode, finPeriode,null);
		// Effectuer les calculs qui sont retournés sous la forme d'un tableau de montants (BigDecimal)
		NSArray resultats = calculerMontantsMoyensPourIndividu(individu, tauxPourFonction, valeursPointIndice, indices, quotients, datePremiereAttribution, fonction, debutPeriode, finPeriode,false);
		if (resultats == null) {
			return "CalculPFI- Une erreur s'est produite pendant le calcul des montants";
		}
		NSArray tauxMaximum = parametresValidesPourCodePeriodeEtQualifier(editingContext,TAUX_MAJORATION_MAXIMUM,debutPeriode,finPeriode,null);
		String erreur = verifierTaux(tauxMaximum);
		if (erreur != null) {
			return erreur;
		}
		double montantMoyenTotal = 0;
		// Vérifier si tous les montants calculés sont inférieurs à celui du taux de majoration
		e = tauxMaximum.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam param = (EOPrimeParam)e.nextElement();
			double taux = param.pparTaux().doubleValue();
			java.util.Enumeration e1 = resultats.objectEnumerator();
			while (e1.hasMoreElements()) {
				double montantMoyen = ((BigDecimal)e1.nextElement()).doubleValue();
				montantMoyenTotal += montantMoyen;
			}
			BigDecimal montantTotal = new BigDecimal(montantMoyenTotal).setScale(2, BigDecimal.ROUND_HALF_UP);
			if (montant == null) {
				return "CalculPFI - Le montant de l'individu est nul";
			} else if (montant.doubleValue() > (montantMoyenTotal * (100 + taux) / 100)) {
				return "CalculPFI - Le montant de l'individu est supérieur à " + taux +"% du montant moyen (" + montantTotal + ")";
			} 
		}
		return null;
	}
	private String verifierTaux(NSArray tauxPourFonction,NSArray valeursPointIndice,NSArray indices,NSArray quotients) {
		String erreur = verifierTaux(tauxPourFonction);
		if (erreur == null)  {
			erreur = verifierMontants(valeursPointIndice);
		}
		if (erreur == null) {
			erreur = verifierIndices(indices);
		}
		if (erreur == null) {
			erreur = verifierTaux(quotients);
		}
		return erreur;
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.modele.goyave.EOPrimeCode;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeParamPerso;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** L'indemnit&eacute; horaire sp&eacute;cifique pour travaux dans les centres de traitement de l'information est fonction du nombre
 * d'heures effectu&eacute;es, de la fonction de l'individu et du moment o&ugrave; les heures sont effectu&eacute;es (semaine entre 20h et 7h, samedi entre 7h et 20h, week-end ou jours f&eacute;ri&eacute;s).
 * Dans les deux derniers cas, on effectue une majoration du montant horaire.
 * @author christine
 *
 */
public class CalculTAI extends PluginAvecParametresPersonnelsEtValidation {
	private final static String	FONCTION_POUR_TAI = "FONINTAI";		// Code pour fonction
	private final static String	CATEGORIE_TVX_IND = "CATINTV";
	private final static String	NB_HEURES_TAI = "NBHEUTAI";
	private final static String	MONTANT_TAI_POUR_FONCTION = "MOHOFTAI";
	private final static String TAUX_MAJORATION_TAI = "TXMATAI";
	private double montantPourFonction;

	protected void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {
		// A besoin des paramètres personnels
	}

	protected void effectuerCalculPourDates(InformationPourPluginPrime information, NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
		LogManager.logDetail("CalculTAI - Calcul du montant de l'indemnité");
		// Récupérer les paramètres de montants pour les fonctions
		EOEditingContext editingContext = information.individu().editingContext();
		// Extraire les paramètres de l'individu
		int nbHeures = 0;
		double tauxMajoration = 0.00, montantIndemnite = 0.00;
		String formuleCalcul ="";
		try {
			if (parametresPersos != null && parametresPersos.count() > 0) {
				java.util.Enumeration e = parametresPersos.objectEnumerator();
				// A ce stade, les paramètres personnels ont été définis et ont été vérifiés
				while (e.hasMoreElements()) {
					EOPrimeParamPerso paramPerso = (EOPrimeParamPerso)e.nextElement();
					if (paramPerso.code().pcodCode().equals(NB_HEURES_TAI)) {

						nbHeures = new Integer(paramPerso.pppeValeur()).intValue();
						LogManager.logDetail("CalculTAI - Nombre d'heures effectuée " + nbHeures);
					} else if (paramPerso.code().pcodCode().equals(CATEGORIE_TVX_IND)) {
						// Rechercher le taux de majoration pour cette catégorie si il ne s'agit pas de la catégorie 1
						if (paramPerso.pppeValeur().indexOf("1") < 0) {
							NSArray codes = EOPrimeCode.rechercherCodesAvecCode(editingContext, paramPerso.pppeValeur());
							EOPrimeCode code = (EOPrimeCode)codes.objectAtIndex(0);
							NSArray parametres = EOPrimeParam.rechercherParametresValidesPourCodeEtPeriode(editingContext,code, debutValidite, finValidite);

							EOPrimeParam parametre = (EOPrimeParam)parametres.objectAtIndex(0);
							if (parametre.pparEntier() == null) {
								throw new Exception("CalculTAI - Contactez l'administrateur, la catégorie du paramètre lié au code " + code.code() + " n'est pas définie");
							}
							codes = EOPrimeCode.rechercherCodesAvecCode(editingContext, TAUX_MAJORATION_TAI + parametre.pparEntier().intValue());
							EOPrimeCode codeTaux = (EOPrimeCode)codes.objectAtIndex(0);
							parametres = EOPrimeParam.rechercherParametresValidesPourCodeEtPeriode(editingContext,codeTaux, debutValidite, finValidite);
							if (parametres.count() == 0) {
								throw new Exception("CalculTAI - Contactez l'administrateur, le paramètre de taux de majoration de la TAI pour le code " + codeTaux.code() + " n'est pas défini");
							}
							parametre = (EOPrimeParam)parametres.objectAtIndex(0);
							if (parametre.pparTaux() == null) {
								throw new Exception("CalculTAI - Contactez l'administrateur, le taux de majoration de la TAI pour le code " + codeTaux.code() + " n'est pas défini");
							}
							tauxMajoration = parametre.pparTaux().doubleValue();
						}
						LogManager.logDetail("CalculTAI - Taux de majoration " + tauxMajoration);
					} else if (paramPerso.code().pcodCode().equals(FONCTION_POUR_TAI)) {
						if (montantPourFonction == 0.00) {
							// les paramètres personnels n'ont pas été vérifiés (c'est le cas quand on calcule l'attribution d'un individu dans la gestion des primes individuelles)
							String texte = evaluerMontantHorairePourFonctionEtDates(editingContext, paramPerso.pppeValeur(), debutValidite, finValidite);
							if (texte != null) {
								throw new Exception(texte);

							}
						}
						// on a déjà le montant
						LogManager.logDetail("CalculTAI - Montant horaire " + " : " + montantPourFonction + " pour Fonction : " + paramPerso.pppeValeur());
					}
				}
				// Effectuer le calcul
				formuleCalcul = "Montant horaire pour la fonction : " + montantPourFonction + "\n";
				if (tauxMajoration > 0) {
					formuleCalcul += "(" + montantPourFonction + " x (1 + (" + tauxMajoration + "/100)))";
				} else {
					formuleCalcul += "" + montantPourFonction;
				}
				montantIndemnite = (montantPourFonction * (1 + (tauxMajoration/100)));
				formuleCalcul += " x " + nbHeures;
				montantIndemnite = montantIndemnite * (double)nbHeures;
				if (tauxMajoration == 0) {
					formuleCalcul += " (pas de taux de majoration)";
				}
				LogManager.logDetail("CalculTAI - " + formuleCalcul);
			} else {
				formuleCalcul = "Montant non évalué en l'absence de paramètres personnels";
			}
			PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),new BigDecimal(montantIndemnite).setScale(2,BigDecimal.ROUND_UP), debutValidite, finValidite,formuleCalcul);
			return;
		} catch (Exception exc) {
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}

	}
	/** ne peut pas calculer sans param&egrave;tre personnel */
	protected boolean peutCalculerSansParametrePersonnel() {
		return false;
	}
	/** pas de crit&egrave;re d'&eacute;ligibilit&eacute; */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	public String modeCalculDescription() {

		return "nb Heures x (montant pour fonction + (1 + taux pour catégorie de travaux/100))";
	}
	/** Non renouvelable */
	public boolean peutEtreRenouvellee() {
		return false;
	}
	/** Non modifiable, seuls les param&egrave;tres personnels sont modifiables */
	public boolean peutModifierMontantPrime() {
		return false;
	}
	protected String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime) {
		LogManager.logDetail("CalculTAI - Vérification du paramètre personnel " + parametrePerso.code());
		EOEditingContext editingContext = individuPourPrime.individu().editingContext();
		if (parametrePerso.code().equals(NB_HEURES_TAI)) {
			try {
				int nbHeures = new Integer(parametrePerso.valeur()).intValue();
				if (nbHeures <= 0) {
					return "Le nombre d'heures est un nombre entier positif";
				}
			} catch (Exception exc) {
				return "Le nombre d'heures est un nombre entier";
			} 
		} else if (parametrePerso.code().equals(FONCTION_POUR_TAI)) {
			String texte = evaluerMontantHorairePourFonctionEtDates(editingContext, parametrePerso.valeur(), individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
			if (texte != null) {
				return texte;
			}
		} else 	if (parametrePerso.code().equals(CATEGORIE_TVX_IND)) {
			// Vérifier que le paramètre existe pour ce code sauf si il s'agit de la catégorie 1
			if (parametrePerso.valeur().indexOf("1") < 0) {
				NSArray codes = EOPrimeCode.rechercherCodesAvecCode(editingContext, parametrePerso.valeur());
				if (codes.count() == 0) {
					return "CalculTAI - Contactez l'administrateur, pas de code correspondant au code " + parametrePerso.valeur();

				}
				EOPrimeCode code = (EOPrimeCode)codes.objectAtIndex(0);
				NSArray parametres = EOPrimeParam.rechercherParametresValidesPourCodeEtPeriode(editingContext,code, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
				if (parametres.count() == 0) {
					return "CalculTAI - Contactez l'administrateur, pas de paramètre défini pour le code " + parametrePerso.valeur();
				}
			}
		}
		return null;	
	}
	/** Pas de v&eacute;rification des montants */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime, EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		return null;
	}
	// Méthodes privées
	private String evaluerMontantHorairePourFonctionEtDates(EOEditingContext editingContext,String fonction,NSTimestamp dateDebut,NSTimestamp dateFin) {
		montantPourFonction = 0.00;
		// Rechercher les montants liés au fonction
		NSArray params = (NSArray)EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(editingContext, MONTANT_TAI_POUR_FONCTION, dateDebut, dateFin, null);
		if (params.count() == 0) {
			return "CalculTAI - Contactez l'administrateur, pas de montant défini selon les fonctions ";
		}
		// Trier les paramètres par ordre de date croissante et par libellé. On prend le premier trouvé car on considère
		// le nombre d'heures à la date début
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("fonction.libelleLong", EOSortOrdering.CompareAscending));
		java.util.Enumeration e = params.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
			if (parametre.fonction().libelleLong().equals(fonction)) {
				if (parametre.pparMontant() == null) {
					return "CalculTAI - Contactez l'administrateur, pas de montant défini pour la fonction " + fonction;
				} else {
					montantPourFonction = parametre.pparMontant().doubleValue();
					return null;
				}
			}
		}
		// On n'a pas trouvé la fonction
		return "CalculTAI - Contactez l'administrateur, pas de paramètre de montant défini pour la fonction " + fonction;
	}
}

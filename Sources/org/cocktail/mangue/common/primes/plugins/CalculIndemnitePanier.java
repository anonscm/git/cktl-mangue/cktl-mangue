/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
 package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class CalculIndemnitePanier extends PluginAvecParametresPersonnels {
	private final static String MONTANT_PAR_NUIT = "MONNPANI";

	/** Refus si l'individu est log&eacute; en n&eacute;cessit&eacute; de service */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individuPourPrime) {
		if (individuPourPrime.individu().personnel().toLoge() != null && individuPourPrime.individu().personnel().toLoge().estNecessiteService()) {
			return "Individu logé pour nécessité de service";
		} else {
			return null;
		}
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return true;
	}
	/** Le montant n'est pas modifiable */
	public boolean peutModifierMontantPrime() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** montant par nuit x nb nuits effectu&eacute;es par l'individu */
	public String modeCalculDescription() {
		return "montant par nuit x nb nuits effectuées par l'individu";
	}
	// Méthodes protégées
	/** Utilise toujours les param&egrave;tres personnels */
	protected  boolean peutCalculerSansParametrePersonnel() {
		return false;
	}
	/** Calcule le montant comme le montant de la personnalisation (nb nuits) x montant par nuit */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {		
		LogManager.logDetail("CalculIndemnitePanier - Calcul du montant de l'attribution");

		int nbNuits = -1;
		// Il y a un seul paramètre personnel défini
		try {
			nbNuits = personnalisation.pmpeMontant().intValue();
			if (nbNuits < 0) {
				PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Le nombre de nuits de travaux pour cet individu ne peut pas être négatif", debutValidite, finValidite);
				return;
			}
		} catch (Exception exc) {
			exc.printStackTrace();
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Le nb de nuits doit être un entier", debutValidite, finValidite);
			return;
		}	
		try {
			// Récupérer les paramètres définis sur cette période
			NSArray parametres = information.parametresPourCodeEtDates(MONTANT_PAR_NUIT,debutValidite, finValidite);
			java.util.Enumeration e = parametres.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
				if (DateCtrl.isBefore(dateDebut,debutValidite)) {
					dateDebut = debutValidite;
				}
				if (dateFin != null && finValidite != null && DateCtrl.isAfter(dateFin, finValidite)) {
					dateFin = finValidite;
				}
				if (parametre.pparMontant() == null) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
				} else {
					double montant = parametre.pparMontant().doubleValue() * nbNuits;
					String formuleCalcul = "Montant par nuit : " + parametre.pparMontant() + ", nb nuits : " + nbNuits + "\nCalcul :" + parametre.pparMontant().doubleValue() + " x " + nbNuits;
					BigDecimal montantRetour = new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP);
					PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montantRetour, dateDebut, dateFin,formuleCalcul);
					LogManager.logDetail(formuleCalcul);
				}
			}
		} catch (Exception exc) {
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);

		}
	}
	/** Pas d'utilisation des param&grave;tres personnels */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite,NSTimestamp finValidite) {
		// Un seul paramètre personnel, on utilise le montant
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
 package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class CalculIndemRespAdmin extends PluginAvecParametresPersonnelsEtValidation {
	private final static String MONTANT_PRIME_SGASU = "MOIRSGA";
	private final static String MONTANT_PRIME_CASU = "MOIRCAS";
	private final static int GROUPE_SGASU_MAX = 2;
	private final static int GROUPE_CASU_MAX = 3;

	/** pas de refus */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** le groupe  doit &circ;tre 1 ou 2 pour les SGASU, 1, 2 ou 3 pour les CASU */
	public String verifierValiditeMontant(BigDecimal numeroGroupe,IndividuPourPrime individuPourPrime,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		LogManager.logDetail("CalculIndemRespAdmin - Verification Parametre Personnel");
		// Récupérer la valeur et la transformer en entier
		int numero = numeroGroupe.intValue();
		LogManager.logDetail("CalculIndemRespAdmin - Catégorie " + numero);
		if (numeroValidePourGrade(numero,individuPourPrime.grade().cGrade())) {
			return null;
		} else {
			return "CalculIndemRespAdmin - Le groupe doit être compris entre 1 et 2 pour les SGASU, 1 et 3 pour les CASU";
		}
	}
	/** montant pour le groupe choisi */
	public String modeCalculDescription() {
		return " montant pour le groupe choisi";
	}
	// Méthodes protégées
	/** Pas de v&eacute;rification de validit&eacute; */
	protected String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPrime) {
		return null;
	}
	/** Ne peut pas calculer sans param&egrave;tre personnel */
	protected  boolean peutCalculerSansParametrePersonnel() {
		return false;
	}
	/** pas de param&grave;tre personnel */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
	}
	/** En l'absence de personnalisation, retourne un montant nul */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {		
		LogManager.logDetail("CalculPrimeAdministration - Calcul du montant de l'attribution");
		try {
			// Commencer par vérifier si un groupe est fourni
			int groupe = 0;
			if (personnalisation == null) {
				String formuleCalcul = "Pas de param&egrave;tre personnel, groupe inconnu";
				LogManager.logDetail(formuleCalcul);
				PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),new BigDecimal(0), debutValidite, finValidite,formuleCalcul);
				return;
			}
			groupe = personnalisation.pmpeMontant().intValue();
			LogManager.logDetail("CalculIndemRespAdmin - Groupe " + groupe);
			String code = MONTANT_PRIME_SGASU;
			if (information.gradeIndividu().cGrade().equals("0314") || information.gradeIndividu().cGrade().equals("0315")) {
				code = MONTANT_PRIME_CASU;
			}
			NSArray montants = information.parametresPourCodeGradeEtDates(code + groupe,information.gradeIndividu(),debutValidite, finValidite);
			String message = verifierMontants(montants);
			if (message != null) {
				PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Contactez l'administrateur, " + message, debutValidite, finValidite);
				LogManager.logDetail("CalculIndemRespAdmin - Erreur dans montants : " + message);
				return;
			}
			montants = EOSortOrdering.sortedArrayUsingKeyOrderArray(montants, new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareAscending)));
			java.util.Enumeration e = montants.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
				if (DateCtrl.isBefore(dateDebut,debutValidite)) {
					dateDebut = debutValidite;
				}
				if (dateFin != null && finValidite != null && DateCtrl.isAfter(dateFin, finValidite)) {
					dateFin = finValidite;
				}
				String formuleCalcul = "Montant pour le groupe " + groupe +" : " + parametre.pparMontant();
				LogManager.logDetail(formuleCalcul);
				PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),parametre.pparMontant().setScale(2,BigDecimal.ROUND_HALF_UP), dateDebut, dateFin,formuleCalcul);
			}
		} catch (Exception exc) {
			exc.printStackTrace();
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}
	}
	// Méthodes privées
	private boolean numeroValidePourGrade(int numero,String grade) {
		if (numero < 1) {
			return false;
		}
		return (((grade.equals("0221") || grade.equals("0503")) && numero <= GROUPE_SGASU_MAX) || 
				((grade.equals("0314") || grade.equals("0315")) && numero <= GROUPE_CASU_MAX));
	}

}

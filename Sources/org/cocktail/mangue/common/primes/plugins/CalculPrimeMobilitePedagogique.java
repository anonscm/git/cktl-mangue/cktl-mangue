/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Cette prime a un montant qui est fix&eacute; par arr&ecirc;t&eacute; annuel. Elle peut &ecirc;tre attribu&eacute;e aux
 * individus qui ont la fonction de directeur de recherche */
public class CalculPrimeMobilitePedagogique extends PluginAvecParametresStandard {
	private final static String MONTANT_PRIME = "MOPRMOPE";
	
	/** Pas de crit&grave;re sp&eacute;cifique */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	public void calculerMontantsPrime(InformationPourPluginPrime information) {
		try {
			NSArray montants = information.parametresPourCodeFonctionEtDates(MONTANT_PRIME, information.fonctionIndividu().libelleLong(), information.debutPeriode(), information.finPeriode());
			String message = verifierMontants(montants);
			if (message != null) {
				PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Contactez l'administrateur, " + message, information.debutPeriode(), information.finPeriode());
				LogManager.logDetail("CalculPrimeMobilitePedagogique - Erreur dans montants : " + message);
				return;
			}
			montants = EOSortOrdering.sortedArrayUsingKeyOrderArray(montants, new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareAscending)));
			java.util.Enumeration e = montants.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
				if (DateCtrl.isBefore(dateDebut,information.debutPeriode())) {
					dateDebut = information.debutPeriode();
				}
				if (dateFin != null && information.finPeriode() != null && DateCtrl.isAfter(dateFin, information.finPeriode())) {
					dateFin = information.finPeriode();
				}
				String formuleCalcul = "Montant pour la fonction " + information.fonctionIndividu().libelleLong() +" : " + parametre.pparMontant();
				LogManager.logDetail(formuleCalcul);
				PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),parametre.pparMontant().setScale(2,BigDecimal.ROUND_HALF_UP), dateDebut, dateFin,formuleCalcul);
			}
		} catch (Exception exc) {
			exc.printStackTrace();
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), information.debutPeriode(), information.finPeriode());

		}
	}
	/** Il suffit d&ecirc;tre directeur de recherche */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	/** Montant fix&eacute; par arr&ecirc;t&eacute; annuel */
	public String modeCalculDescription() {
		return "Montant fixé par arrêté annuel";
	}
	/** Prime non renouvellable */
	public boolean peutEtreRenouvellee() {
		return false;
	}
	/** Montant non modifiable */
	public boolean peutModifierMontantPrime() {
		return false;
	}

}

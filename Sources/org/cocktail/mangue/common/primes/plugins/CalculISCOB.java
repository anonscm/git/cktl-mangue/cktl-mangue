/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
 package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;
import org.cocktail.mangue.modele.grhum.EOGrade;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Le montant annuel de l'Indemnit&eacute; Sp&eacute;ciale de Conservateur de Biblioth&egrave;que est un montant personnalis&eacute; 
 * qui ne peut exc&eacute;der un un montant maximal qui est fonction du grade
 * 
 */

public class CalculISCOB extends PluginAvecParametresPersonnelsEtValidation {
	private final static String MONTANT_MOYEN_POUR_GRADE = "MOMACOB";
	private final static String MONTANT_MAXIMUM_POUR_GRADE = "MOMXCOB";

	/** pas de refus sp&eacute;ficique */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individuPourPrime) {
		return null;
	}
	/** pas de crit&egrave;re sp&eacute;ficique */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** Pas de v&eacute;rification de validit&eacute; car seulement un montant */
	public String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime) {
		return null;
	}
	/** l'ISCOB est un montant personnalis&eacute; */
	public String modeCalculDescription() {
		return "l'Indemnité Spéciale de Conservateur de Bibliothèque est un montant personnalisé";
	}
	/** V&eacute;rifie si le montant d&eacute;passe un certain nombre de fois le montant moyen */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		if (montant == null) {
			return "CalculIFS - Le montant à vérifier est nul";
		}
		double montantReel = montant.doubleValue();
		try {
			LogManager.logDetail("CalculISCOB - Grade individu " + individuPourPrime.grade().cGrade() + ", montant vérifié : " + montant);
			NSArray montantsMax = montantsPourGradeEtPeriode(individuPourPrime.grade(),individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),false);
			java.util.Enumeration e = montantsMax.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				if (parametre.pparMontant() == null) {
					return "CalculISCOB - Contactez l'administrateur, pas de montant défini pour l'Indem. Spéciale de Conservateur pour la période démarrant le " + parametre.debutValidite();
				}
				double montantMax = parametre.pparMontant().doubleValue();
				if (montantReel > montantMax) {
					return "CalculISCOB - Le montant de l'individu (" + montant + ") est supérieur au montant maximum autorisé (" + montantMax + ")\nVeuillez changer le montant";
				}
			}
			return null;
		} catch (Exception exc) {
			return exc.getMessage();
		}
	}
	// Méthodes protégées
	/** Peut calculer sans param&egrave;tre personnel */
	protected boolean peutCalculerSansParametrePersonnel() {
		return true;
	}
	/** pas de gestion des param&egrave;tres personnels */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
	}
	/** En l'absence de param&grave;tre personnel, retourne les montants moyens pour la p&eacute;riode */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information, EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {
		LogManager.logDetail("CalculISCOB - Calcul du montant de l'attribution");
		try {
			// Commencer par vérifier si un montant personnel a été fourni
			if (personnalisation != null) {
				if (personnalisation.pmpeMontant() == null) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Pas de montant dans la personnalisation ", debutValidite, finValidite);	
				} else {
					LogManager.logDetail("CalculISCOB - Montant " + personnalisation.pmpeMontant());
					String formuleCalcul = "Montant annuel personnalisé : " + personnalisation.pmpeMontant();
					LogManager.logDetail(formuleCalcul);
					PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),personnalisation.pmpeMontant(), information.debutPeriode(), information.finPeriode(),formuleCalcul);
				}
			} else {
				NSArray montantsMoyens = montantsPourGradeEtPeriode(information.gradeIndividu(), debutValidite, finValidite,true);
				java.util.Enumeration e = montantsMoyens.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
					NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
					if (DateCtrl.isBefore(dateDebut,debutValidite)) {
						dateDebut = debutValidite;
					}
					if (dateFin == null || (dateFin != null && finValidite != null && DateCtrl.isAfter(dateFin, finValidite))) {
						dateFin = finValidite;
					}
					if (parametre.pparMontant() == null) {
						PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
					} else {
						LogManager.logDetail("montant moyen " + parametre.pparMontant());
						double montant = parametre.pparMontant().doubleValue();
						if (montant == 0) {
							LogManager.logDetail("CalculISCOB - Montant moyen " + montant);
							PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculISCOB - Contactez l'administrateur, Le montant moyen pour la période  est nul", dateDebut, dateFin);
						} else {
							BigDecimal montantRetour = new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP);
							String formuleCalcul = "Montant Moyen annuel : " + montantRetour;
							LogManager.logDetail(formuleCalcul);
							PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montantRetour, dateDebut, dateFin,formuleCalcul);
						}
					}
				}
			}
		} catch (Exception exc) {
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}
	}
	// Méthodes privées
	private NSArray montantsPourGradeEtPeriode(EOGrade grade,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean estMontantMoyen) throws Exception {
		// Vérifier qu'on et bien entre les taux moyen et exceptionnel en recherchant ces paramètres pour le grade de l'individu
		if (grade == null) {
			throw new Exception("CalculISCOB - Grade inconnu");
		}
		String periode = "du " + DateCtrl.dateToString(debutPeriode);
		if (finPeriode != null) {
			periode += " au " + DateCtrl.dateToString(finPeriode);
		}
		NSArray montants = null;
		String texte = "moyen";
		String code = MONTANT_MOYEN_POUR_GRADE;
		if (!estMontantMoyen) {
			texte = "maximum";
			code = MONTANT_MAXIMUM_POUR_GRADE;
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("grade = %@", new NSArray(grade));		
		montants = parametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), code, debutPeriode, finPeriode,qualifier);
		if (montants.count() == 0) {
			throw new Exception("CalculISCOB - Contactez l'administrateur, pas de paramètre de montant " + texte + " défini pour le grade " + grade.cGrade()  + " pour la période " + periode);
		}
		if (parametresValidesSurPeriode(montants,debutPeriode,finPeriode) == false) {
			throw new Exception("CalculISCOB - Les montants " + texte + "s de l'Indemnité Spéciale de Conservateur ne couvrent pas toute la période " + periode);
		}
		return montants;
	}
}

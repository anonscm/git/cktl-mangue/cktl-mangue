/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;
import org.cocktail.mangue.modele.grhum.EOGrade;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Le montant annuel de l'Indemnit&eacute; d'Administration et de Technicit&eacute; est un montant personnalis&eacute; 
 * qui ne peut exc&eacute;der un certain nombre de fois le montant moyen
 * 
 */
public class CalculIAT extends PluginAvecParametresPersonnelsEtValidation {
	private final static String COEFFICIENT_MULTIPLICATEUR = "COEFMIAT";
	private final static String MONTANT_MOYEN_AVEC_ECHELLE = "MOIATEC";
	private final static String MONTANT_MOYEN_CATEGORIE_B = "MOIATCAB";
	private final static String MONTANT_MOYEN_NOUVEL_INDICE = "MOIATNI";
	private final static String MONTANT_MOYEN_INDICE_SPECIFIQUE = "MOIATIS";
	private final static String INDICE_MAX_CAT_B = "INDMAXB";

	/** Refus pour les agents de cat&eacute;gorie B dont l'indice est sup&eacute;rieur &agrave; 380 */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individuPourPrime) {
		if (individuPourPrime.grade().toCategorie() != null && individuPourPrime.grade().toCategorie().code().equals("B")) {
			NSArray parametres = parametresValidesPourCodePeriodeEtQualifier(individuPourPrime.individu().editingContext(), INDICE_MAX_CAT_B, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin(), null);
			for (java.util.Enumeration<EOPrimeParam> e = parametres.objectEnumerator();e.hasMoreElements();) {
				EOPrimeParam parametre = e.nextElement();
				if (parametre.pparIndice() != null) {
					try {
						int indMax = new Integer(individuPourPrime.indice()).intValue();
						// Vrifier l'indice de l'individu
						if (individuPourPrime.indice() != null) {
							int ind = new Integer(individuPourPrime.indice()).intValue();
							if (ind > indMax) {
								return "l'individu ne peut bénéficier de l'IAT, il a un indice supérieur à " + indMax;
							}
						}
					} catch(Exception exc) {
					}
				}
			}
		}
		return null;
	}
	/** Refus pour les agents de cat&eacute;gorie B dont l'indice est sup&eacute;rieur &agrave; 380 */
	public boolean aCriteresEligibiliteSpecifiques() {
		return true;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** Pas de v&eacute;rification de validit&eacute; car seulement un montant */
	public String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime) {
		return null;
	}
	/** l'IAT est un montant personnalis&eacute; */
	public String modeCalculDescription() {
		return "l'IAT est un montant personnalisé";
	}
	/** V&eacute;rifie si le montant d&eacute;passe un certain nombre de fois le montant moyen */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		if (montant == null) {
			return "CalculIAT - Le montant à vérifier est nul";
		}
		double montantReel = montant.doubleValue();
		try {
			NSArray montantsMoyens = montantsMoyensPourGradeEtPeriode(individuPourPrime.grade(),individuPourPrime.periodeDebut(),individuPourPrime.periodeFin());;
			LogManager.logDetail("CalculIAT - Grade individu " + individuPourPrime.grade().cGrade() + ", montant vrifi : " + montant);
			NSArray coefficients = parametresValidesPourCodePeriodeEtQualifier(individuPourPrime.individu().editingContext(), COEFFICIENT_MULTIPLICATEUR, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin(),null);
			if (coefficients.count() == 0) {
				return "CalculIAT - Contactez l'administrateur, pas de coefficient multiplicateur pour l'IAT pour la période démarrant le " + DateCtrl.dateToString(individuPourPrime.periodeDebut());
			}
			// Dterminer le coefficient minimum parmi tous les coefficients
			int coefficient = 0;
			java.util.Enumeration e = coefficients.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				if (parametre.pparEntier() == null) {
					return "CalculIAT - Contactez l'administrateur, pas de coefficient multiplicateur pour l'IAT pour la période démarrant le " + parametre.debutValiditeFormatee();
				}
				int valeur = parametre.pparEntier().intValue();
				if (coefficient == 0 || valeur < coefficient) {
					coefficient = valeur;
				}
			}
			// On peut maintenant vrifier les montants
			e = montantsMoyens.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				if (parametre.pparMontant() == null) {
					return "CalculIAT - Contactez l'administrateur, pas de montant défini pour l'IAT pour la période démarrant le " + parametre.debutValidite();
				}
				double montantMoyen = parametre.pparMontant().doubleValue();
				if (montantReel > montantMoyen * coefficient) {
					return "CalculIAT - Le montant de l'individu (" + montant + ") est supérieur à " + coefficient + " fois le montant moyen (" + montantMoyen + ")\nVeuillez changer le montant";
				}
			}
			return null;
		} catch (Exception exc) {
			return exc.getMessage();
		}
	}
	// Mthodes protges
	/** Peut calculer sans param&egrave;tre personnel */
	protected boolean peutCalculerSansParametrePersonnel() {
		return true;
	}
	/** pas de gestion des param&egrave;tres personnels */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
	}
	/** En l'absence de param&grave;tre personnel, retourne les montants moyens pour la p&eacute;riode */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information, EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {
		LogManager.logDetail("CalculIAT - Calcul du montant de l'attribution");
		try {
			// Commencer par vrifier si un montant personnel a t fourni
			if (personnalisation != null) {
				if (personnalisation.pmpeMontant() == null) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Pas de montant dans la personnalisation ", debutValidite, finValidite);	
				} else {
					LogManager.logDetail("CalculIAT - Montant " + personnalisation.pmpeMontant());
					String formuleCalcul = "Montant annuel personnalisé : " + personnalisation.pmpeMontant();
					LogManager.logDetail(formuleCalcul);
					PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),personnalisation.pmpeMontant(), information.debutPeriode(), information.finPeriode(),formuleCalcul);
				}
			} else {
				NSArray montantsMoyens = montantsMoyensPourGradeEtPeriode(information.gradeIndividu(), debutValidite, finValidite);
				java.util.Enumeration e = montantsMoyens.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
					NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
					if (DateCtrl.isBefore(dateDebut,debutValidite)) {
						dateDebut = debutValidite;
					}
					if (dateFin == null || (dateFin != null && finValidite != null && DateCtrl.isAfter(dateFin, finValidite))) {
						dateFin = finValidite;
					}
					if (parametre.pparMontant() == null) {
						PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
					} else {
						LogManager.logDetail("montant moyen " + parametre.pparMontant());
						double montant = parametre.pparMontant().doubleValue();
						if (montant == 0) {
							LogManager.logDetail("CalculIAT - Montant moyen " + montant);
							PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculIAT - Contactez l'administrateur, Le montant moyen pour la période  est nul", dateDebut, dateFin);
						} else {
							BigDecimal montantRetour = new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP);
							String formuleCalcul = "Montant Moyen annuel : " + montantRetour;
							LogManager.logDetail(formuleCalcul);
							PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montantRetour, dateDebut, dateFin,formuleCalcul);
						}
					}
				}
			}
		} catch (Exception exc) {
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}
	}
	// Mthodes privées
	private NSArray montantsMoyensPourGradeEtPeriode(EOGrade grade,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		// Vrifier qu'on et bien entre les taux moyen et exceptionnel en recherchant ces paramtres pour le grade de l'individu
		if (grade == null) {
			throw new Exception("CalculIAT - Grade inconnu");
		}
		String periode = "du " + DateCtrl.dateToString(debutPeriode);
		if (finPeriode != null) {
			periode += " au " + DateCtrl.dateToString(finPeriode);
		}
		NSArray montantsMoyens = null;
		if (grade.toCategorie() != null && grade.toCategorie().code().equals("B")) {
			montantsMoyens = parametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), MONTANT_MOYEN_CATEGORIE_B, debutPeriode, finPeriode, null);
			if (montantsMoyens.count() == 0) {
				throw new Exception("CalculIAT - Contactez l'administrateur, pas de paramètre de montant défini pour l'IAT avec le code " + MONTANT_MOYEN_CATEGORIE_B  + " pour la période " + periode);
			}
		} else if (grade.echelle() != null && (grade.echelle().equals("3") || grade.echelle().equals("4") || grade.echelle().equals("5"))) {
			String codeMontant = MONTANT_MOYEN_AVEC_ECHELLE + grade.echelle();
			montantsMoyens = parametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), codeMontant, debutPeriode, finPeriode, null);
			if (montantsMoyens.count() == 0) {
				throw new Exception("CalculIAT - Contactez l'administrateur, pas de paramètre de montant défini pour l'IAT avec le code " + codeMontant + " pour la période " + periode);
			}
		} else {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("grade = %@", new NSArray(grade));		
			montantsMoyens = parametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), MONTANT_MOYEN_NOUVEL_INDICE, debutPeriode, finPeriode,qualifier);
			if (montantsMoyens.count() == 0) {
				montantsMoyens = parametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), MONTANT_MOYEN_INDICE_SPECIFIQUE, debutPeriode, finPeriode,qualifier);
			}
			if (montantsMoyens.count() == 0) {
				throw new Exception("CalculIAT - Contactez l'administrateur, pas de paramètre de montant défini pour le grade " + grade.cGrade()  + " pour la période " + periode);
			}
		}
		if (parametresValidesSurPeriode(montantsMoyens,debutPeriode,finPeriode) == false) {
			throw new Exception("CalculIAT - Les montants moyens IAT ne couvrent pas toute la période " + periode);
		}
		return montantsMoyens;
	}
}

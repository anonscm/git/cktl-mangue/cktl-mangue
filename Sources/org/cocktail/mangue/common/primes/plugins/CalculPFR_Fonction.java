/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.common.primes.plugins;
import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOPostePFR;
import org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** Le montant annuel de la PFR est un montant personnalis&eacute; qui est compris entre deux valeurs estim&eacute; &agrave; partir
 * d'une part de Fonctions et d'une part de R&eacute;sultat. Le coefficient de la part Fonctions est affect&eacute; au poste
 * occup&eacute; par l'individu. Le calcul indiquera ces valeurs minimum et maximum
 * 
 */
public class CalculPFR_Fonction extends PluginAvecParametresPersonnelsEtValidation {
	private final static String COEFFICIENT_MINIMUM_PART_FONCTION = "COMIPFRF";
	private final static String COEFFICIENT_MAXIMUM_PART_FONCTION = "COMXPFRF";
	private final static String COEFFICIENT_MINIMUM_PART_FONCTION_AGENT_LOGE = "CMILPFRF";
	private final static String COEFFICIENT_MAXIMUM_PART_FONCTION_AGENT_LOGE = "CMXLPFRF";
	private final static String MONTANT_PART_FONCTION = "MOPFRF";
	private String formuleCalculFonction;

	/** pas de refus */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** pas de param&egrave;tre personnel mais une personnalisation */
	public String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime) {
		return null;
	}
	/** la PFR est un montant personnalis&eacute; */
	public String modeCalculDescription() {
		return "Part de Fonction dont le coefficient est associé au poste";
	}
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return null;
	}
	// Méthodes protégées
	/** Peut calculer sans param&egrave;tre personnel */
	protected boolean peutCalculerSansParametrePersonnel() {
		return true;
	}
	/** pas de param&egrave;tre personnel mais une personnalisation */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
	}
	/** En l'absence de personnalisation, retourne le montant calcul&eacute; &agrave; partir du montant pour la part Fonction et
	 * du montan minimum de la part R&eacute;sultat pour la p&eacute;riode */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information, EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {
		LogManager.logDetail("CalculPFR_Fonction - Calcul du montant de l'attribution");
		try {
			// Afin de calculer la part Fonction, Commencer par évaluer toutes les périodes à gérer pour les différents postes
			// elles sont classées par ordre de date croissante
			NSArray periodesAvecCoefficients = periodesCoefficientsPourIndividuSurPeriode(information.individu(), debutValidite, finValidite);
			java.util.Enumeration e = periodesAvecCoefficients.objectEnumerator();
			while (e.hasMoreElements()) {
				PeriodeAvecCoefficients periode = (PeriodeAvecCoefficients)e.nextElement();
				LogManager.logDetail("Période du " + DateCtrl.dateToString(periode.debutPeriode()) + " au " + DateCtrl.dateToString(periode.finPeriode()));
				java.util.Enumeration e1 = periode.coefficients().objectEnumerator();
				String formuleCalcul = "";
				double montantPartFonction = 0.00;
				while (e1.hasMoreElements()) {
					CoefficientQuotite coefficientQuotite = (CoefficientQuotite)e1.nextElement();
					double coefficientFonction = (coefficientQuotite.quotite() * coefficientQuotite.coefficient()) / 100;
					// Calculer le montant de la part Fonction
					if (formuleCalcul.length() > 0) {
						formuleCalcul += "\n";
					}
					formuleCalcul += "Quotité du poste : " + coefficientQuotite.quotite() + ", coefficient PFR du poste : " + coefficientQuotite.coefficient() + "\n";
					montantPartFonction += montantPourPeriodeEtCoefficient(information.individu(),information.gradeIndividu(),coefficientFonction,periode.debutPeriode(),periode.finPeriode(),true);
					formuleCalcul += formuleCalculFonction;
				}
				LogManager.logDetail(formuleCalcul);
				BigDecimal montantRetour = new BigDecimal(montantPartFonction).setScale(2,BigDecimal.ROUND_HALF_UP);
				PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montantRetour, periode.debutPeriode(), periode.finPeriode(),formuleCalcul);
			}
		} catch (Exception exc) {
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}
	}
	// Méthodes privées
	private NSArray montantsPourGradeEtPeriode(EOGrade grade,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		// Vérifier qu'on et bien entre les taux moyen et exceptionnel en recherchant ces paramètres pour le grade de l'individu
		if (grade == null) {
			throw new Exception("CalculPFR_Fonction - Grade inconnu");
		}
		String code = MONTANT_PART_FONCTION;
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("grade = %@", new NSArray(grade));		
		NSArray parametresMontant = EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), code, debutPeriode, finPeriode,qualifier);
		if (parametresMontant.count() == 0) {
			// Si on n'en trouve pas, vérifier si il n'en n'existe pas pour le corps
			qualifier = EOQualifier.qualifierWithQualifierFormat("corps = %@", new NSArray(grade.toCorps()));		
			parametresMontant = EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), code, debutPeriode, finPeriode,qualifier);
			if (parametresMontant.count() == 0) {
				throw new Exception("CalculPFR_Fonction - Contactez l'administrateur, pas de paramètre de montant défini pour le corps " + grade.toCorps().lcCorps() + " ou pour le grade " + grade.lcGrade());
			}
		}
		return parametresMontant;
	}
	private double montantPourPeriodeEtCoefficient(EOIndividu individu,EOGrade grade,double coefficient,NSTimestamp debutValidite, NSTimestamp finValidite, boolean estMinimum) throws Exception {
		// Calcule le montant de la part en fonction du coefficient
		// Si le coefficient est négatig, calcul les parts mini et maxi selon le booléen estMinimum
		NSArray montants = montantsPourGradeEtPeriode(grade, debutValidite, finValidite);
		double montantCourant = 0;
		formuleCalculFonction = "";
		java.util.Enumeration e = montants.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
			NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
			if (DateCtrl.isBefore(dateDebut,debutValidite)) {
				dateDebut = debutValidite;
			}
			if (dateFin == null || (dateFin != null && finValidite != null && DateCtrl.isAfter(dateFin, finValidite))) {
				dateFin = finValidite;
			}
			if (parametre.pparMontant() == null) {
				PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(individu,"Le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
			} else {
				double montant = parametre.pparMontant().doubleValue();
				if (montant == 0) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(individu,"CalculPFR_Fonction - Contactez l'administrateur, Le montant minimum de la part Fonction de la PFR pour la période  est nul", dateDebut, dateFin);
					return -1;
				}
				montantCourant = evaluerMontantPourParametre(individu,parametre,montantCourant,coefficient,estMinimum);
			}
		}
		return montantCourant;
	}
	private double evaluerMontantPourParametre(EOIndividu individu, EOPrimeParam parametre,double montantCourant,double coefficient, boolean estMinimum) throws Exception {
		boolean estValeurIndividuelle = coefficient >= 0;
		if (coefficient < 0) {	// On passe une valeur négative quand on calcule avec les valeurs réelles
			coefficient = evaluerCoefficientPourIndividu(individu,parametre.debutValidite(),parametre.finValidite(),estMinimum);
		}
		double montant = parametre.pparMontant().doubleValue();
		if (montantCourant == 0 || (estMinimum && montant < montantCourant) || (!estMinimum && montant > montantCourant)) {
			String formuleCalcul = "";
			formuleCalcul = "Part Fonction\nMontant de Référence : " + montant + "\n";

			formuleCalcul += "Coefficient multiplicateur : " + coefficient;
			if (individu.personnel().toLoge() != null && individu.personnel().toLoge().estNecessiteService()) {
				formuleCalcul += " (individu logé par nécessité de service)";
			}
			if (!estValeurIndividuelle) {
				if (estMinimum) {
					formuleCalcul += "\nMontant minimum : ";
				} else {
					formuleCalcul += "\nMontant maximum : ";
				}
			} else {
				formuleCalcul += "\nMontant : ";
			}
			formuleCalcul += montant + " x " + coefficient + " (montant référence x coefficient)";
			formuleCalculFonction = formuleCalcul;
			montantCourant = montant * coefficient;
		}
		return montantCourant;
	}
	private double evaluerCoefficientPourIndividu(EOIndividu individu, NSTimestamp debutValidite, NSTimestamp finValidite,boolean estMinimum) throws Exception {
		String code = "";
		if (individu.personnel().toLoge() != null && individu.personnel().toLoge().estNecessiteService()) {
			if (estMinimum) {
				code = COEFFICIENT_MINIMUM_PART_FONCTION_AGENT_LOGE;
			} else {
				code = COEFFICIENT_MAXIMUM_PART_FONCTION_AGENT_LOGE;
			}
		} else {
			if (estMinimum) {
				code = COEFFICIENT_MINIMUM_PART_FONCTION;
			} else {
				code = COEFFICIENT_MAXIMUM_PART_FONCTION;
			}
		}
	
		NSArray coefficients = EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(individu.editingContext(), code, debutValidite, finValidite, null);
		// Si il s'agit de la valeur minimum on prend le coefficient maximum, pour le maximum la valeur minimum
		// pour être sûr d'être dans le bon intervalle sur la période
		double coefficient = 0;
		java.util.Enumeration e = coefficients.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametreCoeff = (EOPrimeParam)e.nextElement();
			if (parametreCoeff.pparEntier() == null) {
				throw new Exception("CalculPFR_Fonction - Le coefficient du paramètre " + parametreCoeff.pparLibelle() + " n'est pas défini");
			}
			double coeff = parametreCoeff.pparEntier().doubleValue();
			if (estMinimum) {
				if (coefficient == 0 || coeff > coefficient) {
					coefficient = coeff;
				}
			} else {
				if (coefficient == 0 || coeff < coefficient) {
					coefficient = coeff;
				}
			}
		}
		return coefficient;
	}
	private NSArray periodesCoefficientsPourIndividuSurPeriode(EOIndividu individu,NSTimestamp debutValidite,NSTimestamp finValidite) throws Exception {
		EOEditingContext editingContext = individu.editingContext();
		// Vérifier que le coefficient de fonction du poste est fourni sinon interrompre le calcul
		NSArray detailsAffectation = EOAffectationDetail.rechercherDetailsPourIndividuEtPeriode(editingContext, individu, debutValidite, finValidite,true);
		detailsAffectation = EOSortOrdering.sortedArrayUsingKeyOrderArray(detailsAffectation, new NSArray(EOSortOrdering.sortOrderingWithKey("adeDDebut", EOSortOrdering.CompareAscending)));
		NSMutableArray periodesAvecCoefficients = new NSMutableArray();
		// Il va falloir prendre en compte les périodicités des périodes affectations + les périodicités des postesPFR
		// pour ajouter autant de résultats que de détails trouvés et appliquer sur chaque intervalle le coefficient de personnalisation trouvé
		java.util.Enumeration e = detailsAffectation.objectEnumerator();
		while (e.hasMoreElements()) {
			NSTimestamp debutPeriode = debutValidite, finPeriode = finValidite;
			EOAffectationDetail detail = (EOAffectationDetail)e.nextElement();
			if (DateCtrl.isAfter(detail.adeDDebut(),debutPeriode)) {
				debutPeriode = detail.adeDDebut();
			}
			if (detail.adeDFin() != null && DateCtrl.isBefore(detail.adeDFin(), finPeriode)) {
				finPeriode = detail.adeDFin();
			}
			NSArray postesPFR = EOPostePFR.postesPFRPourPosteEtPeriode(editingContext, detail.toPoste(), debutPeriode, finPeriode);
			postesPFR = EOSortOrdering.sortedArrayUsingKeyOrderArray(postesPFR, new NSArray(EOSortOrdering.sortOrderingWithKey("ppfrDDebut", EOSortOrdering.CompareAscending)));
			if (postesPFR.count() == 0) {
				throw new Exception("Veuillez modifier le poste " + detail.toPoste().posCode() + "car il n'a pas de coefficient de PFR défini pour la période du " + DateCtrl.dateToString(debutPeriode) + " au " + DateCtrl.dateToString(finPeriode));
			}
			java.util.Enumeration e1 = postesPFR.objectEnumerator();
			while (e1.hasMoreElements()) {
				NSTimestamp dateDebut = debutPeriode, dateFin = finPeriode;	// Restreindre sur la période du poste PFR
				EOPostePFR postePFR = (EOPostePFR)e1.nextElement();
				if (DateCtrl.isAfter(postePFR.ppfrDDebut(),dateDebut)) {
					dateDebut = postePFR.ppfrDDebut();
				}
				if (postePFR.ppfrDFin() != null && DateCtrl.isBefore(postePFR.ppfrDFin(), dateFin)) {
					dateFin = postePFR.ppfrDFin();
				}
				// Vérifier maintenant si il y a des intersections communes avec les périodes déjà évaluées pour les réorganiser
				// Il n'y a pas de chevauchement des périodes puisqu'on recasse en autant de morceaux que de périodes
				// qui se chevauchent;
				LogManager.logDetail("debut " + DateCtrl.dateToString(dateDebut) + " fin " + DateCtrl.dateToString(dateFin));

				java.util.Enumeration e2 = periodesAvecCoefficients.objectEnumerator();
				NSMutableArray periodesAAJouter = new NSMutableArray();
				// On considère que c'est une quotité à 100% sauf si elle est fournie
				double quotitePoste = 100;
				if (detail.affQuotite() != null) {
					quotitePoste = detail.affQuotite().doubleValue();
				}
				boolean termine = false;
				while (e2.hasMoreElements() && !termine) {
					PeriodeAvecCoefficients periode = (PeriodeAvecCoefficients)e2.nextElement();
					LogManager.logDetail("debut periode " + DateCtrl.dateToString(periode.debutPeriode()) + " fin " + DateCtrl.dateToString(periode.finPeriode()));
					IntervalleTemps intervalle = IntervalleTemps.intersectionPeriodes(periode.debutPeriode(), periode.finPeriode(), dateDebut, dateFin);
					LogManager.logDetail("intervalle " + intervalle);
					if (intervalle != null) {
						// Il y a une intersection commune. Vérifier si il faudra créer des nouvelles périodes
						if (DateCtrl.isBefore(periode.debutPeriode(), intervalle.debutPeriode())) {
							// Il y a une partie non commune en début de période, ajouter cette période
							periodesAAJouter.addObject(periode.dupliquerAvecPeriode(periode.debutPeriode, DateCtrl.jourPrecedent(intervalle.debutPeriode())));
							periode.setDebutPeriode(intervalle.debutPeriode());
							// On sait que le traitement est terminé puisque les périodes ne se chevauchent pas
							termine = true;
						}
						if ((periode.finPeriode() == null && intervalle.finPeriode() != null) || (periode.finPeriode() != null && DateCtrl.isAfter(periode.finPeriode(), intervalle.finPeriode()))) {
							// Il y a une partie non commune en fin de période, ajouter cette période
							periodesAAJouter.addObject(periode.dupliquerAvecPeriode(DateCtrl.jourSuivant(intervalle.finPeriode()), periode.finPeriode()));
							periode.setFinPeriode(intervalle.finPeriode());
							dateDebut = DateCtrl.jourSuivant(intervalle.finPeriode());
							// On sait que le traitement est terminé puisque les périodes ne se chevauchent pas
							termine = true;
						}
						// Ajouter le coefficient du poste trouvé à cette période
						periode.addQuotiteEtCoefficient(quotitePoste,postePFR.ppfrCoef().doubleValue());
						// Vérifier si il y a une partie de postePFR qui n'a pas été traitée
						if (DateCtrl.isBefore(dateDebut,intervalle.debutPeriode())) {
							periodesAAJouter.addObject(new PeriodeAvecCoefficients(dateDebut,DateCtrl.jourPrecedent(intervalle.debutPeriode()),new CoefficientQuotite(quotitePoste,postePFR.ppfrCoef().doubleValue())));
						}
						// On vérifie maintenant si postePFR a encore une partie non couverte en quel cas, on modifie dateDebut
						//if ((dateFin == null && intervalle.finPeriode() != null) || (dateFin != null && DateCtrl.isAfter(dateFin,intervalle.finPeriode()))) {
							dateDebut = DateCtrl.jourSuivant(intervalle.finPeriode());
						//}
					} 
				}
				// Si il reste encore un morceau i.e dateDebut < dateFin alors on l'ajoute et on retrie tous les morceaux
				if (dateFin == null || (DateCtrl.isBefore(dateDebut, dateFin))) {
					periodesAAJouter.addObject(new PeriodeAvecCoefficients(dateDebut,dateFin,new CoefficientQuotite(quotitePoste,postePFR.ppfrCoef().doubleValue())));
				}
				periodesAvecCoefficients.addObjectsFromArray(periodesAAJouter);

				// Retrier les périodes
				EOSortOrdering.sortArrayUsingKeyOrderArray(periodesAvecCoefficients, new NSArray(EOSortOrdering.sortOrderingWithKey("debutPeriode", EOSortOrdering.CompareAscending)));
				LogManager.logDetail("periodesAvecCoefficients\n" + periodesAvecCoefficients);
			}
		}
		return periodesAvecCoefficients;
	}
	public class PeriodeAvecCoefficients implements NSKeyValueCoding {
		private NSTimestamp debutPeriode;
		private NSTimestamp finPeriode;
		private NSMutableArray coefficients;

		public PeriodeAvecCoefficients(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
			this.debutPeriode = debutPeriode;
			this.finPeriode = finPeriode;
		}
		public PeriodeAvecCoefficients(NSTimestamp debutPeriode,NSTimestamp finPeriode,CoefficientQuotite coefficientQuotite) {
			this(debutPeriode,finPeriode);
			coefficients = new NSMutableArray(coefficientQuotite);
		}
		public NSTimestamp debutPeriode() {
			return debutPeriode;
		}
		public void setDebutPeriode(NSTimestamp debutPeriode) {
			this.debutPeriode = debutPeriode;
		}
		public NSTimestamp finPeriode() {
			return finPeriode;
		}
		public void setFinPeriode(NSTimestamp finPeriode) {
			this.finPeriode = finPeriode;
		}
		public NSMutableArray coefficients() {
			return coefficients;
		}
		public void setCoefficients(NSMutableArray coefficients) {
			this.coefficients = coefficients;
		}
		public void addQuotiteEtCoefficient(double quotite,double coefficient) {
			coefficients.addObject(new CoefficientQuotite(quotite,coefficient));
		}
		public PeriodeAvecCoefficients dupliquerAvecPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
			PeriodeAvecCoefficients newPeriode = new PeriodeAvecCoefficients(debutPeriode,finPeriode);
			newPeriode.setCoefficients(new NSMutableArray(coefficients));
			return newPeriode;
		}
		public String toString() {
			return "debut : " + debutPeriode() + ", fin : " + finPeriode() + ", coefficients : " + coefficients();
		}
		// NSKeyValueCoding
		public void takeValueForKey(Object value, String key) {
			NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, value, key);

		}
		public Object valueForKey(String key) {
			return NSKeyValueCoding.DefaultImplementation.valueForKey(this, key);
		}
	}
	public class CoefficientQuotite {
		private double quotite;
		private double coefficient;
		public CoefficientQuotite(double quotite,double coefficient) {
			this.quotite = quotite;
			this.coefficient = coefficient;
		}
		public double quotite() {
			return quotite;
		}
		public double coefficient() {
			return coefficient;
		}
		public String toString() {
			return "quotite : " + quotite + ", coefficient : " + coefficient;
		}
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class PluginAvecParametresPersonnels extends PluginAvecParametresStandard {
	public void calculerMontantsPrime(InformationPourPluginPrime information) {
		LogManager.logDetail(this.getClass().getName() + " - calculerMontantsPrime");
		// Récupérer les personnalisations de l'utilisateur pour cette période
		LogManager.logDetail(this.getClass().getName() + " - personnalisations " + information.personnalisations());
		LogManager.logDetail(this.getClass().getName() + " - peutCalculer sans param perso " + peutCalculerSansParametrePersonnel());

		if (information.personnalisations() == null || information.personnalisations().count() == 0) {
			if (peutCalculerSansParametrePersonnel()) {
				ajouterMontant(information,null);
			} else {
				// Ajouter juste un résultat pour signaler que l'individu y a droit avec un montant égal à 0
				PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),new BigDecimal(0), information.debutPeriode(), information.finPeriode(),"Les valeurs personnalisées sont à définir");
			}
		} else {
			java.util.Enumeration e = information.personnalisations().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimePersonnalisation personnalisation = (EOPrimePersonnalisation)e.nextElement();
				ajouterMontant(information,personnalisation);
			}
		}
	}
	// Méthodes protégées
	/** Retourne true si un plugin peut faire des calculs sans utiliser les param&egrave;tres personnels */
	protected abstract boolean peutCalculerSansParametrePersonnel();
	/** effectue les calculs &grave; partir des informations personnelles. Les calculs retournent des montants annuels quelque soit
	 * la dur&eacute;e de la p&eacute;riode. Ils sont proratis&eacute;s plus tard */
	protected abstract void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite,NSTimestamp finValidite);
	/** effectue les calculs &agrave; partir des param&egrave;tres personnels. Les calculs retournent des montants annuels quelque soit
	 * la dur&eacute;e de la p&eacute;riode. Ils sont proratis&eacute;s plus tard */
	protected abstract void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite,NSTimestamp finValidite);
	// Méthodes privées
	private void ajouterMontant(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation) {
		NSArray parametresPersos = null;
		NSTimestamp dateDebut = information.debutPeriode(),dateFin = information.finPeriode();
		if (personnalisation != null) {
			parametresPersos = information.parametresPersonnels(personnalisation);
			dateDebut = personnalisation.debutValidite();
			dateFin = personnalisation.finValidite();
		}
		if (parametresPersos == null || parametresPersos.count() == 0) {
			if (!peutCalculerSansParametrePersonnel() && personnalisation == null) {
				// Ajouter juste un résultat pour signaler que l'individu y a droit avec un montant égal à 0
				PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),new BigDecimal(0), dateDebut,dateFin,"Les valeurs personnalisées sont à définir");
			} else {
				// Demander au composant de retourner le montant
				effectuerCalculPourDates(information,personnalisation,dateDebut,dateFin);
			}
		} else {
			effectuerCalculPourDates(information,parametresPersos,dateDebut,dateFin);
		}
	}

}

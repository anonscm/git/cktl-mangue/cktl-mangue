/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
 package org.cocktail.mangue.common.primes.plugins;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOGoyaveParametres;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class CalculChAdminSga extends PluginAvecParametresStandard {
	private final static String MONTANT_CATEGORIE = "MOICSGA";
	/** Refus si l'individu est loge en necessite de service */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individuPourPrime) {
		if (individuPourPrime.individu().personnel().toLoge() != null && individuPourPrime.individu().personnel().toLoge().estNecessiteService()) {
			return "Individu logé pour nécessité de service";
		} else {
			return null;
		}
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return true;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	public void calculerMontantsPrime(InformationPourPluginPrime information) {
		// Récupérer la catégorie de l'établissement dans les paramètres de Goyave
		String categorie = EOGoyaveParametres.valeurParametrePourCle(information.editingContext(), "CATEGORIE_ETABLISSEMENT");
		if (categorie == null) {
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Voyez avec votre administrateur. Il faut fournir dans les paramètres de Goyave la catégorie de l'établissement", information.debutPeriode(), information.finPeriode());
		} else {
			String code = MONTANT_CATEGORIE + categorie;
			NSArray parametres = parametresValidesPourCodePeriodeEtQualifier(information.editingContext(), code, information.debutPeriode(), information.finPeriode(), null);
			parametres = EOSortOrdering.sortedArrayUsingKeyOrderArray(parametres,new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareAscending)));
			java.util.Enumeration e = parametres.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
				if (DateCtrl.isBefore(dateDebut,information.debutPeriode())) {
					dateDebut = information.debutPeriode();
				}
				if (dateFin != null && information.finPeriode() != null && DateCtrl.isAfter(dateFin, information.finPeriode())) {
					dateFin = information.finPeriode();
				}
				if (parametre.pparMontant() == null) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
				} else {
					String formuleCalcul = "Montant pour un établissement de catégorie " + categorie + " : " + parametre.pparMontant();
					PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),parametre.pparMontant(), dateDebut, dateFin,formuleCalcul);
					LogManager.logDetail(formuleCalcul);
				}
			}
		}

	}
	public String modeCalculDescription() {
		return "Montant fixe fonction de la catégorie de l'établissement";
	}
	public boolean peutModifierMontantPrime() {
		return false;
	}
	

}

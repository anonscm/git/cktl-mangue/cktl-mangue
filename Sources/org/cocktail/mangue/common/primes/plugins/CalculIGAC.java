/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
 package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.EOLoge;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOGoyaveParametres;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Le montant annuel de l'Indemnit&eacute; de Gestion des Agents Comptables est un montant qui est fonction de la
 * cat&eacute;gorie de l'&eacute;tablissement. Il est major&eacute;r&eacute; si l'individu n'est pas log&eacute; par l'&eacute;tablissement
 * ou par n&eacute;cessit&eacute; de service et minor&eacute; dans le cas contraire.
 * 
 */
public class CalculIGAC extends PluginAvecParametresStandard {
	private final static String TAUX_MAJORATION = "TXMAIGAC";
	private final static String TAUX_MINORATION = "TXMIIGAC";
	private final static String MONTANT_POUR_CATEGORIE = "MOANAGC";

	/** pas de crit&egrave;re de refus sp&eacute;ficique */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** pas de refus sp&eacute;ficique */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	public void calculerMontantsPrime(InformationPourPluginPrime information) {
		String codeTaux = TAUX_MINORATION;
		EOLoge typeLogement = information.individu().personnel().toLoge();
		if (typeLogement == null || typeLogement.code().equals("N")) {
			// Non log
			codeTaux = TAUX_MAJORATION;
		}		
		String categorie = EOGoyaveParametres.valeurParametrePourCle(information.editingContext(), "CATEGORIE_ETABLISSEMENT");
		if (categorie == null) {
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculIGAC- Voyez avec votre administrateur. Il faut fournir dans les paramètres de Goyave la catégorie de l'établissement", information.debutPeriode(), information.finPeriode());
		} else {
			String code = MONTANT_POUR_CATEGORIE + categorie;
			NSArray parametres = parametresValidesPourCodePeriodeEtQualifier(information.editingContext(), code, information.debutPeriode(), information.finPeriode(), null);
			parametres = EOSortOrdering.sortedArrayUsingKeyOrderArray(parametres,new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareAscending)));
			java.util.Enumeration e = parametres.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
				if (DateCtrl.isBefore(dateDebut,information.debutPeriode())) {
					dateDebut = information.debutPeriode();
				}
				if (dateFin != null && information.finPeriode() != null && DateCtrl.isAfter(dateFin, information.finPeriode())) {
					dateFin = information.finPeriode();
				}
				if (parametre.pparMontant() == null) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
				} else {
					NSArray parametresPourTaux = parametresValidesPourCodePeriodeEtQualifier(information.editingContext(), codeTaux, dateDebut, dateFin, null);
					String texte = "de minoration en cas de logement par l'établissement";
					if (codeTaux.equals(TAUX_MAJORATION)) {
						texte = "de majoration lorsque l'individu n'est pas logé par l'établissement";
					}
					if (parametresPourTaux.count() == 0) {
						PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculIGAC- Voyez avec votre administrateur. Le taux " + texte + " n'est pas défini",information.debutPeriode(), information.finPeriode());
					} else {
						java.util.Enumeration e1 = parametresPourTaux.objectEnumerator();
						while (e1.hasMoreElements()) {
							EOPrimeParam paramTaux = (EOPrimeParam)e1.nextElement();
							if (paramTaux.pparTaux() == null) {
								PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculIGAC - Voyez avec votre administrateur. Le taux " + texte + " a une valeur nulle",information.debutPeriode(), information.finPeriode());
							} else {
								String formuleCalcul = "Montant pour un établissement de catégorie " + categorie + " : " + parametre.pparMontant() + "\n";
								double montant = parametre.pparMontant().doubleValue();
								double taux = paramTaux.pparTaux().doubleValue();
								if (codeTaux.equals(TAUX_MAJORATION)) {
									formuleCalcul += "Montant majoré en l'absence de logement par l'établissement\n" + montant + " x (1 + (" + taux + "/100))";
									montant = montant * (1 + (taux/100));
								} else {
									formuleCalcul += "Montant minoré car l'individu est logé par l'établissement\n" + montant + " x (1 - (" + taux + "/100))";
									montant = montant * (1 - (taux/100));
								}
								PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),new BigDecimal(montant).setScale(2,BigDecimal.ROUND_UP), dateDebut, dateFin,formuleCalcul);
								LogManager.logDetail(formuleCalcul);
							}
						}
					}
				}
			}
		}
	}
	public String modeCalculDescription() {
		return "Montant fonction de la catgorie de l'établissement majoré ou minoé si l'individu n'est pas logé par l'établissement";
	}

	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** Montant non modifiable */
	public boolean peutModifierMontantPrime() {
		return false;
	}

}

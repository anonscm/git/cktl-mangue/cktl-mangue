/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
/** Le montant annuel de la PFR est un montant personnalis&eacute; qui est compris entre deux valeurs estim&eacute; &agrave; partir
 * d'une part de Fonctions et d'une part de R&eacute;sultat. Le coefficient de la part R&eacute;sultat est un param&grave;tre personnel.
 * La part R&eacute;sultat peut &ecirc;tre une prime mensuelle ou une prime exceptionnelle (correspond &agrave; deux primes). <BR>
 * Si la PFR exceptionnelle est cumul&eacute;e avec une PFR RESULTAT, le total de l'attribution doit rester dans les limites impos&eacute;es 
 * &agrave; la part R&eacute;sultat.<BR>
 * Le calcul indiquera ces valeurs minimum et maximum
 * 
 */

public class CalculPFR_Resultat extends PluginAvecParametresPersonnelsEtValidation {
	private final static String COEFFICIENT_MINIMUM_PART_RESULTAT = "COMIPFRR";
	private final static String COEFFICIENT_MAXIMUM_PART_RESULTAT = "COMXPFRR";
	private final static String COEFFICIENT_PART_RESULTAT = "COEFPFRR";
	private final static String MONTANT_PART_RESULTAT = "MOPFRR";
	private final static String CODE_INDEMNITE_PRIME_RESULTAT = "1549";
	private final static String CODE_INDEMNITE_PRIME_EXCEPTIONNELLE = "1550";

	private String formuleCalculResultat;
	/** pas de refus */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** pas de param&egrave;tre personnel mais une personnalisation */
	public String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime) {
		LogManager.logDetail(getClass().getName() + " - Vérification paramètres personnels");
		if (parametrePerso.code().equals(COEFFICIENT_PART_RESULTAT)) {
			// Vérifier si il est compris entre les valeurs minimum et maximum
			try {
				double coefficient = new Double(parametrePerso.valeur()).doubleValue();
				if (coefficient < 0) {
					LogManager.logDetail("Le coefficient de résultat est une valeur positive");
					return "Le coefficient de résultat est une valeur positive";
				}
				try {
					double coefficientMin = evaluerCoefficientPourIndividu(individuPourPrime.individu(),individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),false);
					double coefficientMax = evaluerCoefficientPourIndividu(individuPourPrime.individu(),individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),false);
					if (coefficient < coefficientMin) {
						String message = "Le coefficient de résultat doit être supérieur à " + coefficientMin;
						LogManager.logDetail(message);
						return message;
					}
					if (coefficient > coefficientMax) {
						String message = "Le coefficient de résultat doit être inférieur à " + coefficientMax;
						LogManager.logDetail(message);
						return message;
					}
					return null;
				} catch (Exception exc1) {	// si les coefficients ne sont pas définis
					return exc1.getMessage();
				}
			} catch (Exception exc) {
				return "Le coefficient de résultat est un nombre";
			}

		} 
		return null;
	}
	/** la PFR est un montant personnalis&eacute; */
	public String modeCalculDescription() {
		return "Part de Résultat dont le coefficient est individuel";
	}
	/** V&eacute;rification que le coefficient de R&eacute;sultat/Exceptionnel est compris entre les valeurs minimum et maximum. On v&eacute;rifie
	 * aussi qu'il n'existe pas une attribution de PFR Exceptionnel/R&eacute;sultat pour limiter le coefficient */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		if (montant == null) {
			return "CalculPFR_Resultat - Le coefficient à vérifier est nul";
		}
		LogManager.logDetail("CalculPFR_Resultat - Vérification du coefficient de résultat");
		try {
			double coefficient = montant.doubleValue();
			if (coefficient < 0) {
				LogManager.logDetail("Le coefficient de résultat est une valeur positive");
				return "Le coefficient de résultat est une valeur positive";
			}
			try {
				double coefficientMin = evaluerCoefficientPourIndividu(individuPourPrime.individu(),individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),true);
				double coefficientMax = evaluerCoefficientPourIndividu(individuPourPrime.individu(),individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),false);
				if (coefficient < coefficientMin) {
					String message = "Le coefficient de résultat doit être supérieur à " + coefficientMin;
					LogManager.logDetail(message);
					return message;
				}
				if (coefficient > coefficientMax) {
					String message = "Le coefficient de résultat doit être inférieur à " + coefficientMax;
					LogManager.logDetail(message);
					return message;
				}
				String codePrime = CODE_INDEMNITE_PRIME_EXCEPTIONNELLE;
				String messagePrime = "PFR exceptionnelle";
				if (prime.primCodeIndemnite().equals(CODE_INDEMNITE_PRIME_EXCEPTIONNELLE)) {
					codePrime = CODE_INDEMNITE_PRIME_RESULTAT;
					messagePrime = "PFR part résultat";
				}
				EOPrime autrePrime = EOPrime.rechercherPrimePourCodeIndemnite(prime.editingContext(), codePrime, debutPeriode, finPeriode);
				if (autrePrime != null) {
					EOPrimePersonnalisation personnalisation = EOPrimePersonnalisation.rechercherPersonnalisationEnCoursPourIndividuPrimeEtPeriode(prime.editingContext(), individuPourPrime.individu(), autrePrime,debutPeriode,finPeriode);
					if (personnalisation != null && personnalisation.pmpeMontant() != null) {
						// Récupérer son coefficient et vérifier qu'il ne peut pas dépasser le maximum
						double coefficientPrime = personnalisation.pmpeMontant().doubleValue();
						if (coefficient + coefficientPrime > coefficientMax) {
							return "Cet individu a déjà une attribution de " + messagePrime + ", le coefficient ne peut dépasser " + (coefficientMax - coefficientPrime);
						}
					}
				}
				return null;
			} catch (Exception exc1) {	// si les coefficients ne sont pas définis
				return exc1.getMessage();
			}
		} catch (Exception exc) {
			return "Le coefficient de résultat est un nombre";
		}

	}
	// Méthodes protégées
	/** Peut calculer sans param&egrave;tre personnel */
	protected boolean peutCalculerSansParametrePersonnel() {
		return true;
	}
	/** pas de param&egrave;tre personnel mais une personnalisation */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
	}
	/** En l'absence de personnalisation, retourne le montant minimum de la part r&eacute;sultat/exceptionnelle pour la p&eacute;riode en v&eacute;rifiant si
	 * il existe une attribution exceptionnelle/r&eacute;sultat */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information, EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {
		LogManager.logDetail("CalculPFR_Resultat - Calcul du montant de l'attribution");
		try {
			double coefficientResultat = 0;
			if (personnalisation != null) {
				coefficientResultat = personnalisation.pmpeMontant().doubleValue();
			}
			double montantPartResultat = montantPourPeriodeEtCoefficient(information.individu(),information.gradeIndividu(),coefficientResultat,debutValidite, finValidite,true);
			String formuleCalcul = formuleCalculResultat + "\n" + "Part Résultat : " + montantPartResultat;
			LogManager.logDetail(formuleCalcul);
			BigDecimal montantRetour = new BigDecimal(montantPartResultat).setScale(2,BigDecimal.ROUND_HALF_UP);
			PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montantRetour, debutValidite,finValidite,formuleCalcul);
		} catch (Exception exc) {
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}
	}
	// Méthodes privées
	private NSArray montantsPourCorpsEtPeriode(EOGrade grade,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		// Vérifier qu'on et bien entre les taux moyen et exceptionnel en recherchant ces paramètres pour le grade de l'individu
		if (grade == null) {
			throw new Exception("CalculPFR_Resultat - Grade inconnu");
		}
		String code = MONTANT_PART_RESULTAT;
		
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("grade = %@", new NSArray(grade));		
		NSArray parametresMontant = EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), code, debutPeriode, finPeriode,qualifier);
		if (parametresMontant.count() == 0) {
			// Si on n'en trouve pas, vérifier si il n'en n'existe pas pour le corps
			qualifier = EOQualifier.qualifierWithQualifierFormat("corps = %@", new NSArray(grade.toCorps()));		
			parametresMontant = EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(grade.editingContext(), code, debutPeriode, finPeriode,qualifier);
			if (parametresMontant.count() == 0) {
				throw new Exception("CalculPFR_Resultat - Contactez l'administrateur, pas de paramètre de montant défini pour le corps " + grade.toCorps().lcCorps()+ " ou pour le grade " + grade.lcGrade());
			}
		}
		return parametresMontant;
	}
	private double montantPourPeriodeEtCoefficient(EOIndividu individu,EOGrade grade,double coefficient,NSTimestamp debutValidite, NSTimestamp finValidite, boolean estMinimum) throws Exception {
		// Calcule le montant de la part en résultat du coefficient
		// Si le coefficient est négatig, calcul les parts mini et maxi selon le booléen estMinimum
		NSArray montants = montantsPourCorpsEtPeriode(grade, debutValidite, finValidite);
		double montantCourant = 0;
		formuleCalculResultat = "";
		java.util.Enumeration e = montants.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
			NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
			if (DateCtrl.isBefore(dateDebut,debutValidite)) {
				dateDebut = debutValidite;
			}
			if (dateFin == null || (dateFin != null && finValidite != null && DateCtrl.isAfter(dateFin, finValidite))) {
				dateFin = finValidite;
			}
			if (parametre.pparMontant() == null) {
				PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(individu,"Le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
			} else {
				double montant = parametre.pparMontant().doubleValue();
				if (montant == 0) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(individu,"CalculPFR_Resultat - Contactez l'administrateur, Le montant minimum de la part Résultat de la PFR pour la période  est nul", dateDebut, dateFin);
					return -1;
				}
				montantCourant = evaluerMontantPourParametre(individu,parametre,montantCourant,coefficient,estMinimum);
			}
		}
		return montantCourant;
	}
	private double evaluerMontantPourParametre(EOIndividu individu, EOPrimeParam parametre,double montantCourant,double coefficient, boolean estMinimum) throws Exception {
		boolean estValeurIndividuelle = coefficient >= 0;
		if (coefficient < 0) {	// On passe une valeur négative quand on calcule avec les valeurs réelles
			coefficient = evaluerCoefficientPourIndividu(individu,parametre.debutValidite(),parametre.finValidite(),estMinimum);
		}
		double montant = parametre.pparMontant().doubleValue();
		if (montantCourant == 0 || (estMinimum && montant < montantCourant) || (!estMinimum && montant > montantCourant)) {
			String formuleCalcul = "";
			formuleCalcul = "Part Résultat\nMontant de Référence : " + montant + "\n";
			formuleCalcul += "Coefficient multiplicateur : " + coefficient;
			if (individu.personnel().toLoge() != null && individu.personnel().toLoge().estNecessiteService()) {
				formuleCalcul += " (individu logé par nécessité de service)";
			}
			if (!estValeurIndividuelle) {
				if (estMinimum) {
					formuleCalcul += "\nMontant minimum : ";
				} else {
					formuleCalcul += "\nMontant maximum : ";
				}
			} else {
				formuleCalcul += "\nMontant : ";
			}
			formuleCalcul += montant + " x " + coefficient + " (montant référence x coefficient)";
			formuleCalculResultat = formuleCalcul;
			montantCourant = montant * coefficient;
		}
		return montantCourant;
	}
	private double evaluerCoefficientPourIndividu(EOIndividu individu, NSTimestamp debutValidite, NSTimestamp finValidite, boolean estMinimum) throws Exception {
		String code = "";
		if (estMinimum) {
			code = COEFFICIENT_MINIMUM_PART_RESULTAT;
		} else {
			code = COEFFICIENT_MAXIMUM_PART_RESULTAT;
		}
		NSArray coefficients = EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(individu.editingContext(), code, debutValidite, finValidite, null);
		// Si il s'agit de la valeur minimum on prend le coefficient maximum, pour le maximum la valeur minimum
		// pour être sûr d'être dans le bon intervalle sur la période
		double coefficient = 0;
		java.util.Enumeration e = coefficients.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametreCoeff = (EOPrimeParam)e.nextElement();
			if (parametreCoeff.pparEntier() == null) {
				throw new Exception("CalculPFR_Resultat - Le coefficient du paramètre " + parametreCoeff.pparLibelle() + " n'est pas défini");
			}
			double coeff = parametreCoeff.pparEntier().doubleValue();
			if (estMinimum) {
				if (coefficient == 0 || coeff > coefficient) {
					coefficient = coeff;
				}
			} else {
				if (coefficient == 0 || coeff < coefficient) {
					coefficient = coeff;
				}
			}
		}
		return coefficient;
	}
	
	
}

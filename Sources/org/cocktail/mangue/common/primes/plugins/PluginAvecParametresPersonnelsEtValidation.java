/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.primes.InterfacePluginValidation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;

import com.webobjects.foundation.NSArray;

public abstract class PluginAvecParametresPersonnelsEtValidation extends PluginAvecParametresPersonnels implements InterfacePluginValidation {
	/** On ne peut pas modifier le montant d'une prime avec des param&egrave;tres personnels */
	public boolean peutModifierMontantPrime() {
		return false;
	}
	/** V&eacute;rifie que tous les param&egrave;tres personnels sont corrects. D&eacute;l&egrave;gue &agrave; la sous-classe 
	 * la v&eacute;rification r&eacute;elle. Retourne le diagnostic ou null si pas d'erreur
	 */
	public String verifierParametresPersonnels(NSArray parametresPersonnels,IndividuPourPrime individu) {
		LogManager.logDetail(getClass().getName() + " - Vérification paramètres personnels");
		if (parametresPersonnels == null || parametresPersonnels.count() == 0) {
			return null;
		}
		java.util.Enumeration e = parametresPersonnels.objectEnumerator();
		while (e.hasMoreElements()) {
			ParametrePersonnel parametrePerso = (ParametrePersonnel)e.nextElement();
			if (parametrePerso.valeur() == null || parametrePerso.valeur().length() == 0) {
				return getClass().getName() + " - Vous devez fournir une valeur pour le paramètre personnel " + parametrePerso.code();
			}
			String erreur = verifierParametrePourIndividu(parametrePerso,individu);
			if (erreur != null) {
				LogManager.logDetail(erreur);
				return erreur;
			}
		}
		return null;
	}
	/** V&eacute,rifie qu'un param&grave;tre personnel est valide. La pr&eacute;sence d'une valeur est d&eacute;j&agrave; effectu&eacute;
	 * lorsque cette m&eacute;thode est appel&eacute;e.
	 * Retourne le diagnostic ou null si pas d'erreur */
	protected abstract String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime);

}

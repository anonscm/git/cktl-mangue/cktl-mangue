/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Le calcul de la PPRS est fonction du taux de la PPRS, de l'indice de r&eacute;f&eacute;rence : <BR>
 * (indice x valeur du point d'indice)) x taux  / 100
 * 
 */
public class CalculPPRS extends PluginAvecParametresPersonnelsEtValidation {
	private final static String TAUX_EXCEPTIONNEL = "TXEXPPRS";
	private final static String TAUX_MOYEN = "TXMYPPRS";
	private final static String VALEUR_POINT_INDICE = "VALPTIND";

	/** pas de refus */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** le taux personnel doit au maximum atteindre le taux exceptionnel */
	public String verifierValiditeMontant(BigDecimal tauxPPRS,IndividuPourPrime individuPourPrime,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		LogManager.logDetail("CalculPPRS - Verification Parametre Personnel");
		// Récupérer la valeur et la transformer en double
		double taux = tauxPPRS.doubleValue();
		// Vérifier qu'on est bien entre les taux moyen et exceptionnel en recherchant ces paramètres pour le grade de l'individu
		if (individuPourPrime.grade() == null) {
			return "CalculPPRS - Grade inconnu";
		}
		LogManager.logDetail("CalculPPRS - Grade individu " + individuPourPrime.grade().cGrade() + ", taux vérifié : " + taux);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("grade = %@", new NSArray(individuPourPrime.grade()));
		NSArray parametresExceptionnel = parametresValidesPourCodePeriodeEtQualifier(individuPourPrime.individu().editingContext(), TAUX_EXCEPTIONNEL, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin(),qualifier);
		if (parametresExceptionnel.count() == 0) {
			return null;
		}
		java.util.Enumeration e = parametresExceptionnel.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametreExceptionnel = (EOPrimeParam)e.nextElement();
			String periode = "du " + parametreExceptionnel.debutValiditeFormatee();
			if (parametreExceptionnel.finValidite() != null) {
				periode += " au " + parametreExceptionnel.finValiditeFormatee();
			}
			if (parametreExceptionnel.pparTaux() == null) {
				return "CalculPPRS - Contactez l'administrateur, pas de taux défini pour le paramètre avec le code " + TAUX_EXCEPTIONNEL + " pour la période " + periode;
			}
			double tauxMax = parametreExceptionnel.pparTaux().doubleValue();
			if (taux < 0) {
				return "CalculPPRS - Le taux choisi doit être positif. Veuillez changer la valeur du taux";
			}
			if (taux > tauxMax) {
				return "CalculPPRS - Le taux choisi est supérieur au taux exceptionnel (" + tauxMax + "). Veuillez changer la valeur du taux";
			}
		}
		return null;
	}
	/** (indice x valeur du point d'indice)) x taux  / 100 */
	public String modeCalculDescription() {
		return " (indice x valeur du point d'indice)) x taux / 100";
	}
	// Méthodes protégées
	/** Pas de v&eacute;rification de validit&eacute; */
	protected String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPrime) {
		return null;
	}
	/** Peut calculer sans param&egrave;tre personnel */
	protected  boolean peutCalculerSansParametrePersonnel() {
		return true;
	}
	/** pas de param&grave;tre personnel */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {

	}
	/** En l'absence de personnalisation, utilise les taux moyens pour la p&eacute;riode */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {		
		LogManager.logDetail("CalculPPRS - Calcul du montant de l'attribution");
		try {
			// Commencer par vérifier si un taux personnel a été fourni
			double tauxPerso = 0;
			if (personnalisation != null) {
				tauxPerso = personnalisation.pmpeMontant().doubleValue();
				LogManager.logDetail("CalculPPRS - Taux individuel " + tauxPerso);
			}
			NSArray tauxMoyens = information.parametresPourCodeGradeEtDates(TAUX_MOYEN, information.gradeIndividu(),debutValidite, finValidite);
			NSArray valeursPointIndice = information.parametresPourCodeEtDates(VALEUR_POINT_INDICE, debutValidite, finValidite);
			// Tous les paramètres sont bons, on peut maintenant déterminer tous les intervalles pour lesquels il faut des attributions
			// on trouvera alors juste un paramètre pour chaque intervalle
			NSArray intervalles = new NSArray(new Utilitaires.IntervalleTemps(debutValidite,finValidite));
			intervalles = determinerIntervallesPourIntervallesEtParametres(intervalles, tauxMoyens);
			intervalles = determinerIntervallesPourIntervallesEtParametres(intervalles, valeursPointIndice);
			// On boucle sur les valeurs du point d'indice car ce sont celles qui sont le plus à même de varier
			java.util.Enumeration e = intervalles.objectEnumerator();
			while (e.hasMoreElements()) {
				IntervalleTemps intervalle = (IntervalleTemps)e.nextElement();
				NSTimestamp dateDebut = intervalle.debutPeriode(), dateFin = intervalle.finPeriode();
				EOPrimeParam parametre = parametrePourPeriode(valeursPointIndice,intervalle.debutPeriode(),intervalle.finPeriode());
				if (parametre.pparMontant() == null) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculPPRS - Contactez l'administrateur, Le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
				} else {
					double valeurPoint = parametre.pparMontant().doubleValue();
					// Rechercher l'indice de référence de cette période dans les taux moyens
					EOPrimeParam paramMoyen = parametrePourPeriode(tauxMoyens,intervalle.debutPeriode(),intervalle.finPeriode());
					if (paramMoyen == null) {
						PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculPPRS - Contactez l'administrateur, Pas de paramètre avec le code " + TAUX_MOYEN + " pour la période ", dateDebut, dateFin);
					} else if (paramMoyen.pparIndice() == null) {
						PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculPPRS - Contactez l'administrateur, Le paramètre avec le code " + TAUX_MOYEN + " n'a pas d'indice défini pour la période ", dateDebut, dateFin);
					} else if (tauxPerso == 0 && paramMoyen.pparTaux() == null) {
						PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculPPRS - Contactez l'administrateur, Le paramètre avec le code " + TAUX_MOYEN + " n'a pas de taux défini pour la période ", dateDebut, dateFin);
					} else {
						int indice = new Integer(paramMoyen.pparIndice()).intValue();
						String tauxUtilise = "taux de l'individu : " + tauxPerso;
						double taux = tauxPerso;
						if (tauxPerso == 0) {
							taux = paramMoyen.pparTaux().doubleValue();
							tauxUtilise = "taux moyen : " + taux;
							LogManager.logDetail("CalculPPRS - Taux moyen " + taux);
						}
						if (taux == 0) {
							PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"CalculPPRS - Contactez l'administrateur, Le taux moyen pour la période  est nul", dateDebut, dateFin);
						} else {
							double montant = (valeurPoint * indice * taux) / 100;
							String formuleCalcul = "Valeur Point Indice : " + valeurPoint + " - Indice de référence pour grade " + information.gradeIndividu().cGrade() + " : " + indice + " - " + tauxUtilise + "\nCalcul : ";
							formuleCalcul += "(" + valeurPoint + " x " + indice + " x " + taux + ") / 100";
							BigDecimal montantRetour = new BigDecimal(montant);
							if (tauxPerso != 0) {	// pour afficher le montant moyen
								formuleCalcul += "\n" + "Montant annuel avec taux moyen (" + paramMoyen.pparTaux() + ") : ";
								double montantMoyen = (valeurPoint * indice * paramMoyen.pparTaux().doubleValue()) / 100;
								formuleCalcul += new BigDecimal(montantMoyen).setScale(2,BigDecimal.ROUND_HALF_UP).toString();
							}
							LogManager.logDetail(formuleCalcul);
							PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montantRetour.setScale(2,BigDecimal.ROUND_HALF_UP), dateDebut, dateFin,formuleCalcul);
						}
					}
				}
			}
		} catch (Exception exc) {
			exc.printStackTrace();
			LogManager.logException(exc);
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}	}
	
}

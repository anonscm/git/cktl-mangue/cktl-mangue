/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.primes.InterfacePluginPrime;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.IntervalleTempsComparator;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class PluginAvecParametresStandard implements InterfacePluginPrime {
	/** Retourne les param&egrave;tres valides pour une p&eacute;riode et un qualifier
	 * @param code (String correspond au pcodCode)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 * @param qualifier autre qualifier &agrave; appliquer (peut &ecirc;tre nul)
	 * @return tableau de EOPrimeParam
	 */
	NSArray parametresValidesPourCodePeriodeEtQualifier(EOEditingContext editingContext,String code,NSTimestamp debutPeriode,NSTimestamp finPeriode, EOQualifier qualifier) {
		return EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(editingContext, code,debutPeriode, finPeriode,qualifier);
	}
	/** V&eacute;rifie si les param&egrave;tres fournis couvrent toute la p&eacute;riode
	 *  @return true si la p&eacute;riode est couverte
	 */
	public boolean parametresValidesSurPeriode(NSArray parametres,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		parametres = EOSortOrdering.sortedArrayUsingKeyOrderArray(parametres, new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareAscending)));
		EOPrimeParam parametre = (EOPrimeParam)parametres.objectAtIndex(0);
		if (Utilitaires.IntervalleTemps.intersectionPeriodes(debutPeriode, finPeriode, parametre.debutValidite(), parametre.finValidite()) == null) {
			// Le paramètre ne couvre pas la période
			return false;
		}
		NSTimestamp dateFin = parametre.finValidite();
		if (dateFin == null || (finPeriode != null && DateCtrl.isAfter(dateFin, finPeriode))) {
			return true;	// on a terminé
		}
		for (int i = 1; i < parametres.count();i++) {
			parametre = (EOPrimeParam)parametres.objectAtIndex(i);
			if (Utilitaires.IntervalleTemps.intersectionPeriodes(debutPeriode, finPeriode, parametre.debutValidite(), parametre.finValidite()) == null) {
				// Le paramètre ne couvre pas la période
				return false;
			}
			if (DateCtrl.dateToString(DateCtrl.jourSuivant(dateFin)).equals(DateCtrl.dateToString(parametre.debutValidite())) == false) {
				// Pas de continuité des dates
				return false;
			}
			if (dateFin == null || (finPeriode != null && DateCtrl.isAfter(dateFin, finPeriode))) {
				return true;	// on a terminé
			} else {
				dateFin = parametre.finValidite();
			}
		}
		return true;
	}
	/** Retourne le param&egrave;tre valide sur la p&eacute;riode pass&eacute;e en param&egrave;tre parmi les param&egrave;tres fournis 
	 *  @param parametres tableau de EOPrimeParam
	 *  @param debutPeriode (peut &ecirc;tre nulle)
	 * 	@param finPeriode (peut &ecirc;tre nulle)
	 *	@return param&egrave;tre trouv&eacute; ou null
	 *  */
	public EOPrimeParam parametrePourPeriode(NSArray parametres,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		java.util.Enumeration e = parametres.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
			if (Utilitaires.IntervalleTemps.intersectionPeriodes(debutPeriode, finPeriode, parametre.debutValidite(), parametre.finValidite()) != null) {
				return parametre;
			}
		}
		return null;
	}
	/** Retourne un message d'erreur si le montant des param&egrave;tres fournis n'est pas d&eacute;fini 
	 *  @param parametres tableau de EOPrimeParam
	 *  */
	public String verifierMontants(NSArray parametres) {
		java.util.Enumeration e = parametres.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
			if (parametre.pparMontant() == null) {
				return "Le montant est nul";
			} else if (parametre.pparMontant().doubleValue() == 0) {
				return "Le montant doit être fourni";
			}
		}
		return null;
	}
	/** Retourne un message d'erreur si le taux des param&egrave;tres fournis n'est pas d&eacute;fini 
	 *  @param parametres tableau de EOPrimeParam
	 *  */
	public String verifierTaux(NSArray parametres) {
		java.util.Enumeration e = parametres.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
			if (parametre.pparTaux() == null) {
				return "Le taux est nul";
			} else if (parametre.pparTaux().doubleValue() == 0) {
				return "Le taux doit être fourni";
			}
		}
		return null;
	}
	/** Retourne un message d'erreur si l'indice des param&egrave;tres fournis n'est pas d&eacute;fini 
	 *  @param parametres tableau de EOPrimeParam
	 *  */
	public String verifierIndices(NSArray parametres) {
		java.util.Enumeration e = parametres.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
			if (parametre.pparIndice() == null) {
				return "L'indice est nul";
			} else if (parametre.pparIndice().length() == 0) {
				return "L'indice doit être fourni";
			}
		}
		return null;
	}
	/** D&eacute;termine tous les intervalles de temps entre des intervalles de temps et des param&grave;tres
	 * Retourne un tableau ordonn&eacute; par date de d&eacute;but croissante */
	public NSArray determinerIntervallesPourIntervallesEtParametres(NSArray intervalles,NSArray parametres) throws Exception {
		NSMutableArray result = new NSMutableArray();
		LogManager.logDetail("nb intervalles source " + intervalles.count());
		LogManager.logDetail("nb parametres " + parametres.count());
		java.util.Enumeration e = intervalles.objectEnumerator();
		IntervalleTemps intervallePrecedent = null;
		while (e.hasMoreElements()) {
			IntervalleTemps intervalle = (IntervalleTemps)e.nextElement();
			LogManager.logDetail("intervalle a comparer " + intervalle);
			java.util.Enumeration e1 = parametres.objectEnumerator();
			while (e1.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e1.nextElement();
				LogManager.logDetail("intervalle parametre : [" + parametre.debutValiditeFormatee() + "," + parametre.finValiditeFormatee() + "]");
				IntervalleTemps nouvelIntervalle = IntervalleTemps.intersectionPeriodes(intervalle.debutPeriode(), intervalle.finPeriode(), parametre.debutValidite(), parametre.finValidite());
				if (nouvelIntervalle == null) {
					// on a dépassé
					break;
				} else {
					LogManager.logDetail("nouvel Intervalle " + nouvelIntervalle);
					if (intervallePrecedent == null || intervallePrecedent.estIdentique(nouvelIntervalle) == false) {
						result.addObject(nouvelIntervalle);
						intervallePrecedent = nouvelIntervalle;
					}
				}
			}
		}
		return result.sortedArrayUsingComparator(new IntervalleTempsComparator());
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
 package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeParamPerso;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** D&eacute;termine le montant de l'indemnit&eacute; en fonction des montants d'avances de recettes */
public class CalculIndemniteResponsabiliteAvanceRecette extends	PluginAvecParametresPersonnelsEtValidation {
	private final static String NUMERO_TRANCHE = "NUMTRAVR";
	private final static String NB_TRANCHES_SUPERIEURES = "NBTRAVRS";
	private final static String MONTANT_POUR_TRANCHE = "MOIRAV";
	private final static String MONTANT_POUR_TRANCHE_SUP = "MOIRAVRT";
	private final static int NUMERO_MAX = 13;
	
	/** pas de refus */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individu) {
		return null;
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** Pas de montant utilis&eacute; */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime, EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		return null;
	}
	/** Le montant n'est pas modifiable */
	public boolean peutModifierMontantPrime() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** montant cat&eacute;gorie x nb demi-journ&eacute;es effectu&eacute;es par l'individu */
	public String modeCalculDescription() {
		return "montant annuel fonction de la tranche d'avances/recettes et du nombre de tranches d'avances/recettes supérieures à 10 Millions";
	}
	// Méthodes protégées
	/** V&eacute;rifie si la tranche choisie est un nombre entre 1 et 13 et si un nb de tranche de 10 millions est saisi 
	 *  alors que le la tranche est inférieure ou égale à 12 */
	protected String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPourPrime) {
		LogManager.logDetail(getClass().getName() + " - Vérification paramètres personnels");
		if (parametrePerso.code().equals(NUMERO_TRANCHE)) {
			// Vérifier si c'est un entier compris entre 1 et 13
			try {
				int numTranche = new Integer(parametrePerso.valeur()).intValue();
				if (numTranche <= 0 ||numTranche > NUMERO_MAX) {
					return "Le numéro de tranche est compris entre 1 et 13 (voir l'aide utilisateur)";
				}
			} catch (Exception exc) {
				return "Le numéro de tranche est compris entre 1 et 13 (voir l'aide utilisateur)";
			}
		
		} else if (parametrePerso.code().equals(NB_TRANCHES_SUPERIEURES)) {
			try {
				int nbTranches = new Integer(parametrePerso.valeur()).intValue();
				if (nbTranches < 0) {
					return "Le nb de tranches supérieures doit être un entier positif ou nul";
				}
			} catch (Exception exc) {
				return "Le nb de tranches supérieures doit être un entier";
			}
		}	
		return null;
	}
	/** Utilise toujours les param&egrave;tres personnels */
	protected  boolean peutCalculerSansParametrePersonnel() {
		return false;
	}
	/** Pas de calcul &grave; partir de la personnalisation */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {		
	}
	/** Calcule le montant en fonction des param&egrave;tres personnels */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite,NSTimestamp finValidite) {
		LogManager.logDetail("CalculIndemniteResponsabiliteAvanceRecette - Calcul du montant de l'attribution");
		// Rechercher la tranche d'avances/recettes
		int numTranche = -1, nbTrancheSup = -1;
		java.util.Enumeration e = parametresPersos.objectEnumerator();
		// A ce stade, les paramètres personnels ont été définis
		while (e.hasMoreElements()) {
			EOPrimeParamPerso paramPerso = (EOPrimeParamPerso)e.nextElement();
			if (paramPerso.code().pcodCode().equals(NUMERO_TRANCHE)) {
				numTranche = new Integer(paramPerso.pppeValeur()).intValue();
			} else if (paramPerso.code().pcodCode().equals(NB_TRANCHES_SUPERIEURES)) {
				nbTrancheSup = new Integer(paramPerso.pppeValeur()).intValue();
			}
		}
		try {
			// Récupérer les paramètres définis sur cette période
			String codeTranche = MONTANT_POUR_TRANCHE;
			if (numTranche <= 9) {
				codeTranche = codeTranche + "R";
			}
			codeTranche = codeTranche + numTranche;
			NSArray parametres = information.parametresPourCodeEtDates(codeTranche,debutValidite, finValidite);
			e = parametres.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				NSTimestamp dateDebut = parametre.debutValidite(), dateFin = parametre.finValidite();
				if (DateCtrl.isBefore(dateDebut,debutValidite)) {
					dateDebut = debutValidite;
				}
				if (dateFin != null && finValidite != null && DateCtrl.isAfter(dateFin, finValidite)) {
					dateFin = finValidite;
				}
				if (parametre.pparMontant() == null) {
					PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Contactez l'administrateur, le paramètre " + parametre.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
				} else {
					double montant = parametre.pparMontant().doubleValue();
					String formuleCalcul = "Montant de l'indemnité pour la tranche " + numTranche + " : " + montant + "\n"; 
					if (numTranche >= 13 && nbTrancheSup > 0) {
						NSArray parametresSup = information.parametresPourCodeEtDates(MONTANT_POUR_TRANCHE_SUP,dateDebut, dateFin);
						// A priori, on considère qu'il n'y a qu'un seul montant supérieur qui coïncide avec le montant en cours
						if (parametresSup.count() == 0) {
							PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Contactez l'administrateur, le paramètre avec le code " + MONTANT_POUR_TRANCHE_SUP + " n'estb pas défini", dateDebut, dateFin);
						} else {
							EOPrimeParam paramMontantSup = (EOPrimeParam)parametresSup.objectAtIndex(0);
							if (paramMontantSup.pparMontant() == null) {
								PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),"Contactez l'administrateur, le paramètre " + paramMontantSup.pparLibelle() + " n'a pas de montant défini", dateDebut, dateFin);
							} 
							double montantSup = paramMontantSup.pparMontant().doubleValue();
							formuleCalcul += "Montant de l'indemnité pour les tranches supérieures : " + montantSup + "\nNb de tranches supérieures à 10 Millions : " + nbTrancheSup + "\nCalcul :";
							formuleCalcul += "" + montant + " + (" + montantSup + " x " + nbTrancheSup + ")";
							montant = montant + (montantSup * nbTrancheSup);
						}
					} 
					BigDecimal montantRetour = new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP);
					PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montantRetour, dateDebut, dateFin,formuleCalcul);
					LogManager.logDetail(formuleCalcul);
				}
			}
		} catch (Exception exc) {
			PrimeCalcul.sharedInstance().ajouterErreurPourPeriode(information.individu(),exc.getMessage(), debutValidite, finValidite);
		}
	}
}

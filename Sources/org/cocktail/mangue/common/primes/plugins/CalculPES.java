/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeParamPerso;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Retourne le montant de la personnalisation mais valide que ce montant est dans les limites minimum et maximum */
//10/03/2010 - si le montant est stocké dans les paramètres personnels, ce qui est le cas ici, il faut surcharge la méthode
//du parent verifierParametresPersonnels(NSArray parametresPersonnels,IndividuPourPrime individu) en faisant appel à super
//De même, il faut implémenter la méthode effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite)

public class CalculPES extends PluginAvecParametresPersonnelsEtValidation {
	private final static String DISTINCTION_SCIENTIFIQUE = "PESDISSC";
	private final static String COEFFICIENT_MAX = "MOPESMAX";
	private final static String COEFFICIENT_MAX_AVEC_DISTINCTION = "MOPESMA1";
	private final static String COEFFICIENT_MIN = "MOPESMIN";
	private final static String MONTANT_PES = "MOINDPES";

	/** pas de refus */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individuPourPrime) {
		return null;
	}
	/** pas de crit&egrave;re sp&eacute;cifique de refus */
	public boolean aCriteresEligibiliteSpecifiques() {
		return false;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** Peut &ecirc;tre modifi&eacute;e */
	public boolean peutModifierMontantPrime() {
		return true;
	}
	/** Surcharge du parent pour pouvoir valider le montant (on a besoin des autres param&grave;tres personnels) donc l'impl&eacute;mentation
	 * de verifierParametrePourIndividu ne suffit pas.
	 * V&eacute;rifie que le montant se trouve dans les limites minimum et maximum : on prend en compte le fait que l'individu a
	 * ou non une distinction scientifique (param&egrave;tre personnel) pour d&eacute;terminer le montant maximum */
	public String verifierParametresPersonnels(NSArray parametresPersonnels,IndividuPourPrime individuPourPrime) {
		String message = super.verifierParametresPersonnels(parametresPersonnels, individuPourPrime);
		if (message != null || parametresPersonnels == null || parametresPersonnels.count() == 0) {
			return message;
		}
		LogManager.logDetail("CalculPES - Verification Parametres Personnels");
		String codeMontantMax = COEFFICIENT_MAX;
		double montant = 0;
		// Vérifier si l'individu bénéficie d'une distinction scientifique ou non
		java.util.Enumeration e = parametresPersonnels.objectEnumerator();
		while (e.hasMoreElements()) {
			ParametrePersonnel parametrePerso = (ParametrePersonnel)e.nextElement();
			if (parametrePerso.code().equals(MONTANT_PES)) {
				montant = new BigDecimal(parametrePerso.valeur()).doubleValue();
			} else if (parametrePerso.valeur() != null && parametrePerso.valeur().equals("Oui")) {
				codeMontantMax = COEFFICIENT_MAX_AVEC_DISTINCTION;
			}
		}
		NSArray montantsMaximum = parametresValidesPourCodePeriodeEtQualifier(individuPourPrime.individu().editingContext(),codeMontantMax,individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),null);
		if (montantsMaximum.count() == 0) {
			return "CalculPES - Contactez l'administrateur, pas de paramètre défini pour le montant maximum (code " + codeMontantMax + ")";
		}
		String texte = verifierMontants(montantsMaximum);
		if (texte != null) {
			return "CalculPES - Montant maximum : " + texte;
		}
		// On récupère le paramètre valide pour la période, trier les paramètres par date de début décroissante pour prendre le plus grand
		montantsMaximum = EOSortOrdering.sortedArrayUsingKeyOrderArray(montantsMaximum, new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending)));
		EOPrimeParam parametre = parametrePourPeriode(montantsMaximum, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
		double montantMax = parametre.pparMontant().doubleValue();
		NSArray montantsMinimum = parametresValidesPourCodePeriodeEtQualifier(individuPourPrime.individu().editingContext(),COEFFICIENT_MIN,individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),null);
		if (montantsMinimum.count() == 0) {
			return "CalculPES - Contactez l'administrateur, pas de paramètre défini pour le montant minimum";
		}
		texte = verifierMontants(montantsMinimum);
		if (texte != null) {
			return "CalculPES - Montant minimum : " +  texte;
		}		
		montantsMinimum = EOSortOrdering.sortedArrayUsingKeyOrderArray(montantsMinimum, new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending)));
		parametre = parametrePourPeriode(montantsMinimum, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
		double montantMin = parametre.pparMontant().doubleValue();
		if (montant > montantMax) {
			String temp = "En l'absence d'une distinction scientifique internationale, ";
			if (codeMontantMax.equals(COEFFICIENT_MAX_AVEC_DISTINCTION)) {
				temp = "L'individu bénéficiant d'une distinction scientifique internationale, ";
			}
			return temp +  "le montant ne peut dépasser " + new BigDecimal(montantMax).setScale(2,BigDecimal.ROUND_HALF_UP) + "€";
		}
		if (montant < montantMin) {
			return "Le montant doit être supérieur à " + new BigDecimal(montantMin).setScale(2,BigDecimal.ROUND_HALF_UP) + "€";
		}
		return null;
	}
	/** Pas de v&eacute;rification du montant contenu dans la personnalisation */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return null;
	}
	/** montant pour la cat&eacute;gorie choisie */
	public String modeCalculDescription() {
		return "Montant fixé par arrêté annuel";
	}
	// Méthodes protégées
	/** V&eacute;rification de validit&eacute; : la distinction personnelle doit &ecirc;tre Oui ou Non */
	protected String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPrime) {
		if (parametrePerso.code().equals(DISTINCTION_SCIENTIFIQUE)) {
			if (parametrePerso.valeur().equals("Oui") == false && parametrePerso.valeur().equals("Non") == false) {
				return "La valeur à fournir pour indiquer que l'individu a bénéficié d'une distinction scientifique internationale est \"Oui\" ou \"Non\"";
			}
		}
		return null;
	}
	/** Le montant doit &ecirc;tre saisi comme param&egrave;tre personnel */
	protected  boolean peutCalculerSansParametrePersonnel() {
		return false;
	}
	/** utilisation des param&grave;tres personnels */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
		LogManager.logDetail(">>>>>>>effectuerCalcul pour dates");
		String formuleCalcul = "";

		BigDecimal montant = new BigDecimal(0);
		EOPrimeParamPerso paramMontant = null;
		java.util.Enumeration e = parametresPersos.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParamPerso param = (EOPrimeParamPerso)e.nextElement();
			if (param.code().pcodCode().equals(MONTANT_PES)) {
				paramMontant = param;
				break;
			}
		}
		if (paramMontant != null && paramMontant.pppeValeur() != null) {
			montant = new BigDecimal(paramMontant.pppeValeur());
			formuleCalcul = "Montant personnalisé";
		} else {
			LogManager.logDetail("Pas de paramètre personnel pour le montant");
			formuleCalcul = "Pas de montant fourni";
		}
		LogManager.logDetail(formuleCalcul);
		PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),montant, debutValidite, finValidite,formuleCalcul);
	}
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {
	}

}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.primes.plugins;

import java.math.BigDecimal;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.InformationPourPluginPrime;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;
import org.cocktail.mangue.modele.mangue.modalites.EODelegation;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Retourne le montant de la personnalisation mais valide que ce montant est dans les limites minimum et maximum */
public class CalculPrimeResponsabilitePedagogique extends PluginAvecParametresPersonnelsEtValidation {
	//private final static String MONTANT_PRIME = "MOINDPRP";
	private final static String MONTANT_PRIME_TD = "MOINDTD";
	private final static String COEFFICIENT_MAX = "COPRPMAX";
	private final static String COEFFICIENT_MIN = "COPRPMIN";

	/** pas de refus */
	public String diagnosticRefusEgilibilite(IndividuPourPrime individuPourPrime) {
		NSArray tempsPartiels = EOTempsPartiel.rechercherTempsPartielPourIndividuEtPeriode(individuPourPrime.individu().editingContext(),individuPourPrime.individu(), individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
		if (tempsPartiels.count() > 0) {
			return "L'individu est en temps partiel pendant la période, il ne peut bénéficier de cette prime";
		}
		NSArray delegations = EODelegation.rechercherDureesPourIndividuEtPeriode(individuPourPrime.individu().editingContext(), "Delegation", individuPourPrime.individu(), individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
		if (delegations.count() > 0) {
			return "L'individu est en délégation pendant la période, il ne peut bénéficier de cette prime";
		} else {
			return null;
		}
	}
	/** crit&egrave;re sp&eacute;cifique de refus : l'individu est en d&eacute;l&eacute;gation */
	public boolean aCriteresEligibiliteSpecifiques() {
		return true;
	}
	/** Peut &ecirc;tre renouvell&eacute;e */
	public boolean peutEtreRenouvellee() {
		return true;
	}
	/** Peut &ecirc;tre modifi&eacute;e */
	public boolean peutModifierMontantPrime() {
		return true;
	}
	/** V&eacute;rifie que le montant se trouve dans les limites minimum et maximum */
	public String verifierValiditeMontant(BigDecimal montant,IndividuPourPrime individuPourPrime,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		LogManager.logDetail("CalculPrimeResponsabilitePedagogique - Verification Parametre Personnel");
		NSArray tauxMaximum = parametresValidesPourCodePeriodeEtQualifier(individuPourPrime.individu().editingContext(),COEFFICIENT_MAX,individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),null);
		if (tauxMaximum.count() == 0) {
			return "CalculPrimeResponsabilitePedagogique - Contactez l'administrateur, pas de paramètre défini le coefficient multiplicateur maximum";
		}
		String texte = verifierTaux(tauxMaximum);
		if (texte != null) {
			return "CalculPrimeResponsabilitePedagogique - Taux maximum : " + texte;
		}
		// On récupère le paramètre valide pour la période, trier les paramètres par date de début décroissante pour prendre le plus grand
		tauxMaximum = EOSortOrdering.sortedArrayUsingKeyOrderArray(tauxMaximum, new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending)));
		EOPrimeParam parametre = parametrePourPeriode(tauxMaximum, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
		double coeeffMax = parametre.pparTaux().doubleValue();
		NSArray tauxMinimum = parametresValidesPourCodePeriodeEtQualifier(individuPourPrime.individu().editingContext(),COEFFICIENT_MIN,individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),null);
		if (tauxMinimum.count() == 0) {
			return "CalculPrimeResponsabilitePedagogique - Contactez l'administrateur, pas de paramètre défini pour le coefficient multiplicateur minimum";
		}
		texte = verifierTaux(tauxMinimum);
		if (texte != null) {
			return "CalculPrimeResponsabilitePedagogique - Taux minimum : " +  texte;
		}
		parametre = parametrePourPeriode(tauxMinimum, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
		double coeffMin = parametre.pparTaux().doubleValue();
		NSArray montantsTD = parametresValidesPourCodePeriodeEtQualifier(individuPourPrime.individu().editingContext(),MONTANT_PRIME_TD,individuPourPrime.periodeDebut(),individuPourPrime.periodeFin(),null);
		texte = verifierMontants(montantsTD);
		if (montantsTD.count() == 0) {
			return "CalculPrimeResponsabilitePedagogique - Contactez l'administrateur, pas de paramètre défini pour le montant d'indemnité TD";
		}
		if (texte != null) {
			return "CalculPrimeResponsabilitePedagogique - Montant indemnité TD : " + texte;
		}
		// Trier les paramètres par date de début croissante pour prendre le plus petit
		montantsTD = EOSortOrdering.sortedArrayUsingKeyOrderArray(montantsTD, new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareAscending)));
		parametre = parametrePourPeriode(montantsTD, individuPourPrime.periodeDebut(), individuPourPrime.periodeFin());
		double montantTd = parametre.pparMontant().doubleValue();
		double montantMax = montantTd * coeeffMax;
		if (montant.doubleValue() > montantMax) {
			return "Le montant ne peut dépasser " + new BigDecimal(montantMax).setScale(2,BigDecimal.ROUND_HALF_UP) + "€";
		}
		double montantMin = montantTd * coeffMin;
		if (montant.doubleValue() < montantMin) {
			return "Le montant doit être supérieur à " + new BigDecimal(montantMin).setScale(2,BigDecimal.ROUND_HALF_UP) + "€";
		}
		return null;
	}
	/** montant pour la cat&eacute;gorie choisie */
	public String modeCalculDescription() {
		return "Montant fixé par arrêté annuel";
	}
	// Méthodes protégées
	/** Pas de v&eacute;rification de validit&eacute; */
	protected String verifierParametrePourIndividu(ParametrePersonnel parametrePerso,IndividuPourPrime individuPrime) {
		return null;
	}
	/** Ne peut pas calculer sans param&egrave;tre personnel */
	protected  boolean peutCalculerSansParametrePersonnel() {
		return true;
	}
	/** pas de param&grave;tre personnel */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,NSArray parametresPersos,NSTimestamp debutValidite, NSTimestamp finValidite) {
	}
	/** En l'absence de personnalisation, fournit un montant nul */
	protected void effectuerCalculPourDates(InformationPourPluginPrime information,EOPrimePersonnalisation personnalisation,NSTimestamp debutValidite, NSTimestamp finValidite) {
		if (personnalisation == null) {
			String formuleCalcul = "Pas de montant fourni";
			LogManager.logDetail(formuleCalcul);
			PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),new BigDecimal(0), debutValidite, finValidite,formuleCalcul);
			return;
		} else {
			String formuleCalcul = "Montant personnalisé";
			LogManager.logDetail(formuleCalcul);

			PrimeCalcul.sharedInstance().ajouterMontantPourPeriode(information.individu(),personnalisation.pmpeMontant(), debutValidite, finValidite,formuleCalcul);
		}
	}
	
}

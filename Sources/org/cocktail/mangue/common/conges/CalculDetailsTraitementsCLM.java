package org.cocktail.mangue.common.conges;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArreteAnnulation;
import org.cocktail.mangue.modele.mangue.conges.DetailConge;
import org.cocktail.mangue.modele.mangue.conges.EOClm;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe pour calculer le détail des traitements pour les congés longue maladie
 * 
 * @author Chama LAATIK
 */
public class CalculDetailsTraitementsCLM {
	
	private String messageConsole;

	/**
	 * Constructeur
	 */
	public CalculDetailsTraitementsCLM() {
		
	}
	
	/**
	 * Calcul des détails de traitements pour un individu et une periode donnee
	 * 
	 * @param ec : editing Context
	 * @param individu : individu
	 * @param dateDebut : début du congé
	 * @param dateFin : fin du congé
	 * @return liste des détails de traitement
	 */
	@SuppressWarnings("unchecked")
	public NSArray<DetailConge> calculerDetails(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		
		try {

			Integer param = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_PERIODE_REFERENCE);
			int nbAnneesGlissement = (param.intValue()) / 12;			

			LogManager.logDetail("ANNEES : " + nbAnneesGlissement);

			int nbJoursComparaisonTotal = 0;	// pour stocker le nombre de jours de comparaison payé à plein traitement et demi-traitement
			int nbJoursTotal = 0;				// pour stocker le nombre de jours total payé à plein traitement et demi-traitement

			NSMutableArray<DetailConge> detailsConges = new NSMutableArray<DetailConge>();
			NSTimestamp dateATraiter = new NSTimestamp();

			NSTimestamp borne1 = null;
			NSTimestamp borne2 = dateFin;

			messageConsole = "Détail du calcul de traitement des congés maladie non titulaires: \n";
			
			// Recherche de tous les congés concernés jusqu'à la date de fin du congé traité
			NSArray<EOClm> conges = CongeAvecArreteAnnulation.rechercherCongesValidesPourIndividuAnterieurs(ec, EOClm.ENTITY_NAME, individu, borne2);
			conges = EOSortOrdering.sortedArrayUsingKeyOrderArray(conges, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));

			// On parcourt tous les congés à prendre en compte
			for (EOClm congeTraite : conges) {
				
				// Stockage de tous les jours de conges avec leur taux et date
				NSMutableArray<NSMutableDictionary> arrayJours = new NSMutableArray<NSMutableDictionary>();

				// Initialisation des durees max PT et DT en jours (0, 30, 60 ou 90j selon l'anciennete)
				int nbJoursPleinTraitement = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_PLEIN_TRAITEMENT).intValue() * 30;
				int nbJoursDemiTraitement = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_DEMI_TRAITEMENT).intValue() * 30;
				
				LogManager.logDetail("JOURS PLEIN : " + nbJoursPleinTraitement);
				LogManager.logDetail("JOURS DEMI : " + nbJoursDemiTraitement);
				
				
				// Le jour de carence n'est applicable qu'à partir du 01/01/2012
				messageConsole = messageConsole.concat("\n*** CONGE DU : " + DateCtrl.dateToString(congeTraite.dateDebut()) + " au " + DateCtrl.dateToString(congeTraite.dateFin()) 
					 + " : \n\n");
				dateATraiter = congeTraite.dateDebut();

				int nbJoursSupPleinTraitement = 1;
				while (DateCtrl.isBeforeEq(dateATraiter, congeTraite.dateFin())) {			

					NSMutableDictionary dicoJours = new NSMutableDictionary();
					dicoJours.setObjectForKey(dateATraiter, "jour");
					dicoJours.setObjectForKey("N", "carence");
					
					nbJoursTotal++;

					borne1 = DateCtrl.dateAvecAjoutAnnees(dateATraiter, -nbAnneesGlissement);	// Date a traiter moins nb années glissement.
					borne1 = DateCtrl.dateAvecAjoutJours(borne1, 1);							// il faut partir du jour j+1

					borne2 = DateCtrl.jourPrecedent(dateATraiter);
					// Calculer le nombre de jours comptables de plein traitement pendant les congés précédents
					int joursPleinTraitement = nbJoursPleinTraitement(detailsConges, borne1, borne2, false);
					LogManager.logDetail("joursPleinTraitement du " + DateCtrl.dateToString(borne1) + " au " + DateCtrl.dateToString(borne2) + " : " + joursPleinTraitement);

					NSTimestamp dateComparaison = DateCtrl.dateAvecAjoutJours(congeTraite.dateDebut(), nbJoursSupPleinTraitement - 1); 	// on enlève 1 car nbJoursSupPleinTraitement est initialisé à 1
					//durée calendaire à la place de durée comptable
					int dureeSupplementairePleinTraitement = DateCtrl.nbJoursEntre(congeTraite.dateDebut(), dateComparaison, true, false);

					if (dureeSupplementairePleinTraitement <= nbJoursPleinTraitement) {
						dicoJours.setObjectForKey(dureeSupplementairePleinTraitement, "joursPt");
					} else {
						dicoJours.setObjectForKey("0", "joursPt");
					}
					
					LogManager.logDetail("dureeSupplementairePleinTraitement : " + dureeSupplementairePleinTraitement);
					int nbJoursComparaison = joursPleinTraitement + dureeSupplementairePleinTraitement;
					dicoJours.setObjectForKey(new Integer(nbJoursComparaison), "joursComparaison");

					// if (nbJoursComparaison > 0 && nbJoursComparaison <= nbJoursPleinTraitement) { 
					// pose un problème lorsque le congé commence le 31 d'un mois
					LogManager.logDetail("NB JOURS PT TOTAL : " + nbJoursPleinTraitement);
					LogManager.logDetail("NB JOURS COMPARAISON TOTAL : " + nbJoursComparaisonTotal);
					
					if (nbJoursComparaison >= 0 && nbJoursComparaison <= nbJoursPleinTraitement) {
						LogManager.logDetail("NB JOURS COMPARAISON : " + nbJoursComparaison);
						nbJoursSupPleinTraitement++;	// on vient d'ajouter 1 journée à plein traitement
						dicoJours.setObjectForKey((Number) new Integer(100), "taux");
						if (nbJoursComparaison == nbJoursPleinTraitement) {
							nbJoursTotal = nbJoursComparaison;
							nbJoursComparaisonTotal = nbJoursComparaison;
						}
					} else {
						nbJoursComparaisonTotal += DateCtrl.dureeComptable(dateATraiter, dateATraiter, true);

						LogManager.logDetail("NB JOURS COMPARAISON TOTAL : " + nbJoursComparaisonTotal);

						if (nbJoursComparaisonTotal <= nbJoursPleinTraitement + nbJoursDemiTraitement) {
							LogManager.logDetail("TAUX : 50");							
							dicoJours.setObjectForKey((Number) new Integer(50), "taux");
						} else { 
							// si on dépasse le nombre de jours à plein traitement et le nombre de jours à demi-traitement
							// pas de rémunération => taux nul
							LogManager.logDetail("TAUX : 0");							
							dicoJours.setObjectForKey((Number) new Integer(0), "taux");
						}
					}

					if (dicoJours.objectForKey("carence").equals("O")) {
						dicoJours.setObjectForKey((Number) new Integer(0), "taux");
					}
					
					arrayJours.addObject(dicoJours);
					dateATraiter = DateCtrl.jourSuivant(dateATraiter);
				}

				// Affichage du detail du calcul dans la console
				for (NSMutableDictionary myDico : arrayJours) {
					Integer taux = (Integer) myDico.objectForKey("taux");
					messageConsole = messageConsole.concat("	Jour : " + DateCtrl.dateToString((NSTimestamp) myDico.objectForKey("jour")));
					messageConsole = messageConsole.concat("	Taux : " + myDico.objectForKey("taux"));
					if (myDico.objectForKey("joursPt") != null && taux.intValue() != 50) {
						messageConsole = messageConsole.concat("	Jours PT : " + myDico.objectForKey("joursPt"));
					} else {
						messageConsole = messageConsole.concat("	             ");
					}
					
					if (taux.intValue() != 50) {
						messageConsole = messageConsole.concat("	TOTAL PT : " + myDico.objectForKey("joursComparaison"));
					}
					
					messageConsole = messageConsole.concat("\n");
				}
				
				NSMutableArray<NSDictionary> periodesTraitement = regrouperPeriodesTaux(arrayJours);
				boolean isPremier = true;
				// Créer les détails de congés par taux
				for (NSDictionary dicoTemp : periodesTraitement) {
					DetailConge newDetail = DetailConge.nouvellePeriode((NSTimestamp) dicoTemp.objectForKey("debut"), 
							(NSTimestamp) dicoTemp.objectForKey("fin"), (Integer) dicoTemp.objectForKey("taux"));
					newDetail.setTemJourCarence("N");
					detailsConges.addObject(newDetail);
				}
			}
			
			return detailsConges;
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
	
	/**
	 * 	Renvoie le nombre de jours de maladie à plein traitement entre deux dates
	 * 
	 */
	private static int nbJoursPleinTraitement(NSArray<DetailConge> periodesDetail, NSTimestamp date1, NSTimestamp date2, boolean estDureeComptable) {
		NSTimestamp dateDebut = null, dateFin = null;
		float nbJours = (float)0.0;
		for (DetailConge dicoDetail : periodesDetail) {
			

			NSTimestamp detailDebut = dicoDetail.dateDebut();
			NSTimestamp detailFin = dicoDetail.dateFin();
			Integer detailTaux = dicoDetail.numTaux();

			// si le détail de congé démarre avant la date de fin et se termine après la date de début
			// ils ont une période commune : c'est la période comprise entre le max des dates début et le min des dates fin
			if (DateCtrl.isBeforeEq(detailDebut, date2) && DateCtrl.isAfterEq(detailFin, date1) && (detailTaux.intValue() == 100 || detailTaux.intValue() == 0)) {
				if (DateCtrl.isBeforeEq(detailDebut, date1)) {
					//dateDebut = DateCtrl.jourSuivant(date1.timestampByAddingGregorianUnits);
					dateDebut = date1;	// on prend en compte les bornes dans les calculs
				} else {
					dateDebut = detailDebut;
					//dateDebut = DateCtrl.jourPrecedent(detail.dateDebut());
				}
				if (DateCtrl.isAfterEq(detailFin, date2)) {
					dateFin = date2;
				} else {
					dateFin = detailFin;
				}
				if (DateCtrl.isBeforeEq(dateDebut, dateFin)) {
					// cas aux limites ou la date de début est égale ou passe après la date de fin
					// Pour chaque conge maladie , on calcule le nombre de jours comptables de l'absence
					try {
						nbJours = nbJours + DateCtrl.nbJoursEntre(dateDebut, dateFin, true, estDureeComptable);	
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		}
		return (int)nbJours;
	}

	/**
	 * Regroupement de périodes de congés ayant le meme taux de traitement
	 * Un congé peut donc etre fractionné en deux periodes à 100% et 50%
	 * @param arrayJours
	 * @return
	 */
	private NSMutableArray regrouperPeriodesTaux(NSMutableArray<NSMutableDictionary> arrayJours) {
		Number taux = (Number) new Integer(-1);
		NSMutableArray periodesTraitement = new NSMutableArray();
		NSTimestamp lastDate = null;
		for (NSDictionary myDico : arrayJours) {

			// Si on change d'etat, on ajoute la date et l'etat
			if (taux.intValue() != ((Number) myDico.objectForKey("taux")).intValue()) {
				if (periodesTraitement.count() > 0) {
					NSMutableDictionary periodePrecedente = (NSMutableDictionary) periodesTraitement.objectAtIndex(periodesTraitement.count() - 1);
					periodePrecedente.setObjectForKey(DateCtrl.jourPrecedent((NSTimestamp) myDico.objectForKey("jour")), "fin");
				}
				NSMutableDictionary dicoTraitement = new NSMutableDictionary();
				dicoTraitement.setObjectForKey((NSTimestamp) myDico.objectForKey("jour"), "debut");
				dicoTraitement.setObjectForKey(myDico.objectForKey("taux"), "taux");
				taux = (Number) myDico.objectForKey("taux");
				periodesTraitement.addObject(dicoTraitement);
			}

			lastDate = (NSTimestamp) myDico.objectForKey("jour");
		}
		// Pour la dernière période
		if (periodesTraitement.count() > 0) {
			NSMutableDictionary periodePrecedente = (NSMutableDictionary) periodesTraitement.objectAtIndex(periodesTraitement.count() - 1);
			periodePrecedente.setObjectForKey(lastDate, "fin");
		}

		return periodesTraitement;
	}
	
	public String getMessageConsole() {
		return messageConsole;
	}

	public void setMessageConsole(String messageConsole) {
		this.messageConsole = messageConsole;
	}
}
